/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For the complete reference:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	//config.extraPlugins = 'Images';

	config.toolbar = 'Full';

	config.toolbar_Full =
	[
		{ name : 'basico',  items : ['Bold', 'Italic']},
		{ name : 'listas',  items : ['NumberedList', 'BulletedList']},
		{ name : 'links',   items : ['Link', 'Unlink']},
		{ name : 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] }
	];

	// The default plugins included in the basic setup define some buttons that
	// we don't want too have in a basic editor. We remove them here.
	config.removeButtons = '';

	// Let's have it basic on dialogs as well.
	config.removeDialogTabs = 'link:advanced';

	config.filebrowserBrowseUrl = '/kcfinder/browse.php?type=files&lang=pt-br';
   	config.filebrowserImageBrowseUrl = '/kcfinder/browse.php?type=images&lang=pt-br';
   	config.filebrowserFlashBrowseUrl = '/kcfinder/browse.php?type=flash&lang=pt-br';
   	config.filebrowserUploadUrl = '/kcfinder/upload.php?type=files&lang=pt-br';
   	config.filebrowserImageUploadUrl = '/kcfinder/upload.php?type=images&lang=pt-br';
   	config.filebrowserFlashUploadUrl = '/kcfinder/upload.php?type=flash&lang=pt-br';	
};
