$(window).load(function(){
	$('.direita-wrapper').height($('body').height());
});

if(window.location.href.search('previa') == -1){
	var prefixo = "/";
}else{
	var prefixo = "/previa/";
}

//Imagem triangular Área de Atuação home
var triangulos = [
	{ area: 'aeronautica', src: 'home_hover-aeronautica.png' },
	{ area: 'campo',  src: 'home_hover-campo.png' },
	{ area: 'construcao-civil', src: 'home_hover-construcao.png' },
	{ area: 'midia', src: 'home_hover-industria.png' },
	{ area: 'maritimo', src: 'home_hover-maritima.png' },
	{ area: 'militar', src: 'home_hover-militar.png' },
	{ area: 'mineracao', src: 'home_hover-mineracao.png' },
	{ area: 'petroleo-e-gas', src: 'home_hover-petroleo.png' },
	{ area: 'energia-eolica', src: 'home_hover-eolica.png' }
]

function getSrc(area){
	var src = '';
	$.map(triangulos, function(i) {
		if(i.area === area){
			src = i.src;
		}
	});
	return src;
}

var atuacaoTriangulo = $('.home-atuacao-triangulo');

$('.home-atuacao-link').hover(function() {
	src = getSrc($(this).data('area'));
	img = $('<img />');

	img.attr('src', prefixo+'assets/img/' + src);
	atuacaoTriangulo.append(img);
}, function() {
	atuacaoTriangulo.empty()
});


//Equaliza a altura dos boxes de notícias
$(".noticia-container").equalize();

//Accordion na tabela de produtos
var produtos = $('.accordion-wrapper');
    produtos.find("tr:not('.accordion')").hide();
    produtos.find('.accordion').click(function(){
        $(this).siblings().slideToggle('slow');
        $(this).toggleClass('active');
    });

//Cores alternadas nas linhas da tabela de produtos
$(".accordion-wrapper > tbody > tr:odd").not('.accordion').addClass("odd");

//Borda na última linha de produto de cada categoria
$('.accordion-wrapper tbody > .produto:last-child').css('border-bottom', 'solid 1px #D3BCA7');

//Link nas linhas da tabela de produtos
$('.accordion-wrapper tbody > .produto').click(function(){
	document.location.href = $(this).data('link');
});

//Hover nas colunas da tabela de produtos
$('.thbottom').hover(function(){
	$('[data-area=' + $(this).data('area-definition') + ']').addClass('columnhover');
}, function(){
	$('[data-area=' + $(this).data('area-definition') + ']').removeClass('columnhover');
})

//Accordion na página de área geral de atuãção
$('.atuacao-accordion').accordion({
	heightStyle: 'content'
});

//Envio do formulário de contato
$('#contato-form').submit(function(e){
	e.preventDefault();
	var post = {
		nome: $('#nome').val(),
		email: $('#email').val(),
		empresa: $('#empresa').val(),
		telefone: $('#telefone').val(),
		mensagem: $('#mensagem').val(),
		setor: $('#setor').val()
	}
	$.ajax({
		url: 'contato',
		type: 'POST',
		dataType: 'json',
		data: post,
	})
	.done(function(data) {
		if(data.errors)
		{
			alert(data.msg + "\n" + data.errors);
		} else {
			$('input[type="text"]').each(function(index, el) {
				$(this).val('');
			});
			$('select').val('');
			$('textarea').val('');

			alert(data.msg);
		}
	});
});

$('#form-ativar').submit(function(e){
	e.preventDefault();
	var post = {
		nome: $('#txt-nome').val(),
		email: $('#txt-email').val(),
		empresa: $('#txt-empresa').val(),
		telefone: $('#txt-telefone').val(),
		equipamentos: $('#txt-equipamentos').val(),
		servicos: $('#txt-servicos').val(),
		observacoes : $('#txt-observacoes').val()
	}
	$.ajax({
		url: prefixo+'ativar',
		type: 'POST',
		dataType: 'json',
		data: post,
	})
	.done(function(data) {
		if(data.errors)
		{
			alert(data.msg + "\n" + data.errors);
		} else {
			$('input[type="text"], input[type="email"]').each(function(index, el) {
				$(this).val('');
			});
			$('textarea').val('');

			alert(data.msg);
		}
	});
});