//Defnição de prefixo de url para diferentes ambientes
var environment = "testing";
if (environment == 'development') {
	var prefix = "";
} else if (environment == 'testing') {
	var prefix = ":8000";
} else {
	var prefix = "";
}

if(window.location.href.search('previa') == -1){
	var prefixo = "/";
}else{
	var prefixo = "/previa/";
}

//Seletor de tipo de mídia
$('#midia-form #midiatipo_id').change(function(){
	if( $(this).val() === '1' )
	{
		$('#midia-form #video-controls').hide();
		$('#midia-form #imagem-controls').show();
	} else {
		$('#midia-form #video-controls').show();
		$('#midia-form #imagem-controls').hide();
	}
});

//Datepicker
if($(".datepicker").val() === ''){
	var datepicker = $( ".datepicker" ).datepicker({
		dateFormat: "dd/mm/yy"
	}).datepicker("setDate", new Date());
} else {
	var datepicker = $( ".datepicker" ).datepicker({
		dateFormat: "dd/mm/yy"
	});
}

$(".datepicker[value='']")
$(".datesetter").click(function(){
	datepicker.datepicker("show");
});

//Timepicker
var timepicker = $(".timepicker").timepicker();

$(".timesetter").click(function(){
	timepicker.timepicker("show");
})

/*==============================================================================
	Áreas específicas de atuação
  ============================================================================*/

//Obtém as áreas específicas de áreas gerais selecionadas
function getEspecificas(geralId){
    $.getJSON(prefixo+'painel/especificas/json/' + geralId, function(data) {
    	if(data.error == false){
    		if($('#atuacao option:selected').length <= 1){
				$('#especifica option').not(":first").remove();
			}
	    	$.each(data.especificas, function(index, val) {
	    		var option = '<option data-geral="' + geralId + '" value="' + index + '">' + val + '</option>';
	    		$('#especifica').append(option);
	    	});
    	}
    })
}

//Remove as áreas específicas de áreas gerais não selecionadas
function removeEspecificas(geralId){
    $('#especifica option[data-geral="' + geralId + '"]').remove();
}

//Seletor de área específica de atuação
$('#atuacao option').click(function(){
	if(this.selected == true){
	    getEspecificas($(this).val());
	}
	else{
	    removeEspecificas($(this).val());
	}
});

/*==============================================================================
	Produtos
  ============================================================================*/

  /**
   * Upload de arquivos para produtos
   */
  $('#produtos-file-upload').submit(function(e) {
  		var response = $('#response');
	    e.preventDefault();
	    $.ajaxFileUpload({
	        url         : prefixo+"painel/arquivos",
	        secureuri      : false,
	        fileElementId  : 'path',
	        dataType    : "JSON",
	        data        : {
	            'titulo' : $('#produtos-file-titulo').val(),
	            'produto_id': $('#produto_id').val()
	        },
	        success  : function (data)
	        {
	            data = JSON.parse(data);
	            if(data.error)
	            {
	            	response.removeClass('alert-success').addClass('alert-warning').text(data.error).show();
	            } else {
	            	$('.emptyMessage').hide();
	            	var arquivoTemplate = '<div class="produto-arquivo" data-arquivo-id="' + data.arquivoId +'"><span>' +
	            						  $('#produtos-file-titulo').val() + '</span>' +
	            						  '<a href="#" class="btn btn-danger btn-mini deleta-arquivo" data-id="' +
	            						  data.arquivoId + '"><i class="iconic-trash"></i></a></div>';
	            	$('.lista-produtos').append(arquivoTemplate);
	            	response.show().removeClass('alert-warning').addClass('alert-success').text(data.msg);
	            }
	        },
	    });
	    return false;
   });

  /**
   * Upload de arquivos para produtos
   */
  $('#produtos-file-upload-en').submit(function(e) {
  		var response = $('#response-en');
	    e.preventDefault();
	    $.ajaxFileUpload({
	        url         : prefixo+"painel/arquivosEn",
	        secureuri      : false,
	        fileElementId  : 'path-en',
	        dataType    : "JSON",
	        data        : {
	            'titulo' : $('#produtos-file-titulo-en').val(),
	            'produto_id': $('#produto_id').val()
	        },
	        success  : function (data)
	        {
	            data = JSON.parse(data);
	            if(data.error)
	            {
	            	response.removeClass('alert-success').addClass('alert-warning').text(data.error).show();
	            } else {
	            	$('.emptyMessage').hide();
	            	var arquivoTemplate = '<div class="produto-arquivo" data-arquivo-id="' + data.arquivoId +'"><span>' +
	            						  $('#produtos-file-titulo-en').val() + '</span>' +
	            						  '<a href="#" class="btn btn-danger btn-mini deleta-arquivo-en" data-id="' +
	            						  data.arquivoId + '"><i class="iconic-trash"></i></a></div>';
	            	$('.lista-produtos-en').append(arquivoTemplate);
	            	response.show().removeClass('alert-warning').addClass('alert-success').text(data.msg);
	            }
	        },
	    });
	    return false;
   });

  /**
   * Apagar arquivo de produtos
   */
$(document.body).on('click', '.deleta-arquivo', function(){
	var id = $(this).data("id");
	$.ajax({
		url: prefixo+"painel/arquivos/" + id,
		type: 'POST',
		dataType: 'json',
		type: 'DELETE'
	})
	.done(function() {
		$('[data-arquivo-id="' + id + '"]').remove();
		console.log("success");
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	return false;
});

$(document.body).on('click', '.deleta-arquivo-en', function(){
	var id = $(this).data("id");
	$.ajax({
		url: prefixo+"painel/arquivosEn/" + id,
		type: 'POST',
		dataType: 'json',
		type: 'DELETE'
	})
	.done(function() {
		$('[data-arquivo-id="' + id + '"]').remove();
		console.log("success");
	})
	.fail(function() {
		console.log("error");
	})
	.always(function() {
		console.log("complete");
	});
	return false;
});

/*==============================================================================
	Ordenar ítens
  ============================================================================*/

$('.ordenar').click(function(){
	if($(this).hasClass('salvar-ordem')){
		$.ajax({
			url: prefixo+'painel/ordenar',
			type: 'POST',
			dataType: 'json',
			data: {
				model: $(this).data('model'),
				itens: $("table tbody").sortable("serialize")
			},
		})
		.done(function(data) {
			$('.ordenar').removeClass('btn-sucess salvar-ordem').addClass('btn-info').text('Ordenar ítens');
			$("table tbody").sortable({
				'disabled': false,
				'delay': '100',
				helper: function(e, ui) {
			      ui.children().each(function() {
			        $(this).width($(this).width());
			      });
			      return ui;
			    },
			});
			alert(data.message);
		})
		.fail(function() {
			console.log("error");
		})
	} else {
		$("table tbody").sortable({
			'disabled': false,
			'delay': '100',
			helper: function(e, ui) {
		      ui.children().each(function() {
		        $(this).width($(this).width());
		      });
		      return ui;
		    },
		});
		$(this).removeClass('btn-info').addClass('salvar-ordem btn-success').text('Salvar Ordem');
		alert("Arraste os itens para ordená-los e depois clique no Botão 'Salvar Ordem'.");
	}
	return false;
});


$.datepicker.regional['pt-BR'] = {
    closeText: 'Fechar',
    prevText: '&#x3c;Anterior',
    nextText: 'Pr&oacute;ximo&#x3e;',
    currentText: 'Hoje',
    monthNames: ['Janeiro','Fevereiro','Mar&ccedil;o','Abril','Maio','Junho',
    'Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun',
    'Jul','Ago','Set','Out','Nov','Dez'],
    dayNames: ['Domingo','Segunda-feira','Ter&ccedil;a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S&aacute;bado'],
    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
    dayNamesMin: ['Dom','Seg','Ter','Qua','Qui','Sex','S&aacute;b'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 0,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
};

$.datepicker.setDefaults($.datepicker.regional['pt-BR']);

$('document').ready( function(){

    //$('.datepicker').datepicker();

    $('#input-tipo').change( function(){
        var tipo = $(this).val();

        if(tipo == 'imagem'){
            $('#popupInfoLink').show('normal');
            $('#popupInfoVideo').hide('normal');
        }else if(tipo == 'video'){
            $('#popupInfoLink').hide('normal');
            $('#popupInfoVideo').show('normal');
        }else{
            $('#popupInfoLink').hide('normal');
            $('#popupInfoVideo').hide('normal');
        }
    });

});