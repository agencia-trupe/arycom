if(!Modernizr.input.placeholder){

  $('[placeholder]').focus(function() {
    var input = $(this);
    if (input.val() == input.attr('placeholder')) {
    input.val('');
    input.removeClass('placeholder');
    }
  }).blur(function() {
    var input = $(this);
    if (input.val() == '' || input.val() == input.attr('placeholder')) {
    input.addClass('placeholder');
    input.val(input.attr('placeholder'));
    }
  }).blur();
  $('[placeholder]').parents('form').submit(function() {
    $(this).find('[placeholder]').each(function() {
    var input = $(this);
    if (input.val() == input.attr('placeholder')) {
      input.val('');
    }
    })
  });

}

/* Portuguese initialisation for the jQuery UI date picker plugin. */
jQuery(function(e){e.datepicker.regional["pt"]={closeText:"Fechar",prevText:"<Anterior",nextText:"Seguinte",currentText:"Hoje",monthNames:["Janeiro","Fevereiro","Março","Abril","Maio","Junho","Julho","Agosto","Setembro","Outubro","Novembro","Dezembro"],monthNamesShort:["Jan","Fev","Mar","Abr","Mai","Jun","Jul","Ago","Set","Out","Nov","Dez"],dayNames:["Domingo","Segunda-feira","Terça-feira","Quarta-feira","Quinta-feira","Sexta-feira","Sábado"],dayNamesShort:["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],dayNamesMin:["Dom","Seg","Ter","Qua","Qui","Sex","Sáb"],weekHeader:"Sem",dateFormat:"dd/mm/yy",firstDay:0,isRTL:false,showMonthAfterYear:false,yearSuffix:""};e.datepicker.setDefaults(e.datepicker.regional["pt"])});

/*! jQuery Timepicker Addon - v1.4 - 2013-08-11
* http://trentrichardson.com/examples/timepicker
* Copyright (c) 2013 Trent Richardson; Licensed MIT */
(function ($) {

  /*
  * Lets not redefine timepicker, Prevent "Uncaught RangeError: Maximum call stack size exceeded"
  */
  $.ui.timepicker = $.ui.timepicker || {};
  if ($.ui.timepicker.version) {
    return;
  }

  /*
  * Extend jQueryUI, get it started with our version number
  */
  $.extend($.ui, {
    timepicker: {
      version: "1.4"
    }
  });

  /* 
  * Timepicker manager.
  * Use the singleton instance of this class, $.timepicker, to interact with the time picker.
  * Settings for (groups of) time pickers are maintained in an instance object,
  * allowing multiple different settings on the same page.
  */
  var Timepicker = function () {
    this.regional = []; // Available regional settings, indexed by language code
    this.regional[''] = { // Default regional settings
      currentText: 'Now',
      closeText: 'Done',
      amNames: ['AM', 'A'],
      pmNames: ['PM', 'P'],
      timeFormat: 'HH:mm',
      timeSuffix: '',
      timeOnlyTitle: 'Choose Time',
      timeText: 'Time',
      hourText: 'Hour',
      minuteText: 'Minute',
      secondText: 'Second',
      millisecText: 'Millisecond',
      microsecText: 'Microsecond',
      timezoneText: 'Time Zone',
      isRTL: false
    };
    this._defaults = { // Global defaults for all the datetime picker instances
      showButtonPanel: true,
      timeOnly: false,
      showHour: null,
      showMinute: null,
      showSecond: null,
      showMillisec: null,
      showMicrosec: null,
      showTimezone: null,
      showTime: true,
      stepHour: 1,
      stepMinute: 1,
      stepSecond: 1,
      stepMillisec: 1,
      stepMicrosec: 1,
      hour: 0,
      minute: 0,
      second: 0,
      millisec: 0,
      microsec: 0,
      timezone: null,
      hourMin: 0,
      minuteMin: 0,
      secondMin: 0,
      millisecMin: 0,
      microsecMin: 0,
      hourMax: 23,
      minuteMax: 59,
      secondMax: 59,
      millisecMax: 999,
      microsecMax: 999,
      minDateTime: null,
      maxDateTime: null,
      onSelect: null,
      hourGrid: 0,
      minuteGrid: 0,
      secondGrid: 0,
      millisecGrid: 0,
      microsecGrid: 0,
      alwaysSetTime: true,
      separator: ' ',
      altFieldTimeOnly: true,
      altTimeFormat: null,
      altSeparator: null,
      altTimeSuffix: null,
      pickerTimeFormat: null,
      pickerTimeSuffix: null,
      showTimepicker: true,
      timezoneList: null,
      addSliderAccess: false,
      sliderAccessArgs: null,
      controlType: 'slider',
      defaultValue: null,
      parse: 'strict'
    };
    $.extend(this._defaults, this.regional['']);
  };

  $.extend(Timepicker.prototype, {
    $input: null,
    $altInput: null,
    $timeObj: null,
    inst: null,
    hour_slider: null,
    minute_slider: null,
    second_slider: null,
    millisec_slider: null,
    microsec_slider: null,
    timezone_select: null,
    hour: 0,
    minute: 0,
    second: 0,
    millisec: 0,
    microsec: 0,
    timezone: null,
    hourMinOriginal: null,
    minuteMinOriginal: null,
    secondMinOriginal: null,
    millisecMinOriginal: null,
    microsecMinOriginal: null,
    hourMaxOriginal: null,
    minuteMaxOriginal: null,
    secondMaxOriginal: null,
    millisecMaxOriginal: null,
    microsecMaxOriginal: null,
    ampm: '',
    formattedDate: '',
    formattedTime: '',
    formattedDateTime: '',
    timezoneList: null,
    units: ['hour', 'minute', 'second', 'millisec', 'microsec'],
    support: {},
    control: null,

    /* 
    * Override the default settings for all instances of the time picker.
    * @param  {Object} settings  object - the new settings to use as defaults (anonymous object)
    * @return {Object} the manager object
    */
    setDefaults: function (settings) {
      extendRemove(this._defaults, settings || {});
      return this;
    },

    /*
    * Create a new Timepicker instance
    */
    _newInst: function ($input, opts) {
      var tp_inst = new Timepicker(),
        inlineSettings = {},
        fns = {},
        overrides, i;

      for (var attrName in this._defaults) {
        if (this._defaults.hasOwnProperty(attrName)) {
          var attrValue = $input.attr('time:' + attrName);
          if (attrValue) {
            try {
              inlineSettings[attrName] = eval(attrValue);
            } catch (err) {
              inlineSettings[attrName] = attrValue;
            }
          }
        }
      }

      overrides = {
        beforeShow: function (input, dp_inst) {
          if ($.isFunction(tp_inst._defaults.evnts.beforeShow)) {
            return tp_inst._defaults.evnts.beforeShow.call($input[0], input, dp_inst, tp_inst);
          }
        },
        onChangeMonthYear: function (year, month, dp_inst) {
          // Update the time as well : this prevents the time from disappearing from the $input field.
          tp_inst._updateDateTime(dp_inst);
          if ($.isFunction(tp_inst._defaults.evnts.onChangeMonthYear)) {
            tp_inst._defaults.evnts.onChangeMonthYear.call($input[0], year, month, dp_inst, tp_inst);
          }
        },
        onClose: function (dateText, dp_inst) {
          if (tp_inst.timeDefined === true && $input.val() !== '') {
            tp_inst._updateDateTime(dp_inst);
          }
          if ($.isFunction(tp_inst._defaults.evnts.onClose)) {
            tp_inst._defaults.evnts.onClose.call($input[0], dateText, dp_inst, tp_inst);
          }
        }
      };
      for (i in overrides) {
        if (overrides.hasOwnProperty(i)) {
          fns[i] = opts[i] || null;
        }
      }

      tp_inst._defaults = $.extend({}, this._defaults, inlineSettings, opts, overrides, {
        evnts: fns,
        timepicker: tp_inst // add timepicker as a property of datepicker: $.datepicker._get(dp_inst, 'timepicker');
      });
      tp_inst.amNames = $.map(tp_inst._defaults.amNames, function (val) {
        return val.toUpperCase();
      });
      tp_inst.pmNames = $.map(tp_inst._defaults.pmNames, function (val) {
        return val.toUpperCase();
      });

      // detect which units are supported
      tp_inst.support = detectSupport(
          tp_inst._defaults.timeFormat + 
          (tp_inst._defaults.pickerTimeFormat ? tp_inst._defaults.pickerTimeFormat : '') +
          (tp_inst._defaults.altTimeFormat ? tp_inst._defaults.altTimeFormat : ''));

      // controlType is string - key to our this._controls
      if (typeof(tp_inst._defaults.controlType) === 'string') {
        if (tp_inst._defaults.controlType === 'slider' && typeof($.ui.slider) === 'undefined') {
          tp_inst._defaults.controlType = 'select';
        }
        tp_inst.control = tp_inst._controls[tp_inst._defaults.controlType];
      }
      // controlType is an object and must implement create, options, value methods
      else {
        tp_inst.control = tp_inst._defaults.controlType;
      }

      // prep the timezone options
      var timezoneList = [-720, -660, -600, -570, -540, -480, -420, -360, -300, -270, -240, -210, -180, -120, -60,
          0, 60, 120, 180, 210, 240, 270, 300, 330, 345, 360, 390, 420, 480, 525, 540, 570, 600, 630, 660, 690, 720, 765, 780, 840];
      if (tp_inst._defaults.timezoneList !== null) {
        timezoneList = tp_inst._defaults.timezoneList;
      }
      var tzl = timezoneList.length, tzi = 0, tzv = null;
      if (tzl > 0 && typeof timezoneList[0] !== 'object') {
        for (; tzi < tzl; tzi++) {
          tzv = timezoneList[tzi];
          timezoneList[tzi] = { value: tzv, label: $.timepicker.timezoneOffsetString(tzv, tp_inst.support.iso8601) };
        }
      }
      tp_inst._defaults.timezoneList = timezoneList;

      // set the default units
      tp_inst.timezone = tp_inst._defaults.timezone !== null ? $.timepicker.timezoneOffsetNumber(tp_inst._defaults.timezone) :
              ((new Date()).getTimezoneOffset() * -1);
      tp_inst.hour = tp_inst._defaults.hour < tp_inst._defaults.hourMin ? tp_inst._defaults.hourMin :
              tp_inst._defaults.hour > tp_inst._defaults.hourMax ? tp_inst._defaults.hourMax : tp_inst._defaults.hour;
      tp_inst.minute = tp_inst._defaults.minute < tp_inst._defaults.minuteMin ? tp_inst._defaults.minuteMin :
              tp_inst._defaults.minute > tp_inst._defaults.minuteMax ? tp_inst._defaults.minuteMax : tp_inst._defaults.minute;
      tp_inst.second = tp_inst._defaults.second < tp_inst._defaults.secondMin ? tp_inst._defaults.secondMin :
              tp_inst._defaults.second > tp_inst._defaults.secondMax ? tp_inst._defaults.secondMax : tp_inst._defaults.second;
      tp_inst.millisec = tp_inst._defaults.millisec < tp_inst._defaults.millisecMin ? tp_inst._defaults.millisecMin :
              tp_inst._defaults.millisec > tp_inst._defaults.millisecMax ? tp_inst._defaults.millisecMax : tp_inst._defaults.millisec;
      tp_inst.microsec = tp_inst._defaults.microsec < tp_inst._defaults.microsecMin ? tp_inst._defaults.microsecMin :
              tp_inst._defaults.microsec > tp_inst._defaults.microsecMax ? tp_inst._defaults.microsecMax : tp_inst._defaults.microsec;
      tp_inst.ampm = '';
      tp_inst.$input = $input;

      if (tp_inst._defaults.altField) {
        tp_inst.$altInput = $(tp_inst._defaults.altField).css({
          cursor: 'pointer'
        }).focus(function () {
          $input.trigger("focus");
        });
      }

      if (tp_inst._defaults.minDate === 0 || tp_inst._defaults.minDateTime === 0) {
        tp_inst._defaults.minDate = new Date();
      }
      if (tp_inst._defaults.maxDate === 0 || tp_inst._defaults.maxDateTime === 0) {
        tp_inst._defaults.maxDate = new Date();
      }

      // datepicker needs minDate/maxDate, timepicker needs minDateTime/maxDateTime..
      if (tp_inst._defaults.minDate !== undefined && tp_inst._defaults.minDate instanceof Date) {
        tp_inst._defaults.minDateTime = new Date(tp_inst._defaults.minDate.getTime());
      }
      if (tp_inst._defaults.minDateTime !== undefined && tp_inst._defaults.minDateTime instanceof Date) {
        tp_inst._defaults.minDate = new Date(tp_inst._defaults.minDateTime.getTime());
      }
      if (tp_inst._defaults.maxDate !== undefined && tp_inst._defaults.maxDate instanceof Date) {
        tp_inst._defaults.maxDateTime = new Date(tp_inst._defaults.maxDate.getTime());
      }
      if (tp_inst._defaults.maxDateTime !== undefined && tp_inst._defaults.maxDateTime instanceof Date) {
        tp_inst._defaults.maxDate = new Date(tp_inst._defaults.maxDateTime.getTime());
      }
      tp_inst.$input.bind('focus', function () {
        tp_inst._onFocus();
      });

      return tp_inst;
    },

    /*
    * add our sliders to the calendar
    */
    _addTimePicker: function (dp_inst) {
      var currDT = (this.$altInput && this._defaults.altFieldTimeOnly) ? this.$input.val() + ' ' + this.$altInput.val() : this.$input.val();

      this.timeDefined = this._parseTime(currDT);
      this._limitMinMaxDateTime(dp_inst, false);
      this._injectTimePicker();
    },

    /*
    * parse the time string from input value or _setTime
    */
    _parseTime: function (timeString, withDate) {
      if (!this.inst) {
        this.inst = $.datepicker._getInst(this.$input[0]);
      }

      if (withDate || !this._defaults.timeOnly) {
        var dp_dateFormat = $.datepicker._get(this.inst, 'dateFormat');
        try {
          var parseRes = parseDateTimeInternal(dp_dateFormat, this._defaults.timeFormat, timeString, $.datepicker._getFormatConfig(this.inst), this._defaults);
          if (!parseRes.timeObj) {
            return false;
          }
          $.extend(this, parseRes.timeObj);
        } catch (err) {
          $.timepicker.log("Error parsing the date/time string: " + err +
                  "\ndate/time string = " + timeString +
                  "\ntimeFormat = " + this._defaults.timeFormat +
                  "\ndateFormat = " + dp_dateFormat);
          return false;
        }
        return true;
      } else {
        var timeObj = $.datepicker.parseTime(this._defaults.timeFormat, timeString, this._defaults);
        if (!timeObj) {
          return false;
        }
        $.extend(this, timeObj);
        return true;
      }
    },

    /*
    * generate and inject html for timepicker into ui datepicker
    */
    _injectTimePicker: function () {
      var $dp = this.inst.dpDiv,
        o = this.inst.settings,
        tp_inst = this,
        litem = '',
        uitem = '',
        show = null,
        max = {},
        gridSize = {},
        size = null,
        i = 0,
        l = 0;

      // Prevent displaying twice
      if ($dp.find("div.ui-timepicker-div").length === 0 && o.showTimepicker) {
        var noDisplay = ' style="display:none;"',
          html = '<div class="ui-timepicker-div' + (o.isRTL ? ' ui-timepicker-rtl' : '') + '"><dl>' + '<dt class="ui_tpicker_time_label"' + ((o.showTime) ? '' : noDisplay) + '>' + o.timeText + '</dt>' +
                '<dd class="ui_tpicker_time"' + ((o.showTime) ? '' : noDisplay) + '></dd>';

        // Create the markup
        for (i = 0, l = this.units.length; i < l; i++) {
          litem = this.units[i];
          uitem = litem.substr(0, 1).toUpperCase() + litem.substr(1);
          show = o['show' + uitem] !== null ? o['show' + uitem] : this.support[litem];

          // Added by Peter Medeiros:
          // - Figure out what the hour/minute/second max should be based on the step values.
          // - Example: if stepMinute is 15, then minMax is 45.
          max[litem] = parseInt((o[litem + 'Max'] - ((o[litem + 'Max'] - o[litem + 'Min']) % o['step' + uitem])), 10);
          gridSize[litem] = 0;

          html += '<dt class="ui_tpicker_' + litem + '_label"' + (show ? '' : noDisplay) + '>' + o[litem + 'Text'] + '</dt>' +
                '<dd class="ui_tpicker_' + litem + '"><div class="ui_tpicker_' + litem + '_slider"' + (show ? '' : noDisplay) + '></div>';

          if (show && o[litem + 'Grid'] > 0) {
            html += '<div style="padding-left: 1px"><table class="ui-tpicker-grid-label"><tr>';

            if (litem === 'hour') {
              for (var h = o[litem + 'Min']; h <= max[litem]; h += parseInt(o[litem + 'Grid'], 10)) {
                gridSize[litem]++;
                var tmph = $.datepicker.formatTime(this.support.ampm ? 'hht' : 'HH', {hour: h}, o);
                html += '<td data-for="' + litem + '">' + tmph + '</td>';
              }
            }
            else {
              for (var m = o[litem + 'Min']; m <= max[litem]; m += parseInt(o[litem + 'Grid'], 10)) {
                gridSize[litem]++;
                html += '<td data-for="' + litem + '">' + ((m < 10) ? '0' : '') + m + '</td>';
              }
            }

            html += '</tr></table></div>';
          }
          html += '</dd>';
        }
        
        // Timezone
        var showTz = o.showTimezone !== null ? o.showTimezone : this.support.timezone;
        html += '<dt class="ui_tpicker_timezone_label"' + (showTz ? '' : noDisplay) + '>' + o.timezoneText + '</dt>';
        html += '<dd class="ui_tpicker_timezone" ' + (showTz ? '' : noDisplay) + '></dd>';

        // Create the elements from string
        html += '</dl></div>';
        var $tp = $(html);

        // if we only want time picker...
        if (o.timeOnly === true) {
          $tp.prepend('<div class="ui-widget-header ui-helper-clearfix ui-corner-all">' + '<div class="ui-datepicker-title">' + o.timeOnlyTitle + '</div>' + '</div>');
          $dp.find('.ui-datepicker-header, .ui-datepicker-calendar').hide();
        }
        
        // add sliders, adjust grids, add events
        for (i = 0, l = tp_inst.units.length; i < l; i++) {
          litem = tp_inst.units[i];
          uitem = litem.substr(0, 1).toUpperCase() + litem.substr(1);
          show = o['show' + uitem] !== null ? o['show' + uitem] : this.support[litem];

          // add the slider
          tp_inst[litem + '_slider'] = tp_inst.control.create(tp_inst, $tp.find('.ui_tpicker_' + litem + '_slider'), litem, tp_inst[litem], o[litem + 'Min'], max[litem], o['step' + uitem]);

          // adjust the grid and add click event
          if (show && o[litem + 'Grid'] > 0) {
            size = 100 * gridSize[litem] * o[litem + 'Grid'] / (max[litem] - o[litem + 'Min']);
            $tp.find('.ui_tpicker_' + litem + ' table').css({
              width: size + "%",
              marginLeft: o.isRTL ? '0' : ((size / (-2 * gridSize[litem])) + "%"),
              marginRight: o.isRTL ? ((size / (-2 * gridSize[litem])) + "%") : '0',
              borderCollapse: 'collapse'
            }).find("td").click(function (e) {
                var $t = $(this),
                  h = $t.html(),
                  n = parseInt(h.replace(/[^0-9]/g), 10),
                  ap = h.replace(/[^apm]/ig),
                  f = $t.data('for'); // loses scope, so we use data-for

                if (f === 'hour') {
                  if (ap.indexOf('p') !== -1 && n < 12) {
                    n += 12;
                  }
                  else {
                    if (ap.indexOf('a') !== -1 && n === 12) {
                      n = 0;
                    }
                  }
                }
                
                tp_inst.control.value(tp_inst, tp_inst[f + '_slider'], litem, n);

                tp_inst._onTimeChange();
                tp_inst._onSelectHandler();
              }).css({
                cursor: 'pointer',
                width: (100 / gridSize[litem]) + '%',
                textAlign: 'center',
                overflow: 'hidden'
              });
          } // end if grid > 0
        } // end for loop

        // Add timezone options
        this.timezone_select = $tp.find('.ui_tpicker_timezone').append('<select></select>').find("select");
        $.fn.append.apply(this.timezone_select,
        $.map(o.timezoneList, function (val, idx) {
          return $("<option />").val(typeof val === "object" ? val.value : val).text(typeof val === "object" ? val.label : val);
        }));
        if (typeof(this.timezone) !== "undefined" && this.timezone !== null && this.timezone !== "") {
          var local_timezone = (new Date(this.inst.selectedYear, this.inst.selectedMonth, this.inst.selectedDay, 12)).getTimezoneOffset() * -1;
          if (local_timezone === this.timezone) {
            selectLocalTimezone(tp_inst);
          } else {
            this.timezone_select.val(this.timezone);
          }
        } else {
          if (typeof(this.hour) !== "undefined" && this.hour !== null && this.hour !== "") {
            this.timezone_select.val(o.timezone);
          } else {
            selectLocalTimezone(tp_inst);
          }
        }
        this.timezone_select.change(function () {
          tp_inst._onTimeChange();
          tp_inst._onSelectHandler();
        });
        // End timezone options
        
        // inject timepicker into datepicker
        var $buttonPanel = $dp.find('.ui-datepicker-buttonpane');
        if ($buttonPanel.length) {
          $buttonPanel.before($tp);
        } else {
          $dp.append($tp);
        }

        this.$timeObj = $tp.find('.ui_tpicker_time');

        if (this.inst !== null) {
          var timeDefined = this.timeDefined;
          this._onTimeChange();
          this.timeDefined = timeDefined;
        }

        // slideAccess integration: http://trentrichardson.com/2011/11/11/jquery-ui-sliders-and-touch-accessibility/
        if (this._defaults.addSliderAccess) {
          var sliderAccessArgs = this._defaults.sliderAccessArgs,
            rtl = this._defaults.isRTL;
          sliderAccessArgs.isRTL = rtl;
            
          setTimeout(function () { // fix for inline mode
            if ($tp.find('.ui-slider-access').length === 0) {
              $tp.find('.ui-slider:visible').sliderAccess(sliderAccessArgs);

              // fix any grids since sliders are shorter
              var sliderAccessWidth = $tp.find('.ui-slider-access:eq(0)').outerWidth(true);
              if (sliderAccessWidth) {
                $tp.find('table:visible').each(function () {
                  var $g = $(this),
                    oldWidth = $g.outerWidth(),
                    oldMarginLeft = $g.css(rtl ? 'marginRight' : 'marginLeft').toString().replace('%', ''),
                    newWidth = oldWidth - sliderAccessWidth,
                    newMarginLeft = ((oldMarginLeft * newWidth) / oldWidth) + '%',
                    css = { width: newWidth, marginRight: 0, marginLeft: 0 };
                  css[rtl ? 'marginRight' : 'marginLeft'] = newMarginLeft;
                  $g.css(css);
                });
              }
            }
          }, 10);
        }
        // end slideAccess integration

        tp_inst._limitMinMaxDateTime(this.inst, true);
      }
    },

    /*
    * This function tries to limit the ability to go outside the
    * min/max date range
    */
    _limitMinMaxDateTime: function (dp_inst, adjustSliders) {
      var o = this._defaults,
        dp_date = new Date(dp_inst.selectedYear, dp_inst.selectedMonth, dp_inst.selectedDay);

      if (!this._defaults.showTimepicker) {
        return;
      } // No time so nothing to check here

      if ($.datepicker._get(dp_inst, 'minDateTime') !== null && $.datepicker._get(dp_inst, 'minDateTime') !== undefined && dp_date) {
        var minDateTime = $.datepicker._get(dp_inst, 'minDateTime'),
          minDateTimeDate = new Date(minDateTime.getFullYear(), minDateTime.getMonth(), minDateTime.getDate(), 0, 0, 0, 0);

        if (this.hourMinOriginal === null || this.minuteMinOriginal === null || this.secondMinOriginal === null || this.millisecMinOriginal === null || this.microsecMinOriginal === null) {
          this.hourMinOriginal = o.hourMin;
          this.minuteMinOriginal = o.minuteMin;
          this.secondMinOriginal = o.secondMin;
          this.millisecMinOriginal = o.millisecMin;
          this.microsecMinOriginal = o.microsecMin;
        }

        if (dp_inst.settings.timeOnly || minDateTimeDate.getTime() === dp_date.getTime()) {
          this._defaults.hourMin = minDateTime.getHours();
          if (this.hour <= this._defaults.hourMin) {
            this.hour = this._defaults.hourMin;
            this._defaults.minuteMin = minDateTime.getMinutes();
            if (this.minute <= this._defaults.minuteMin) {
              this.minute = this._defaults.minuteMin;
              this._defaults.secondMin = minDateTime.getSeconds();
              if (this.second <= this._defaults.secondMin) {
                this.second = this._defaults.secondMin;
                this._defaults.millisecMin = minDateTime.getMilliseconds();
                if (this.millisec <= this._defaults.millisecMin) {
                  this.millisec = this._defaults.millisecMin;
                  this._defaults.microsecMin = minDateTime.getMicroseconds();
                } else {
                  if (this.microsec < this._defaults.microsecMin) {
                    this.microsec = this._defaults.microsecMin;
                  }
                  this._defaults.microsecMin = this.microsecMinOriginal;
                }
              } else {
                this._defaults.millisecMin = this.millisecMinOriginal;
                this._defaults.microsecMin = this.microsecMinOriginal;
              }
            } else {
              this._defaults.secondMin = this.secondMinOriginal;
              this._defaults.millisecMin = this.millisecMinOriginal;
              this._defaults.microsecMin = this.microsecMinOriginal;
            }
          } else {
            this._defaults.minuteMin = this.minuteMinOriginal;
            this._defaults.secondMin = this.secondMinOriginal;
            this._defaults.millisecMin = this.millisecMinOriginal;
            this._defaults.microsecMin = this.microsecMinOriginal;
          }
        } else {
          this._defaults.hourMin = this.hourMinOriginal;
          this._defaults.minuteMin = this.minuteMinOriginal;
          this._defaults.secondMin = this.secondMinOriginal;
          this._defaults.millisecMin = this.millisecMinOriginal;
          this._defaults.microsecMin = this.microsecMinOriginal;
        }
      }

      if ($.datepicker._get(dp_inst, 'maxDateTime') !== null && $.datepicker._get(dp_inst, 'maxDateTime') !== undefined && dp_date) {
        var maxDateTime = $.datepicker._get(dp_inst, 'maxDateTime'),
          maxDateTimeDate = new Date(maxDateTime.getFullYear(), maxDateTime.getMonth(), maxDateTime.getDate(), 0, 0, 0, 0);

        if (this.hourMaxOriginal === null || this.minuteMaxOriginal === null || this.secondMaxOriginal === null || this.millisecMaxOriginal === null) {
          this.hourMaxOriginal = o.hourMax;
          this.minuteMaxOriginal = o.minuteMax;
          this.secondMaxOriginal = o.secondMax;
          this.millisecMaxOriginal = o.millisecMax;
          this.microsecMaxOriginal = o.microsecMax;
        }

        if (dp_inst.settings.timeOnly || maxDateTimeDate.getTime() === dp_date.getTime()) {
          this._defaults.hourMax = maxDateTime.getHours();
          if (this.hour >= this._defaults.hourMax) {
            this.hour = this._defaults.hourMax;
            this._defaults.minuteMax = maxDateTime.getMinutes();
            if (this.minute >= this._defaults.minuteMax) {
              this.minute = this._defaults.minuteMax;
              this._defaults.secondMax = maxDateTime.getSeconds();
              if (this.second >= this._defaults.secondMax) {
                this.second = this._defaults.secondMax;
                this._defaults.millisecMax = maxDateTime.getMilliseconds();
                if (this.millisec >= this._defaults.millisecMax) {
                  this.millisec = this._defaults.millisecMax;
                  this._defaults.microsecMax = maxDateTime.getMicroseconds();
                } else {
                  if (this.microsec > this._defaults.microsecMax) {
                    this.microsec = this._defaults.microsecMax;
                  }
                  this._defaults.microsecMax = this.microsecMaxOriginal;
                }
              } else {
                this._defaults.millisecMax = this.millisecMaxOriginal;
                this._defaults.microsecMax = this.microsecMaxOriginal;
              }
            } else {
              this._defaults.secondMax = this.secondMaxOriginal;
              this._defaults.millisecMax = this.millisecMaxOriginal;
              this._defaults.microsecMax = this.microsecMaxOriginal;
            }
          } else {
            this._defaults.minuteMax = this.minuteMaxOriginal;
            this._defaults.secondMax = this.secondMaxOriginal;
            this._defaults.millisecMax = this.millisecMaxOriginal;
            this._defaults.microsecMax = this.microsecMaxOriginal;
          }
        } else {
          this._defaults.hourMax = this.hourMaxOriginal;
          this._defaults.minuteMax = this.minuteMaxOriginal;
          this._defaults.secondMax = this.secondMaxOriginal;
          this._defaults.millisecMax = this.millisecMaxOriginal;
          this._defaults.microsecMax = this.microsecMaxOriginal;
        }
      }

      if (adjustSliders !== undefined && adjustSliders === true) {
        var hourMax = parseInt((this._defaults.hourMax - ((this._defaults.hourMax - this._defaults.hourMin) % this._defaults.stepHour)), 10),
          minMax = parseInt((this._defaults.minuteMax - ((this._defaults.minuteMax - this._defaults.minuteMin) % this._defaults.stepMinute)), 10),
          secMax = parseInt((this._defaults.secondMax - ((this._defaults.secondMax - this._defaults.secondMin) % this._defaults.stepSecond)), 10),
          millisecMax = parseInt((this._defaults.millisecMax - ((this._defaults.millisecMax - this._defaults.millisecMin) % this._defaults.stepMillisec)), 10),
          microsecMax = parseInt((this._defaults.microsecMax - ((this._defaults.microsecMax - this._defaults.microsecMin) % this._defaults.stepMicrosec)), 10);

        if (this.hour_slider) {
          this.control.options(this, this.hour_slider, 'hour', { min: this._defaults.hourMin, max: hourMax });
          this.control.value(this, this.hour_slider, 'hour', this.hour - (this.hour % this._defaults.stepHour));
        }
        if (this.minute_slider) {
          this.control.options(this, this.minute_slider, 'minute', { min: this._defaults.minuteMin, max: minMax });
          this.control.value(this, this.minute_slider, 'minute', this.minute - (this.minute % this._defaults.stepMinute));
        }
        if (this.second_slider) {
          this.control.options(this, this.second_slider, 'second', { min: this._defaults.secondMin, max: secMax });
          this.control.value(this, this.second_slider, 'second', this.second - (this.second % this._defaults.stepSecond));
        }
        if (this.millisec_slider) {
          this.control.options(this, this.millisec_slider, 'millisec', { min: this._defaults.millisecMin, max: millisecMax });
          this.control.value(this, this.millisec_slider, 'millisec', this.millisec - (this.millisec % this._defaults.stepMillisec));
        }
        if (this.microsec_slider) {
          this.control.options(this, this.microsec_slider, 'microsec', { min: this._defaults.microsecMin, max: microsecMax });
          this.control.value(this, this.microsec_slider, 'microsec', this.microsec - (this.microsec % this._defaults.stepMicrosec));
        }
      }

    },

    /*
    * when a slider moves, set the internal time...
    * on time change is also called when the time is updated in the text field
    */
    _onTimeChange: function () {
      if (!this._defaults.showTimepicker) {
                                return;
      }
      var hour = (this.hour_slider) ? this.control.value(this, this.hour_slider, 'hour') : false,
        minute = (this.minute_slider) ? this.control.value(this, this.minute_slider, 'minute') : false,
        second = (this.second_slider) ? this.control.value(this, this.second_slider, 'second') : false,
        millisec = (this.millisec_slider) ? this.control.value(this, this.millisec_slider, 'millisec') : false,
        microsec = (this.microsec_slider) ? this.control.value(this, this.microsec_slider, 'microsec') : false,
        timezone = (this.timezone_select) ? this.timezone_select.val() : false,
        o = this._defaults,
        pickerTimeFormat = o.pickerTimeFormat || o.timeFormat,
        pickerTimeSuffix = o.pickerTimeSuffix || o.timeSuffix;

      if (typeof(hour) === 'object') {
        hour = false;
      }
      if (typeof(minute) === 'object') {
        minute = false;
      }
      if (typeof(second) === 'object') {
        second = false;
      }
      if (typeof(millisec) === 'object') {
        millisec = false;
      }
      if (typeof(microsec) === 'object') {
        microsec = false;
      }
      if (typeof(timezone) === 'object') {
        timezone = false;
      }

      if (hour !== false) {
        hour = parseInt(hour, 10);
      }
      if (minute !== false) {
        minute = parseInt(minute, 10);
      }
      if (second !== false) {
        second = parseInt(second, 10);
      }
      if (millisec !== false) {
        millisec = parseInt(millisec, 10);
      }
      if (microsec !== false) {
        microsec = parseInt(microsec, 10);
      }

      var ampm = o[hour < 12 ? 'amNames' : 'pmNames'][0];

      // If the update was done in the input field, the input field should not be updated.
      // If the update was done using the sliders, update the input field.
      var hasChanged = (hour !== this.hour || minute !== this.minute || second !== this.second || millisec !== this.millisec || microsec !== this.microsec || 
          (this.ampm.length > 0 && (hour < 12) !== ($.inArray(this.ampm.toUpperCase(), this.amNames) !== -1)) || (this.timezone !== null && timezone !== this.timezone));

      if (hasChanged) {

        if (hour !== false) {
          this.hour = hour;
        }
        if (minute !== false) {
          this.minute = minute;
        }
        if (second !== false) {
          this.second = second;
        }
        if (millisec !== false) {
          this.millisec = millisec;
        }
        if (microsec !== false) {
          this.microsec = microsec;
        }
        if (timezone !== false) {
          this.timezone = timezone;
        }

        if (!this.inst) {
          this.inst = $.datepicker._getInst(this.$input[0]);
        }

        this._limitMinMaxDateTime(this.inst, true);
      }
      if (this.support.ampm) {
        this.ampm = ampm;
      }

      // Updates the time within the timepicker
      this.formattedTime = $.datepicker.formatTime(o.timeFormat, this, o);
      if (this.$timeObj) {
        if (pickerTimeFormat === o.timeFormat) {
          this.$timeObj.text(this.formattedTime + pickerTimeSuffix);
        }
        else {
          this.$timeObj.text($.datepicker.formatTime(pickerTimeFormat, this, o) + pickerTimeSuffix);
        }
      }

      this.timeDefined = true;
      if (hasChanged) {
        this._updateDateTime();
      }
    },

    /*
    * call custom onSelect.
    * bind to sliders slidestop, and grid click.
    */
    _onSelectHandler: function () {
      var onSelect = this._defaults.onSelect || this.inst.settings.onSelect;
      var inputEl = this.$input ? this.$input[0] : null;
      if (onSelect && inputEl) {
        onSelect.apply(inputEl, [this.formattedDateTime, this]);
      }
    },

    /*
    * update our input with the new date time..
    */
    _updateDateTime: function (dp_inst) {
      dp_inst = this.inst || dp_inst;
      var dtTmp = (dp_inst.currentYear > 0? 
              new Date(dp_inst.currentYear, dp_inst.currentMonth, dp_inst.currentDay) : 
              new Date(dp_inst.selectedYear, dp_inst.selectedMonth, dp_inst.selectedDay)),
        dt = $.datepicker._daylightSavingAdjust(dtTmp),
        //dt = $.datepicker._daylightSavingAdjust(new Date(dp_inst.selectedYear, dp_inst.selectedMonth, dp_inst.selectedDay)),
        //dt = $.datepicker._daylightSavingAdjust(new Date(dp_inst.currentYear, dp_inst.currentMonth, dp_inst.currentDay)),
        dateFmt = $.datepicker._get(dp_inst, 'dateFormat'),
        formatCfg = $.datepicker._getFormatConfig(dp_inst),
        timeAvailable = dt !== null && this.timeDefined;
      this.formattedDate = $.datepicker.formatDate(dateFmt, (dt === null ? new Date() : dt), formatCfg);
      var formattedDateTime = this.formattedDate;
      
      // if a slider was changed but datepicker doesn't have a value yet, set it
      if (dp_inst.lastVa === "") {
                dp_inst.currentYear = dp_inst.selectedYear;
                dp_inst.currentMonth = dp_inst.selectedMonth;
                dp_inst.currentDay = dp_inst.selectedDay;
            }

      /*
      * remove following lines to force every changes in date picker to change the input value
      * Bug descriptions: when an input field has a default value, and click on the field to pop up the date picker. 
      * If the user manually empty the value in the input field, the date picker will never change selected value.
      */
      //if (dp_inst.lastVal !== undefined && (dp_inst.lastVal.length > 0 && this.$input.val().length === 0)) {
      //  return;
      //}

      if (this._defaults.timeOnly === true) {
        formattedDateTime = this.formattedTime;
      } else if (this._defaults.timeOnly !== true && (this._defaults.alwaysSetTime || timeAvailable)) {
        formattedDateTime += this._defaults.separator + this.formattedTime + this._defaults.timeSuffix;
      }

      this.formattedDateTime = formattedDateTime;

      if (!this._defaults.showTimepicker) {
        this.$input.val(this.formattedDate);
      } else if (this.$altInput && this._defaults.timeOnly === false && this._defaults.altFieldTimeOnly === true) {
        this.$altInput.val(this.formattedTime);
        this.$input.val(this.formattedDate);
      } else if (this.$altInput) {
        this.$input.val(formattedDateTime);
        var altFormattedDateTime = '',
          altSeparator = this._defaults.altSeparator ? this._defaults.altSeparator : this._defaults.separator,
          altTimeSuffix = this._defaults.altTimeSuffix ? this._defaults.altTimeSuffix : this._defaults.timeSuffix;
        
        if (!this._defaults.timeOnly) {
          if (this._defaults.altFormat) {
            altFormattedDateTime = $.datepicker.formatDate(this._defaults.altFormat, (dt === null ? new Date() : dt), formatCfg);
          }
          else {
            altFormattedDateTime = this.formattedDate;
          }

          if (altFormattedDateTime) {
            altFormattedDateTime += altSeparator;
          }
        }

        if (this._defaults.altTimeFormat) {
          altFormattedDateTime += $.datepicker.formatTime(this._defaults.altTimeFormat, this, this._defaults) + altTimeSuffix;
        }
        else {
          altFormattedDateTime += this.formattedTime + altTimeSuffix;
        }
        this.$altInput.val(altFormattedDateTime);
      } else {
        this.$input.val(formattedDateTime);
      }

      this.$input.trigger("change");
    },

    _onFocus: function () {
      if (!this.$input.val() && this._defaults.defaultValue) {
        this.$input.val(this._defaults.defaultValue);
        var inst = $.datepicker._getInst(this.$input.get(0)),
          tp_inst = $.datepicker._get(inst, 'timepicker');
        if (tp_inst) {
          if (tp_inst._defaults.timeOnly && (inst.input.val() !== inst.lastVal)) {
            try {
              $.datepicker._updateDatepicker(inst);
            } catch (err) {
              $.timepicker.log(err);
            }
          }
        }
      }
    },

    /*
    * Small abstraction to control types
    * We can add more, just be sure to follow the pattern: create, options, value
    */
    _controls: {
      // slider methods
      slider: {
        create: function (tp_inst, obj, unit, val, min, max, step) {
          var rtl = tp_inst._defaults.isRTL; // if rtl go -60->0 instead of 0->60
          return obj.prop('slide', null).slider({
            orientation: "horizontal",
            value: rtl ? val * -1 : val,
            min: rtl ? max * -1 : min,
            max: rtl ? min * -1 : max,
            step: step,
            slide: function (event, ui) {
              tp_inst.control.value(tp_inst, $(this), unit, rtl ? ui.value * -1 : ui.value);
              tp_inst._onTimeChange();
            },
            stop: function (event, ui) {
              tp_inst._onSelectHandler();
            }
          }); 
        },
        options: function (tp_inst, obj, unit, opts, val) {
          if (tp_inst._defaults.isRTL) {
            if (typeof(opts) === 'string') {
              if (opts === 'min' || opts === 'max') {
                if (val !== undefined) {
                  return obj.slider(opts, val * -1);
                }
                return Math.abs(obj.slider(opts));
              }
              return obj.slider(opts);
            }
            var min = opts.min, 
              max = opts.max;
            opts.min = opts.max = null;
            if (min !== undefined) {
              opts.max = min * -1;
            }
            if (max !== undefined) {
              opts.min = max * -1;
            }
            return obj.slider(opts);
          }
          if (typeof(opts) === 'string' && val !== undefined) {
            return obj.slider(opts, val);
          }
          return obj.slider(opts);
        },
        value: function (tp_inst, obj, unit, val) {
          if (tp_inst._defaults.isRTL) {
            if (val !== undefined) {
              return obj.slider('value', val * -1);
            }
            return Math.abs(obj.slider('value'));
          }
          if (val !== undefined) {
            return obj.slider('value', val);
          }
          return obj.slider('value');
        }
      },
      // select methods
      select: {
        create: function (tp_inst, obj, unit, val, min, max, step) {
          var sel = '<select class="ui-timepicker-select" data-unit="' + unit + '" data-min="' + min + '" data-max="' + max + '" data-step="' + step + '">',
            format = tp_inst._defaults.pickerTimeFormat || tp_inst._defaults.timeFormat;

          for (var i = min; i <= max; i += step) {
            sel += '<option value="' + i + '"' + (i === val ? ' selected' : '') + '>';
            if (unit === 'hour') {
              sel += $.datepicker.formatTime($.trim(format.replace(/[^ht ]/ig, '')), {hour: i}, tp_inst._defaults);
            }
            else if (unit === 'millisec' || unit === 'microsec' || i >= 10) { sel += i; }
            else {sel += '0' + i.toString(); }
            sel += '</option>';
          }
          sel += '</select>';

          obj.children('select').remove();

          $(sel).appendTo(obj).change(function (e) {
            tp_inst._onTimeChange();
            tp_inst._onSelectHandler();
          });

          return obj;
        },
        options: function (tp_inst, obj, unit, opts, val) {
          var o = {},
            $t = obj.children('select');
          if (typeof(opts) === 'string') {
            if (val === undefined) {
              return $t.data(opts);
            }
            o[opts] = val;  
          }
          else { o = opts; }
          return tp_inst.control.create(tp_inst, obj, $t.data('unit'), $t.val(), o.min || $t.data('min'), o.max || $t.data('max'), o.step || $t.data('step'));
        },
        value: function (tp_inst, obj, unit, val) {
          var $t = obj.children('select');
          if (val !== undefined) {
            return $t.val(val);
          }
          return $t.val();
        }
      }
    } // end _controls

  });

  $.fn.extend({
    /*
    * shorthand just to use timepicker.
    */
    timepicker: function (o) {
      o = o || {};
      var tmp_args = Array.prototype.slice.call(arguments);

      if (typeof o === 'object') {
        tmp_args[0] = $.extend(o, {
          timeOnly: true
        });
      }

      return $(this).each(function () {
        $.fn.datetimepicker.apply($(this), tmp_args);
      });
    },

    /*
    * extend timepicker to datepicker
    */
    datetimepicker: function (o) {
      o = o || {};
      var tmp_args = arguments;

      if (typeof(o) === 'string') {
        if (o === 'getDate') {
          return $.fn.datepicker.apply($(this[0]), tmp_args);
        } else {
          return this.each(function () {
            var $t = $(this);
            $t.datepicker.apply($t, tmp_args);
          });
        }
      } else {
        return this.each(function () {
          var $t = $(this);
          $t.datepicker($.timepicker._newInst($t, o)._defaults);
        });
      }
    }
  });

  /*
  * Public Utility to parse date and time
  */
  $.datepicker.parseDateTime = function (dateFormat, timeFormat, dateTimeString, dateSettings, timeSettings) {
    var parseRes = parseDateTimeInternal(dateFormat, timeFormat, dateTimeString, dateSettings, timeSettings);
    if (parseRes.timeObj) {
      var t = parseRes.timeObj;
      parseRes.date.setHours(t.hour, t.minute, t.second, t.millisec);
      parseRes.date.setMicroseconds(t.microsec);
    }

    return parseRes.date;
  };

  /*
  * Public utility to parse time
  */
  $.datepicker.parseTime = function (timeFormat, timeString, options) {
    var o = extendRemove(extendRemove({}, $.timepicker._defaults), options || {}),
      iso8601 = (timeFormat.replace(/\'.*?\'/g, '').indexOf('Z') !== -1);

    // Strict parse requires the timeString to match the timeFormat exactly
    var strictParse = function (f, s, o) {

      // pattern for standard and localized AM/PM markers
      var getPatternAmpm = function (amNames, pmNames) {
        var markers = [];
        if (amNames) {
          $.merge(markers, amNames);
        }
        if (pmNames) {
          $.merge(markers, pmNames);
        }
        markers = $.map(markers, function (val) {
          return val.replace(/[.*+?|()\[\]{}\\]/g, '\\$&');
        });
        return '(' + markers.join('|') + ')?';
      };

      // figure out position of time elements.. cause js cant do named captures
      var getFormatPositions = function (timeFormat) {
        var finds = timeFormat.toLowerCase().match(/(h{1,2}|m{1,2}|s{1,2}|l{1}|c{1}|t{1,2}|z|'.*?')/g),
          orders = {
            h: -1,
            m: -1,
            s: -1,
            l: -1,
            c: -1,
            t: -1,
            z: -1
          };

        if (finds) {
          for (var i = 0; i < finds.length; i++) {
            if (orders[finds[i].toString().charAt(0)] === -1) {
              orders[finds[i].toString().charAt(0)] = i + 1;
            }
          }
        }
        return orders;
      };

      var regstr = '^' + f.toString()
          .replace(/([hH]{1,2}|mm?|ss?|[tT]{1,2}|[zZ]|[lc]|'.*?')/g, function (match) {
              var ml = match.length;
              switch (match.charAt(0).toLowerCase()) {
              case 'h':
                return ml === 1 ? '(\\d?\\d)' : '(\\d{' + ml + '})';
              case 'm':
                return ml === 1 ? '(\\d?\\d)' : '(\\d{' + ml + '})';
              case 's':
                return ml === 1 ? '(\\d?\\d)' : '(\\d{' + ml + '})';
              case 'l':
                return '(\\d?\\d?\\d)';
              case 'c':
                return '(\\d?\\d?\\d)';
              case 'z':
                return '(z|[-+]\\d\\d:?\\d\\d|\\S+)?';
              case 't':
                return getPatternAmpm(o.amNames, o.pmNames);
              default:    // literal escaped in quotes
                return '(' + match.replace(/\'/g, "").replace(/(\.|\$|\^|\\|\/|\(|\)|\[|\]|\?|\+|\*)/g, function (m) { return "\\" + m; }) + ')?';
              }
            })
          .replace(/\s/g, '\\s?') +
          o.timeSuffix + '$',
        order = getFormatPositions(f),
        ampm = '',
        treg;

      treg = s.match(new RegExp(regstr, 'i'));

      var resTime = {
        hour: 0,
        minute: 0,
        second: 0,
        millisec: 0,
        microsec: 0
      };

      if (treg) {
        if (order.t !== -1) {
          if (treg[order.t] === undefined || treg[order.t].length === 0) {
            ampm = '';
            resTime.ampm = '';
          } else {
            ampm = $.inArray(treg[order.t].toUpperCase(), o.amNames) !== -1 ? 'AM' : 'PM';
            resTime.ampm = o[ampm === 'AM' ? 'amNames' : 'pmNames'][0];
          }
        }

        if (order.h !== -1) {
          if (ampm === 'AM' && treg[order.h] === '12') {
            resTime.hour = 0; // 12am = 0 hour
          } else {
            if (ampm === 'PM' && treg[order.h] !== '12') {
              resTime.hour = parseInt(treg[order.h], 10) + 12; // 12pm = 12 hour, any other pm = hour + 12
            } else {
              resTime.hour = Number(treg[order.h]);
            }
          }
        }

        if (order.m !== -1) {
          resTime.minute = Number(treg[order.m]);
        }
        if (order.s !== -1) {
          resTime.second = Number(treg[order.s]);
        }
        if (order.l !== -1) {
          resTime.millisec = Number(treg[order.l]);
        }
        if (order.c !== -1) {
          resTime.microsec = Number(treg[order.c]);
        }
        if (order.z !== -1 && treg[order.z] !== undefined) {
          resTime.timezone = $.timepicker.timezoneOffsetNumber(treg[order.z]);
        }


        return resTime;
      }
      return false;
    };// end strictParse

    // First try JS Date, if that fails, use strictParse
    var looseParse = function (f, s, o) {
      try {
        var d = new Date('2012-01-01 ' + s);
        if (isNaN(d.getTime())) {
          d = new Date('2012-01-01T' + s);
          if (isNaN(d.getTime())) {
            d = new Date('01/01/2012 ' + s);
            if (isNaN(d.getTime())) {
              throw "Unable to parse time with native Date: " + s;
            }
          }
        }

        return {
          hour: d.getHours(),
          minute: d.getMinutes(),
          second: d.getSeconds(),
          millisec: d.getMilliseconds(),
          microsec: d.getMicroseconds(),
          timezone: d.getTimezoneOffset() * -1
        };
      }
      catch (err) {
        try {
          return strictParse(f, s, o);
        }
        catch (err2) {
          $.timepicker.log("Unable to parse \ntimeString: " + s + "\ntimeFormat: " + f);
        }       
      }
      return false;
    }; // end looseParse
    
    if (typeof o.parse === "function") {
      return o.parse(timeFormat, timeString, o);
    }
    if (o.parse === 'loose') {
      return looseParse(timeFormat, timeString, o);
    }
    return strictParse(timeFormat, timeString, o);
  };

  /**
   * Public utility to format the time
   * @param {string} format format of the time
   * @param {Object} time Object not a Date for timezones
   * @param {Object} [options] essentially the regional[].. amNames, pmNames, ampm
   * @returns {string} the formatted time
   */
  $.datepicker.formatTime = function (format, time, options) {
    options = options || {};
    options = $.extend({}, $.timepicker._defaults, options);
    time = $.extend({
      hour: 0,
      minute: 0,
      second: 0,
      millisec: 0,
      microsec: 0,
      timezone: null
    }, time);

    var tmptime = format,
      ampmName = options.amNames[0],
      hour = parseInt(time.hour, 10);

    if (hour > 11) {
      ampmName = options.pmNames[0];
    }

    tmptime = tmptime.replace(/(?:HH?|hh?|mm?|ss?|[tT]{1,2}|[zZ]|[lc]|'.*?')/g, function (match) {
      switch (match) {
      case 'HH':
        return ('0' + hour).slice(-2);
      case 'H':
        return hour;
      case 'hh':
        return ('0' + convert24to12(hour)).slice(-2);
      case 'h':
        return convert24to12(hour);
      case 'mm':
        return ('0' + time.minute).slice(-2);
      case 'm':
        return time.minute;
      case 'ss':
        return ('0' + time.second).slice(-2);
      case 's':
        return time.second;
      case 'l':
        return ('00' + time.millisec).slice(-3);
      case 'c':
        return ('00' + time.microsec).slice(-3);
      case 'z':
        return $.timepicker.timezoneOffsetString(time.timezone === null ? options.timezone : time.timezone, false);
      case 'Z':
        return $.timepicker.timezoneOffsetString(time.timezone === null ? options.timezone : time.timezone, true);
      case 'T':
        return ampmName.charAt(0).toUpperCase();
      case 'TT':
        return ampmName.toUpperCase();
      case 't':
        return ampmName.charAt(0).toLowerCase();
      case 'tt':
        return ampmName.toLowerCase();
      default:
        return match.replace(/'/g, "");
      }
    });

    return tmptime;
  };

  /*
  * the bad hack :/ override datepicker so it doesn't close on select
  // inspired: http://stackoverflow.com/questions/1252512/jquery-datepicker-prevent-closing-picker-when-clicking-a-date/1762378#1762378
  */
  $.datepicker._base_selectDate = $.datepicker._selectDate;
  $.datepicker._selectDate = function (id, dateStr) {
    var inst = this._getInst($(id)[0]),
      tp_inst = this._get(inst, 'timepicker');

    if (tp_inst) {
      tp_inst._limitMinMaxDateTime(inst, true);
      inst.inline = inst.stay_open = true;
      //This way the onSelect handler called from calendarpicker get the full dateTime
      this._base_selectDate(id, dateStr);
      inst.inline = inst.stay_open = false;
      this._notifyChange(inst);
      this._updateDatepicker(inst);
    } else {
      this._base_selectDate(id, dateStr);
    }
  };

  /*
  * second bad hack :/ override datepicker so it triggers an event when changing the input field
  * and does not redraw the datepicker on every selectDate event
  */
  $.datepicker._base_updateDatepicker = $.datepicker._updateDatepicker;
  $.datepicker._updateDatepicker = function (inst) {

    // don't popup the datepicker if there is another instance already opened
    var input = inst.input[0];
    if ($.datepicker._curInst && $.datepicker._curInst !== inst && $.datepicker._datepickerShowing && $.datepicker._lastInput !== input) {
      return;
    }

    if (typeof(inst.stay_open) !== 'boolean' || inst.stay_open === false) {

      this._base_updateDatepicker(inst);

      // Reload the time control when changing something in the input text field.
      var tp_inst = this._get(inst, 'timepicker');
      if (tp_inst) {
        tp_inst._addTimePicker(inst);
      }
    }
  };

  /*
  * third bad hack :/ override datepicker so it allows spaces and colon in the input field
  */
  $.datepicker._base_doKeyPress = $.datepicker._doKeyPress;
  $.datepicker._doKeyPress = function (event) {
    var inst = $.datepicker._getInst(event.target),
      tp_inst = $.datepicker._get(inst, 'timepicker');

    if (tp_inst) {
      if ($.datepicker._get(inst, 'constrainInput')) {
        var ampm = tp_inst.support.ampm,
          tz = tp_inst._defaults.showTimezone !== null ? tp_inst._defaults.showTimezone : tp_inst.support.timezone,
          dateChars = $.datepicker._possibleChars($.datepicker._get(inst, 'dateFormat')),
          datetimeChars = tp_inst._defaults.timeFormat.toString()
                      .replace(/[hms]/g, '')
                      .replace(/TT/g, ampm ? 'APM' : '')
                      .replace(/Tt/g, ampm ? 'AaPpMm' : '')
                      .replace(/tT/g, ampm ? 'AaPpMm' : '')
                      .replace(/T/g, ampm ? 'AP' : '')
                      .replace(/tt/g, ampm ? 'apm' : '')
                      .replace(/t/g, ampm ? 'ap' : '') + 
                      " " + tp_inst._defaults.separator + 
                      tp_inst._defaults.timeSuffix + 
                      (tz ? tp_inst._defaults.timezoneList.join('') : '') + 
                      (tp_inst._defaults.amNames.join('')) + (tp_inst._defaults.pmNames.join('')) + 
                      dateChars,
          chr = String.fromCharCode(event.charCode === undefined ? event.keyCode : event.charCode);
        return event.ctrlKey || (chr < ' ' || !dateChars || datetimeChars.indexOf(chr) > -1);
      }
    }

    return $.datepicker._base_doKeyPress(event);
  };

  /*
  * Fourth bad hack :/ override _updateAlternate function used in inline mode to init altField
  * Update any alternate field to synchronise with the main field.
  */
  $.datepicker._base_updateAlternate = $.datepicker._updateAlternate;
  $.datepicker._updateAlternate = function (inst) {
    var tp_inst = this._get(inst, 'timepicker');
    if (tp_inst) {
      var altField = tp_inst._defaults.altField;
      if (altField) { // update alternate field too
        var altFormat = tp_inst._defaults.altFormat || tp_inst._defaults.dateFormat,
          date = this._getDate(inst),
          formatCfg = $.datepicker._getFormatConfig(inst),
          altFormattedDateTime = '', 
          altSeparator = tp_inst._defaults.altSeparator ? tp_inst._defaults.altSeparator : tp_inst._defaults.separator, 
          altTimeSuffix = tp_inst._defaults.altTimeSuffix ? tp_inst._defaults.altTimeSuffix : tp_inst._defaults.timeSuffix,
          altTimeFormat = tp_inst._defaults.altTimeFormat !== null ? tp_inst._defaults.altTimeFormat : tp_inst._defaults.timeFormat;
        
        altFormattedDateTime += $.datepicker.formatTime(altTimeFormat, tp_inst, tp_inst._defaults) + altTimeSuffix;
        if (!tp_inst._defaults.timeOnly && !tp_inst._defaults.altFieldTimeOnly && date !== null) {
          if (tp_inst._defaults.altFormat) {
            altFormattedDateTime = $.datepicker.formatDate(tp_inst._defaults.altFormat, date, formatCfg) + altSeparator + altFormattedDateTime;
          }
          else {
            altFormattedDateTime = tp_inst.formattedDate + altSeparator + altFormattedDateTime;
          }
        }
        $(altField).val(altFormattedDateTime);
      }
    }
    else {
      $.datepicker._base_updateAlternate(inst);
    }
  };

  /*
  * Override key up event to sync manual input changes.
  */
  $.datepicker._base_doKeyUp = $.datepicker._doKeyUp;
  $.datepicker._doKeyUp = function (event) {
    var inst = $.datepicker._getInst(event.target),
      tp_inst = $.datepicker._get(inst, 'timepicker');

    if (tp_inst) {
      if (tp_inst._defaults.timeOnly && (inst.input.val() !== inst.lastVal)) {
        try {
          $.datepicker._updateDatepicker(inst);
        } catch (err) {
          $.timepicker.log(err);
        }
      }
    }

    return $.datepicker._base_doKeyUp(event);
  };

  /*
  * override "Today" button to also grab the time.
  */
  $.datepicker._base_gotoToday = $.datepicker._gotoToday;
  $.datepicker._gotoToday = function (id) {
    var inst = this._getInst($(id)[0]),
      $dp = inst.dpDiv;
    this._base_gotoToday(id);
    var tp_inst = this._get(inst, 'timepicker');
    selectLocalTimezone(tp_inst);
    var now = new Date();
    this._setTime(inst, now);
    $('.ui-datepicker-today', $dp).click();
  };

  /*
  * Disable & enable the Time in the datetimepicker
  */
  $.datepicker._disableTimepickerDatepicker = function (target) {
    var inst = this._getInst(target);
    if (!inst) {
      return;
    }

    var tp_inst = this._get(inst, 'timepicker');
    $(target).datepicker('getDate'); // Init selected[Year|Month|Day]
    if (tp_inst) {
      inst.settings.showTimepicker = false;
      tp_inst._defaults.showTimepicker = false;
      tp_inst._updateDateTime(inst);
    }
  };

  $.datepicker._enableTimepickerDatepicker = function (target) {
    var inst = this._getInst(target);
    if (!inst) {
      return;
    }

    var tp_inst = this._get(inst, 'timepicker');
    $(target).datepicker('getDate'); // Init selected[Year|Month|Day]
    if (tp_inst) {
      inst.settings.showTimepicker = true;
      tp_inst._defaults.showTimepicker = true;
      tp_inst._addTimePicker(inst); // Could be disabled on page load
      tp_inst._updateDateTime(inst);
    }
  };

  /*
  * Create our own set time function
  */
  $.datepicker._setTime = function (inst, date) {
    var tp_inst = this._get(inst, 'timepicker');
    if (tp_inst) {
      var defaults = tp_inst._defaults;

      // calling _setTime with no date sets time to defaults
      tp_inst.hour = date ? date.getHours() : defaults.hour;
      tp_inst.minute = date ? date.getMinutes() : defaults.minute;
      tp_inst.second = date ? date.getSeconds() : defaults.second;
      tp_inst.millisec = date ? date.getMilliseconds() : defaults.millisec;
      tp_inst.microsec = date ? date.getMicroseconds() : defaults.microsec;

      //check if within min/max times.. 
      tp_inst._limitMinMaxDateTime(inst, true);

      tp_inst._onTimeChange();
      tp_inst._updateDateTime(inst);
    }
  };

  /*
  * Create new public method to set only time, callable as $().datepicker('setTime', date)
  */
  $.datepicker._setTimeDatepicker = function (target, date, withDate) {
    var inst = this._getInst(target);
    if (!inst) {
      return;
    }

    var tp_inst = this._get(inst, 'timepicker');

    if (tp_inst) {
      this._setDateFromField(inst);
      var tp_date;
      if (date) {
        if (typeof date === "string") {
          tp_inst._parseTime(date, withDate);
          tp_date = new Date();
          tp_date.setHours(tp_inst.hour, tp_inst.minute, tp_inst.second, tp_inst.millisec);
          tp_date.setMicroseconds(tp_inst.microsec);
        } else {
          tp_date = new Date(date.getTime());
          tp_date.setMicroseconds(date.getMicroseconds());
        }
        if (tp_date.toString() === 'Invalid Date') {
          tp_date = undefined;
        }
        this._setTime(inst, tp_date);
      }
    }

  };

  /*
  * override setDate() to allow setting time too within Date object
  */
  $.datepicker._base_setDateDatepicker = $.datepicker._setDateDatepicker;
  $.datepicker._setDateDatepicker = function (target, date) {
    var inst = this._getInst(target);
    if (!inst) {
      return;
    }

    if (typeof(date) === 'string') {
      date = new Date(date);
      if (!date.getTime()) {
        $.timepicker.log("Error creating Date object from string.");
      }
    }

    var tp_inst = this._get(inst, 'timepicker');
    var tp_date;
    if (date instanceof Date) {
      tp_date = new Date(date.getTime());
      tp_date.setMicroseconds(date.getMicroseconds());
    } else {
      tp_date = date;
    }
    
    // This is important if you are using the timezone option, javascript's Date 
    // object will only return the timezone offset for the current locale, so we 
    // adjust it accordingly.  If not using timezone option this won't matter..
    // If a timezone is different in tp, keep the timezone as is
    if (tp_inst) {
      // look out for DST if tz wasn't specified
      if (!tp_inst.support.timezone && tp_inst._defaults.timezone === null) {
        tp_inst.timezone = tp_date.getTimezoneOffset() * -1;
      }
      date = $.timepicker.timezoneAdjust(date, tp_inst.timezone);
      tp_date = $.timepicker.timezoneAdjust(tp_date, tp_inst.timezone);
    }

    this._updateDatepicker(inst);
    this._base_setDateDatepicker.apply(this, arguments);
    this._setTimeDatepicker(target, tp_date, true);
  };

  /*
  * override getDate() to allow getting time too within Date object
  */
  $.datepicker._base_getDateDatepicker = $.datepicker._getDateDatepicker;
  $.datepicker._getDateDatepicker = function (target, noDefault) {
    var inst = this._getInst(target);
    if (!inst) {
      return;
    }

    var tp_inst = this._get(inst, 'timepicker');

    if (tp_inst) {
      // if it hasn't yet been defined, grab from field
      if (inst.lastVal === undefined) {
        this._setDateFromField(inst, noDefault);
      }

      var date = this._getDate(inst);
      if (date && tp_inst._parseTime($(target).val(), tp_inst.timeOnly)) {
        date.setHours(tp_inst.hour, tp_inst.minute, tp_inst.second, tp_inst.millisec);
        date.setMicroseconds(tp_inst.microsec);

        // This is important if you are using the timezone option, javascript's Date 
        // object will only return the timezone offset for the current locale, so we 
        // adjust it accordingly.  If not using timezone option this won't matter..
        if (tp_inst.timezone != null) {
          // look out for DST if tz wasn't specified
          if (!tp_inst.support.timezone && tp_inst._defaults.timezone === null) {
            tp_inst.timezone = date.getTimezoneOffset() * -1;
          }
          date = $.timepicker.timezoneAdjust(date, tp_inst.timezone);
        }
      }
      return date;
    }
    return this._base_getDateDatepicker(target, noDefault);
  };

  /*
  * override parseDate() because UI 1.8.14 throws an error about "Extra characters"
  * An option in datapicker to ignore extra format characters would be nicer.
  */
  $.datepicker._base_parseDate = $.datepicker.parseDate;
  $.datepicker.parseDate = function (format, value, settings) {
    var date;
    try {
      date = this._base_parseDate(format, value, settings);
    } catch (err) {
      // Hack!  The error message ends with a colon, a space, and
      // the "extra" characters.  We rely on that instead of
      // attempting to perfectly reproduce the parsing algorithm.
      if (err.indexOf(":") >= 0) {
        date = this._base_parseDate(format, value.substring(0, value.length - (err.length - err.indexOf(':') - 2)), settings);
        $.timepicker.log("Error parsing the date string: " + err + "\ndate string = " + value + "\ndate format = " + format);
      } else {
        throw err;
      }
    }
    return date;
  };

  /*
  * override formatDate to set date with time to the input
  */
  $.datepicker._base_formatDate = $.datepicker._formatDate;
  $.datepicker._formatDate = function (inst, day, month, year) {
    var tp_inst = this._get(inst, 'timepicker');
    if (tp_inst) {
      tp_inst._updateDateTime(inst);
      return tp_inst.$input.val();
    }
    return this._base_formatDate(inst);
  };

  /*
  * override options setter to add time to maxDate(Time) and minDate(Time). MaxDate
  */
  $.datepicker._base_optionDatepicker = $.datepicker._optionDatepicker;
  $.datepicker._optionDatepicker = function (target, name, value) {
    var inst = this._getInst(target),
      name_clone;
    if (!inst) {
      return null;
    }

    var tp_inst = this._get(inst, 'timepicker');
    if (tp_inst) {
      var min = null,
        max = null,
        onselect = null,
        overrides = tp_inst._defaults.evnts,
        fns = {},
        prop;
      if (typeof name === 'string') { // if min/max was set with the string
        if (name === 'minDate' || name === 'minDateTime') {
          min = value;
        } else if (name === 'maxDate' || name === 'maxDateTime') {
          max = value;
        } else if (name === 'onSelect') {
          onselect = value;
        } else if (overrides.hasOwnProperty(name)) {
          if (typeof (value) === 'undefined') {
            return overrides[name];
          }
          fns[name] = value;
          name_clone = {}; //empty results in exiting function after overrides updated
        }
      } else if (typeof name === 'object') { //if min/max was set with the JSON
        if (name.minDate) {
          min = name.minDate;
        } else if (name.minDateTime) {
          min = name.minDateTime;
        } else if (name.maxDate) {
          max = name.maxDate;
        } else if (name.maxDateTime) {
          max = name.maxDateTime;
        }
        for (prop in overrides) {
          if (overrides.hasOwnProperty(prop) && name[prop]) {
            fns[prop] = name[prop];
          }
        }
      }
      for (prop in fns) {
        if (fns.hasOwnProperty(prop)) {
          overrides[prop] = fns[prop];
          if (!name_clone) { name_clone = $.extend({}, name); }
          delete name_clone[prop];
        }
      }
      if (name_clone && isEmptyObject(name_clone)) { return; }
      if (min) { //if min was set
        if (min === 0) {
          min = new Date();
        } else {
          min = new Date(min);
        }
        tp_inst._defaults.minDate = min;
        tp_inst._defaults.minDateTime = min;
      } else if (max) { //if max was set
        if (max === 0) {
          max = new Date();
        } else {
          max = new Date(max);
        }
        tp_inst._defaults.maxDate = max;
        tp_inst._defaults.maxDateTime = max;
      } else if (onselect) {
        tp_inst._defaults.onSelect = onselect;
      }
    }
    if (value === undefined) {
      return this._base_optionDatepicker.call($.datepicker, target, name);
    }
    return this._base_optionDatepicker.call($.datepicker, target, name_clone || name, value);
  };
  
  /*
  * jQuery isEmptyObject does not check hasOwnProperty - if someone has added to the object prototype,
  * it will return false for all objects
  */
  var isEmptyObject = function (obj) {
    var prop;
    for (prop in obj) {
      if (obj.hasOwnProperty(prop)) {
        return false;
      }
    }
    return true;
  };

  /*
  * jQuery extend now ignores nulls!
  */
  var extendRemove = function (target, props) {
    $.extend(target, props);
    for (var name in props) {
      if (props[name] === null || props[name] === undefined) {
        target[name] = props[name];
      }
    }
    return target;
  };

  /*
  * Determine by the time format which units are supported
  * Returns an object of booleans for each unit
  */
  var detectSupport = function (timeFormat) {
    var tf = timeFormat.replace(/'.*?'/g, '').toLowerCase(), // removes literals
      isIn = function (f, t) { // does the format contain the token?
          return f.indexOf(t) !== -1 ? true : false;
        };
    return {
        hour: isIn(tf, 'h'),
        minute: isIn(tf, 'm'),
        second: isIn(tf, 's'),
        millisec: isIn(tf, 'l'),
        microsec: isIn(tf, 'c'),
        timezone: isIn(tf, 'z'),
        ampm: isIn(tf, 't') && isIn(timeFormat, 'h'),
        iso8601: isIn(timeFormat, 'Z')
      };
  };

  /*
  * Converts 24 hour format into 12 hour
  * Returns 12 hour without leading 0
  */
  var convert24to12 = function (hour) {
    hour %= 12;

    if (hour === 0) {
      hour = 12;
    }

    return String(hour);
  };

  var computeEffectiveSetting = function (settings, property) {
    return settings && settings[property] ? settings[property] : $.timepicker._defaults[property];
  };

  /*
  * Splits datetime string into date and time substrings.
  * Throws exception when date can't be parsed
  * Returns {dateString: dateString, timeString: timeString}
  */
  var splitDateTime = function (dateTimeString, timeSettings) {
    // The idea is to get the number separator occurrences in datetime and the time format requested (since time has
    // fewer unknowns, mostly numbers and am/pm). We will use the time pattern to split.
    var separator = computeEffectiveSetting(timeSettings, 'separator'),
      format = computeEffectiveSetting(timeSettings, 'timeFormat'),
      timeParts = format.split(separator), // how many occurrences of separator may be in our format?
      timePartsLen = timeParts.length,
      allParts = dateTimeString.split(separator),
      allPartsLen = allParts.length;

    if (allPartsLen > 1) {
      return {
        dateString: allParts.splice(0, allPartsLen - timePartsLen).join(separator),
        timeString: allParts.splice(0, timePartsLen).join(separator)
      };
    }

    return {
      dateString: dateTimeString,
      timeString: ''
    };
  };

  /*
  * Internal function to parse datetime interval
  * Returns: {date: Date, timeObj: Object}, where
  *   date - parsed date without time (type Date)
  *   timeObj = {hour: , minute: , second: , millisec: , microsec: } - parsed time. Optional
  */
  var parseDateTimeInternal = function (dateFormat, timeFormat, dateTimeString, dateSettings, timeSettings) {
    var date,
      parts,
      parsedTime;

    parts = splitDateTime(dateTimeString, timeSettings);
    date = $.datepicker._base_parseDate(dateFormat, parts.dateString, dateSettings);

    if (parts.timeString === '') {
      return {
        date: date
      };
    }

    parsedTime = $.datepicker.parseTime(timeFormat, parts.timeString, timeSettings);

    if (!parsedTime) {
      throw 'Wrong time format';
    }

    return {
      date: date,
      timeObj: parsedTime
    };
  };

  /*
  * Internal function to set timezone_select to the local timezone
  */
  var selectLocalTimezone = function (tp_inst, date) {
    if (tp_inst && tp_inst.timezone_select) {
      var now = date || new Date();
      tp_inst.timezone_select.val(-now.getTimezoneOffset());
    }
  };

  /*
  * Create a Singleton Instance
  */
  $.timepicker = new Timepicker();

  /**
   * Get the timezone offset as string from a date object (eg '+0530' for UTC+5.5)
   * @param {number} tzMinutes if not a number, less than -720 (-1200), or greater than 840 (+1400) this value is returned
   * @param {boolean} iso8601 if true formats in accordance to iso8601 "+12:45"
   * @return {string}
   */
  $.timepicker.timezoneOffsetString = function (tzMinutes, iso8601) {
    if (isNaN(tzMinutes) || tzMinutes > 840 || tzMinutes < -720) {
      return tzMinutes;
    }

    var off = tzMinutes,
      minutes = off % 60,
      hours = (off - minutes) / 60,
      iso = iso8601 ? ':' : '',
      tz = (off >= 0 ? '+' : '-') + ('0' + Math.abs(hours)).slice(-2) + iso + ('0' + Math.abs(minutes)).slice(-2);
    
    if (tz === '+00:00') {
      return 'Z';
    }
    return tz;
  };

  /**
   * Get the number in minutes that represents a timezone string
   * @param  {string} tzString formatted like "+0500", "-1245", "Z"
   * @return {number} the offset minutes or the original string if it doesn't match expectations
   */
  $.timepicker.timezoneOffsetNumber = function (tzString) {
    var normalized = tzString.toString().replace(':', ''); // excuse any iso8601, end up with "+1245"

    if (normalized.toUpperCase() === 'Z') { // if iso8601 with Z, its 0 minute offset
      return 0;
    }

    if (!/^(\-|\+)\d{4}$/.test(normalized)) { // possibly a user defined tz, so just give it back
      return tzString;
    }

    return ((normalized.substr(0, 1) === '-' ? -1 : 1) * // plus or minus
          ((parseInt(normalized.substr(1, 2), 10) * 60) + // hours (converted to minutes)
          parseInt(normalized.substr(3, 2), 10))); // minutes
  };

  /**
   * No way to set timezone in js Date, so we must adjust the minutes to compensate. (think setDate, getDate)
   * @param  {Date} date
   * @param  {string} toTimezone formatted like "+0500", "-1245"
   * @return {Date}
   */
  $.timepicker.timezoneAdjust = function (date, toTimezone) {
    var toTz = $.timepicker.timezoneOffsetNumber(toTimezone);
    if (!isNaN(toTz)) {
      date.setMinutes(date.getMinutes() + -date.getTimezoneOffset() - toTz);
    }
    return date;
  };

  /**
   * Calls `timepicker()` on the `startTime` and `endTime` elements, and configures them to
   * enforce date range limits.
   * n.b. The input value must be correctly formatted (reformatting is not supported)
   * @param  {Element} startTime
   * @param  {Element} endTime
   * @param  {Object} options Options for the timepicker() call
   * @return {jQuery}
   */
  $.timepicker.timeRange = function (startTime, endTime, options) {
    return $.timepicker.handleRange('timepicker', startTime, endTime, options);
  };

  /**
   * Calls `datetimepicker` on the `startTime` and `endTime` elements, and configures them to
   * enforce date range limits.
   * @param  {Element} startTime
   * @param  {Element} endTime
   * @param  {Object} options Options for the `timepicker()` call. Also supports `reformat`,
   *   a boolean value that can be used to reformat the input values to the `dateFormat`.
   * @param  {string} method Can be used to specify the type of picker to be added
   * @return {jQuery}
   */
  $.timepicker.datetimeRange = function (startTime, endTime, options) {
    $.timepicker.handleRange('datetimepicker', startTime, endTime, options);
  };

  /**
   * Calls `datepicker` on the `startTime` and `endTime` elements, and configures them to
   * enforce date range limits.
   * @param  {Element} startTime
   * @param  {Element} endTime
   * @param  {Object} options Options for the `timepicker()` call. Also supports `reformat`,
   *   a boolean value that can be used to reformat the input values to the `dateFormat`.
   * @return {jQuery}
   */
  $.timepicker.dateRange = function (startTime, endTime, options) {
    $.timepicker.handleRange('datepicker', startTime, endTime, options);
  };

  /**
   * Calls `method` on the `startTime` and `endTime` elements, and configures them to
   * enforce date range limits.
   * @param  {string} method Can be used to specify the type of picker to be added
   * @param  {Element} startTime
   * @param  {Element} endTime
   * @param  {Object} options Options for the `timepicker()` call. Also supports `reformat`,
   *   a boolean value that can be used to reformat the input values to the `dateFormat`.
   * @return {jQuery}
   */
  $.timepicker.handleRange = function (method, startTime, endTime, options) {
    options = $.extend({}, {
      minInterval: 0, // min allowed interval in milliseconds
      maxInterval: 0, // max allowed interval in milliseconds
      start: {},      // options for start picker
      end: {}         // options for end picker
    }, options);

    function checkDates(changed, other) {
      var startdt = startTime[method]('getDate'),
        enddt = endTime[method]('getDate'),
        changeddt = changed[method]('getDate');

      if (startdt !== null) {
        var minDate = new Date(startdt.getTime()),
          maxDate = new Date(startdt.getTime());

        minDate.setMilliseconds(minDate.getMilliseconds() + options.minInterval);
        maxDate.setMilliseconds(maxDate.getMilliseconds() + options.maxInterval);

        if (options.minInterval > 0 && minDate > enddt) { // minInterval check
          endTime[method]('setDate', minDate);
        }
        else if (options.maxInterval > 0 && maxDate < enddt) { // max interval check
          endTime[method]('setDate', maxDate);
        }
        else if (startdt > enddt) {
          other[method]('setDate', changeddt);
        }
      }
    }

    function selected(changed, other, option) {
      if (!changed.val()) {
        return;
      }
      var date = changed[method].call(changed, 'getDate');
      if (date !== null && options.minInterval > 0) {
        if (option === 'minDate') {
          date.setMilliseconds(date.getMilliseconds() + options.minInterval);
        }
        if (option === 'maxDate') {
          date.setMilliseconds(date.getMilliseconds() - options.minInterval);
        }
      }
      if (date.getTime) {
        other[method].call(other, 'option', option, date);
      }
    }

    $.fn[method].call(startTime, $.extend({
      onClose: function (dateText, inst) {
        checkDates($(this), endTime);
      },
      onSelect: function (selectedDateTime) {
        selected($(this), endTime, 'minDate');
      }
    }, options, options.start));
    $.fn[method].call(endTime, $.extend({
      onClose: function (dateText, inst) {
        checkDates($(this), startTime);
      },
      onSelect: function (selectedDateTime) {
        selected($(this), startTime, 'maxDate');
      }
    }, options, options.end));

    checkDates(startTime, endTime);
    selected(startTime, endTime, 'minDate');
    selected(endTime, startTime, 'maxDate');
    return $([startTime.get(0), endTime.get(0)]);
  };

  /**
   * Log error or data to the console during error or debugging
   * @param  {Object} err pass any type object to log to the console during error or debugging
   * @return {void}
   */
  $.timepicker.log = function (err) {
    if (window.console) {
      window.console.log(err);
    }
  };

  /*
   * Add util object to allow access to private methods for testability.
   */
  $.timepicker._util = {
    _extendRemove: extendRemove,
    _isEmptyObject: isEmptyObject,
    _convert24to12: convert24to12,
    _detectSupport: detectSupport,
    _selectLocalTimezone: selectLocalTimezone,
    _computeEffectiveSetting: computeEffectiveSetting,
    _splitDateTime: splitDateTime,
    _parseDateTimeInternal: parseDateTimeInternal
  };

  /*
  * Microsecond support
  */
  if (!Date.prototype.getMicroseconds) {
    Date.prototype.microseconds = 0;
    Date.prototype.getMicroseconds = function () { return this.microseconds; };
    Date.prototype.setMicroseconds = function (m) {
      this.setMilliseconds(this.getMilliseconds() + Math.floor(m / 1000));
      this.microseconds = m % 1000;
      return this;
    };
  }

  /*
  * Keep up with the version
  */
  $.timepicker.version = "1.4";

})(jQuery);

/*!
 * liScroll 1.0
 * Examples and documentation at: 
 * http://www.gcmingati.net/wordpress/wp-content/lab/jquery/newsticker/jq-liscroll/scrollanimate.html
 * 2007-2010 Gian Carlo Mingati
 * Version: 1.0.2.1 (22-APRIL-2011)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 * Requires:
 * jQuery v1.2.x or later
 * 
 */


jQuery.fn.liScroll = function(settings) {
    settings = jQuery.extend({
    travelocity: 0.07
    }, settings);   
    return this.each(function(){
        var $strip = jQuery(this);
        $strip.addClass("newsticker")
        var stripWidth = 1;
        $strip.find("li").each(function(i){
        stripWidth += jQuery(this, i).outerWidth(true); // thanks to Michael Haszprunar and Fabien Volpi
        });
        var $mask = $strip.wrap("<div class='mask'></div>");
        var $tickercontainer = $strip.parent().wrap("<div class='tickercontainer'></div>");               
        var containerWidth = $strip.parent().parent().width();  //a.k.a. 'mask' width   
        $strip.width(stripWidth);     
        var totalTravel = stripWidth+containerWidth;
        var defTiming = totalTravel/settings.travelocity; // thanks to Scott Waye   
        function scrollnews(spazio, tempo){
        $strip.animate({left: '-='+ spazio}, tempo, "linear", function(){$strip.css("left", containerWidth); scrollnews(totalTravel, defTiming);});
        }
        scrollnews(totalTravel, defTiming);       
        $strip.hover(function(){
        jQuery(this).stop();
        },
        function(){
        var offset = jQuery(this).offset();
        var residualSpace = offset.left + stripWidth;
        var residualTime = residualSpace/settings.travelocity;
        scrollnews(residualSpace, residualTime);
        });     
    }); 
};

/**
 * equalize.js
 * Author & copyright (c) 2012: Tim Svensen
 * Dual MIT & GPL license
 *
 * Page: http://tsvensen.github.com/equalize.js
 * Repo: https://github.com/tsvensen/equalize.js/
 */
(function(b){b.fn.equalize=function(a){var d=!1,g=!1,c,e;b.isPlainObject(a)?(c=a.equalize||"height",d=a.children||!1,g=a.reset||!1):c=a||"height";if(!b.isFunction(b.fn[c]))return!1;e=0<c.indexOf("eight")?"height":"width";return this.each(function(){var a=d?b(this).find(d):b(this).children(),f=0;a.each(function(){var a=b(this);g&&a.css(e,"");a=a[c]();a>f&&(f=a)});a.css(e,f+"px")})}})(jQuery);
/**
 * Dropzone.js
 */

!function(){function a(b,c,d){var e=a.resolve(b);if(null==e){d=d||b,c=c||"root";var f=new Error('Failed to require "'+d+'" from "'+c+'"');throw f.path=d,f.parent=c,f.require=!0,f}var g=a.modules[e];return g.exports||(g.exports={},g.client=g.component=!0,g.call(this,g.exports,a.relative(e),g)),g.exports}a.modules={},a.aliases={},a.resolve=function(b){"/"===b.charAt(0)&&(b=b.slice(1));for(var c=[b,b+".js",b+".json",b+"/index.js",b+"/index.json"],d=0;d<c.length;d++){var b=c[d];if(a.modules.hasOwnProperty(b))return b;if(a.aliases.hasOwnProperty(b))return a.aliases[b]}},a.normalize=function(a,b){var c=[];if("."!=b.charAt(0))return b;a=a.split("/"),b=b.split("/");for(var d=0;d<b.length;++d)".."==b[d]?a.pop():"."!=b[d]&&""!=b[d]&&c.push(b[d]);return a.concat(c).join("/")},a.register=function(b,c){a.modules[b]=c},a.alias=function(b,c){if(!a.modules.hasOwnProperty(b))throw new Error('Failed to alias "'+b+'", it does not exist');a.aliases[c]=b},a.relative=function(b){function c(a,b){for(var c=a.length;c--;)if(a[c]===b)return c;return-1}function d(c){var e=d.resolve(c);return a(e,b,c)}var e=a.normalize(b,"..");return d.resolve=function(d){var f=d.charAt(0);if("/"==f)return d.slice(1);if("."==f)return a.normalize(e,d);var g=b.split("/"),h=c(g,"deps")+1;return h||(h=0),d=g.slice(0,h+1).join("/")+"/deps/"+d},d.exists=function(b){return a.modules.hasOwnProperty(d.resolve(b))},d},a.register("component-emitter/index.js",function(a,b,c){function d(a){return a?e(a):void 0}function e(a){for(var b in d.prototype)a[b]=d.prototype[b];return a}c.exports=d,d.prototype.on=function(a,b){return this._callbacks=this._callbacks||{},(this._callbacks[a]=this._callbacks[a]||[]).push(b),this},d.prototype.once=function(a,b){function c(){d.off(a,c),b.apply(this,arguments)}var d=this;return this._callbacks=this._callbacks||{},b._off=c,this.on(a,c),this},d.prototype.off=d.prototype.removeListener=d.prototype.removeAllListeners=function(a,b){this._callbacks=this._callbacks||{};var c=this._callbacks[a];if(!c)return this;if(1==arguments.length)return delete this._callbacks[a],this;var d=c.indexOf(b._off||b);return~d&&c.splice(d,1),this},d.prototype.emit=function(a){this._callbacks=this._callbacks||{};var b=[].slice.call(arguments,1),c=this._callbacks[a];if(c){c=c.slice(0);for(var d=0,e=c.length;e>d;++d)c[d].apply(this,b)}return this},d.prototype.listeners=function(a){return this._callbacks=this._callbacks||{},this._callbacks[a]||[]},d.prototype.hasListeners=function(a){return!!this.listeners(a).length}}),a.register("dropzone/index.js",function(a,b,c){c.exports=b("./lib/dropzone.js")}),a.register("dropzone/lib/dropzone.js",function(a,b,c){!function(){var a,d,e,f,g,h,i={}.hasOwnProperty,j=function(a,b){function c(){this.constructor=a}for(var d in b)i.call(b,d)&&(a[d]=b[d]);return c.prototype=b.prototype,a.prototype=new c,a.__super__=b.prototype,a},k=[].slice;d="undefined"!=typeof Emitter&&null!==Emitter?Emitter:b("emitter"),g=function(){},a=function(a){function b(a,d){var e,f,g;if(this.element=a,this.version=b.version,this.defaultOptions.previewTemplate=this.defaultOptions.previewTemplate.replace(/\n*/g,""),this.clickableElements=[],this.listeners=[],this.files=[],"string"==typeof this.element&&(this.element=document.querySelector(this.element)),!this.element||null==this.element.nodeType)throw new Error("Invalid dropzone element.");if(this.element.dropzone)throw new Error("Dropzone already attached.");if(b.instances.push(this),a.dropzone=this,e=null!=(g=b.optionsForElement(this.element))?g:{},this.options=c({},this.defaultOptions,e,null!=d?d:{}),this.options.forceFallback||!b.isBrowserSupported())return this.options.fallback.call(this);if(null==this.options.url&&(this.options.url=this.element.getAttribute("action")),!this.options.url)throw new Error("No URL provided.");if(this.options.acceptedFiles&&this.options.acceptedMimeTypes)throw new Error("You can't provide both 'acceptedFiles' and 'acceptedMimeTypes'. 'acceptedMimeTypes' is deprecated.");this.options.acceptedMimeTypes&&(this.options.acceptedFiles=this.options.acceptedMimeTypes,delete this.options.acceptedMimeTypes),this.options.method=this.options.method.toUpperCase(),(f=this.getExistingFallback())&&f.parentNode&&f.parentNode.removeChild(f),this.previewsContainer=this.options.previewsContainer?b.getElement(this.options.previewsContainer,"previewsContainer"):this.element,this.options.clickable&&(this.clickableElements=this.options.clickable===!0?[this.element]:b.getElements(this.options.clickable,"clickable")),this.init()}var c;return j(b,a),b.prototype.events=["drop","dragstart","dragend","dragenter","dragover","dragleave","selectedfiles","addedfile","removedfile","thumbnail","error","errormultiple","processing","processingmultiple","uploadprogress","totaluploadprogress","sending","sendingmultiple","success","successmultiple","canceled","canceledmultiple","complete","completemultiple","reset","maxfilesexceeded"],b.prototype.defaultOptions={url:null,method:"post",withCredentials:!1,parallelUploads:2,uploadMultiple:!1,maxFilesize:256,paramName:"file",createImageThumbnails:!0,maxThumbnailFilesize:10,thumbnailWidth:100,thumbnailHeight:100,maxFiles:null,params:{},clickable:!0,ignoreHiddenFiles:!0,acceptedFiles:null,acceptedMimeTypes:null,autoProcessQueue:!0,addRemoveLinks:!1,previewsContainer:null,dictDefaultMessage:"Drop files here to upload",dictFallbackMessage:"Your browser does not support drag'n'drop file uploads.",dictFallbackText:"Please use the fallback form below to upload your files like in the olden days.",dictFileTooBig:"File is too big ({{filesize}}MB). Max filesize: {{maxFilesize}}MB.",dictInvalidFileType:"You can't upload files of this type.",dictResponseError:"Server responded with {{statusCode}} code.",dictCancelUpload:"Cancel upload",dictCancelUploadConfirmation:"Are you sure you want to cancel this upload?",dictRemoveFile:"Remove file",dictRemoveFileConfirmation:null,dictMaxFilesExceeded:"You can only upload {{maxFiles}} files.",accept:function(a,b){return b()},init:function(){return g},forceFallback:!1,fallback:function(){var a,c,d,e,f,g;for(this.element.className=""+this.element.className+" dz-browser-not-supported",g=this.element.getElementsByTagName("div"),e=0,f=g.length;f>e;e++)a=g[e],/(^| )dz-message($| )/.test(a.className)&&(c=a,a.className="dz-message");return c||(c=b.createElement('<div class="dz-message"><span></span></div>'),this.element.appendChild(c)),d=c.getElementsByTagName("span")[0],d&&(d.textContent=this.options.dictFallbackMessage),this.element.appendChild(this.getFallbackForm())},resize:function(a){var b,c,d;return b={srcX:0,srcY:0,srcWidth:a.width,srcHeight:a.height},c=a.width/a.height,d=this.options.thumbnailWidth/this.options.thumbnailHeight,a.height<this.options.thumbnailHeight||a.width<this.options.thumbnailWidth?(b.trgHeight=b.srcHeight,b.trgWidth=b.srcWidth):c>d?(b.srcHeight=a.height,b.srcWidth=b.srcHeight*d):(b.srcWidth=a.width,b.srcHeight=b.srcWidth/d),b.srcX=(a.width-b.srcWidth)/2,b.srcY=(a.height-b.srcHeight)/2,b},drop:function(){return this.element.classList.remove("dz-drag-hover")},dragstart:g,dragend:function(){return this.element.classList.remove("dz-drag-hover")},dragenter:function(){return this.element.classList.add("dz-drag-hover")},dragover:function(){return this.element.classList.add("dz-drag-hover")},dragleave:function(){return this.element.classList.remove("dz-drag-hover")},selectedfiles:function(){return this.element===this.previewsContainer?this.element.classList.add("dz-started"):void 0},reset:function(){return this.element.classList.remove("dz-started")},addedfile:function(a){var c=this;return a.previewElement=b.createElement(this.options.previewTemplate),a.previewTemplate=a.previewElement,this.previewsContainer.appendChild(a.previewElement),a.previewElement.querySelector("[data-dz-name]").textContent=a.name,a.previewElement.querySelector("[data-dz-size]").innerHTML=this.filesize(a.size),this.options.addRemoveLinks&&(a._removeLink=b.createElement('<a class="dz-remove" href="javascript:undefined;">'+this.options.dictRemoveFile+"</a>"),a._removeLink.addEventListener("click",function(d){return d.preventDefault(),d.stopPropagation(),a.status===b.UPLOADING?b.confirm(c.options.dictCancelUploadConfirmation,function(){return c.removeFile(a)}):c.options.dictRemoveFileConfirmation?b.confirm(c.options.dictRemoveFileConfirmation,function(){return c.removeFile(a)}):c.removeFile(a)}),a.previewElement.appendChild(a._removeLink)),this._updateMaxFilesReachedClass()},removedfile:function(a){var b;return null!=(b=a.previewElement)&&b.parentNode.removeChild(a.previewElement),this._updateMaxFilesReachedClass()},thumbnail:function(a,b){var c;return a.previewElement.classList.remove("dz-file-preview"),a.previewElement.classList.add("dz-image-preview"),c=a.previewElement.querySelector("[data-dz-thumbnail]"),c.alt=a.name,c.src=b},error:function(a,b){return a.previewElement.classList.add("dz-error"),a.previewElement.querySelector("[data-dz-errormessage]").textContent=b},errormultiple:g,processing:function(a){return a.previewElement.classList.add("dz-processing"),a._removeLink?a._removeLink.textContent=this.options.dictCancelUpload:void 0},processingmultiple:g,uploadprogress:function(a,b){return a.previewElement.querySelector("[data-dz-uploadprogress]").style.width=""+b+"%"},totaluploadprogress:g,sending:g,sendingmultiple:g,success:function(a){return a.previewElement.classList.add("dz-success")},successmultiple:g,canceled:function(a){return this.emit("error",a,"Upload canceled.")},canceledmultiple:g,complete:function(a){return a._removeLink?a._removeLink.textContent=this.options.dictRemoveFile:void 0},completemultiple:g,maxfilesexceeded:g,previewTemplate:'<div class="dz-preview dz-file-preview">\n  <div class="dz-details">\n    <div class="dz-filename"><span data-dz-name></span></div>\n    <div class="dz-size" data-dz-size></div>\n    <img data-dz-thumbnail />\n  </div>\n  <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>\n  <div class="dz-success-mark"><span>✔</span></div>\n  <div class="dz-error-mark"><span>✘</span></div>\n  <div class="dz-error-message"><span data-dz-errormessage></span></div>\n</div>'},c=function(){var a,b,c,d,e,f,g;for(d=arguments[0],c=2<=arguments.length?k.call(arguments,1):[],f=0,g=c.length;g>f;f++){b=c[f];for(a in b)e=b[a],d[a]=e}return d},b.prototype.getAcceptedFiles=function(){var a,b,c,d,e;for(d=this.files,e=[],b=0,c=d.length;c>b;b++)a=d[b],a.accepted&&e.push(a);return e},b.prototype.getRejectedFiles=function(){var a,b,c,d,e;for(d=this.files,e=[],b=0,c=d.length;c>b;b++)a=d[b],a.accepted||e.push(a);return e},b.prototype.getQueuedFiles=function(){var a,c,d,e,f;for(e=this.files,f=[],c=0,d=e.length;d>c;c++)a=e[c],a.status===b.QUEUED&&f.push(a);return f},b.prototype.getUploadingFiles=function(){var a,c,d,e,f;for(e=this.files,f=[],c=0,d=e.length;d>c;c++)a=e[c],a.status===b.UPLOADING&&f.push(a);return f},b.prototype.init=function(){var a,c,d,e,f,g,h,i=this;for("form"===this.element.tagName&&this.element.setAttribute("enctype","multipart/form-data"),this.element.classList.contains("dropzone")&&!this.element.querySelector(".dz-message")&&this.element.appendChild(b.createElement('<div class="dz-default dz-message"><span>'+this.options.dictDefaultMessage+"</span></div>")),this.clickableElements.length&&(d=function(){return i.hiddenFileInput&&document.body.removeChild(i.hiddenFileInput),i.hiddenFileInput=document.createElement("input"),i.hiddenFileInput.setAttribute("type","file"),i.hiddenFileInput.setAttribute("multiple","multiple"),null!=i.options.acceptedFiles&&i.hiddenFileInput.setAttribute("accept",i.options.acceptedFiles),i.hiddenFileInput.style.visibility="hidden",i.hiddenFileInput.style.position="absolute",i.hiddenFileInput.style.top="0",i.hiddenFileInput.style.left="0",i.hiddenFileInput.style.height="0",i.hiddenFileInput.style.width="0",document.body.appendChild(i.hiddenFileInput),i.hiddenFileInput.addEventListener("change",function(){var a;return a=i.hiddenFileInput.files,a.length&&(i.emit("selectedfiles",a),i.handleFiles(a)),d()})},d()),this.URL=null!=(g=window.URL)?g:window.webkitURL,h=this.events,e=0,f=h.length;f>e;e++)a=h[e],this.on(a,this.options[a]);return this.on("uploadprogress",function(){return i.updateTotalUploadProgress()}),this.on("removedfile",function(){return i.updateTotalUploadProgress()}),this.on("canceled",function(a){return i.emit("complete",a)}),c=function(a){return a.stopPropagation(),a.preventDefault?a.preventDefault():a.returnValue=!1},this.listeners=[{element:this.element,events:{dragstart:function(a){return i.emit("dragstart",a)},dragenter:function(a){return c(a),i.emit("dragenter",a)},dragover:function(a){return c(a),i.emit("dragover",a)},dragleave:function(a){return i.emit("dragleave",a)},drop:function(a){return c(a),i.drop(a)},dragend:function(a){return i.emit("dragend",a)}}}],this.clickableElements.forEach(function(a){return i.listeners.push({element:a,events:{click:function(c){return a!==i.element||c.target===i.element||b.elementInside(c.target,i.element.querySelector(".dz-message"))?i.hiddenFileInput.click():void 0}}})}),this.enable(),this.options.init.call(this)},b.prototype.destroy=function(){var a;return this.disable(),this.removeAllFiles(!0),(null!=(a=this.hiddenFileInput)?a.parentNode:void 0)&&(this.hiddenFileInput.parentNode.removeChild(this.hiddenFileInput),this.hiddenFileInput=null),delete this.element.dropzone},b.prototype.updateTotalUploadProgress=function(){var a,b,c,d,e,f,g,h;if(d=0,c=0,a=this.getAcceptedFiles(),a.length){for(h=this.getAcceptedFiles(),f=0,g=h.length;g>f;f++)b=h[f],d+=b.upload.bytesSent,c+=b.upload.total;e=100*d/c}else e=100;return this.emit("totaluploadprogress",e,c,d)},b.prototype.getFallbackForm=function(){var a,c,d,e;return(a=this.getExistingFallback())?a:(d='<div class="dz-fallback">',this.options.dictFallbackText&&(d+="<p>"+this.options.dictFallbackText+"</p>"),d+='<input type="file" name="'+this.options.paramName+(this.options.uploadMultiple?"[]":"")+'" '+(this.options.uploadMultiple?'multiple="multiple"':void 0)+' /><button type="submit">Upload!</button></div>',c=b.createElement(d),"FORM"!==this.element.tagName?(e=b.createElement('<form action="'+this.options.url+'" enctype="multipart/form-data" method="'+this.options.method+'"></form>'),e.appendChild(c)):(this.element.setAttribute("enctype","multipart/form-data"),this.element.setAttribute("method",this.options.method)),null!=e?e:c)},b.prototype.getExistingFallback=function(){var a,b,c,d,e,f;for(b=function(a){var b,c,d;for(c=0,d=a.length;d>c;c++)if(b=a[c],/(^| )fallback($| )/.test(b.className))return b},f=["div","form"],d=0,e=f.length;e>d;d++)if(c=f[d],a=b(this.element.getElementsByTagName(c)))return a},b.prototype.setupEventListeners=function(){var a,b,c,d,e,f,g;for(f=this.listeners,g=[],d=0,e=f.length;e>d;d++)a=f[d],g.push(function(){var d,e;d=a.events,e=[];for(b in d)c=d[b],e.push(a.element.addEventListener(b,c,!1));return e}());return g},b.prototype.removeEventListeners=function(){var a,b,c,d,e,f,g;for(f=this.listeners,g=[],d=0,e=f.length;e>d;d++)a=f[d],g.push(function(){var d,e;d=a.events,e=[];for(b in d)c=d[b],e.push(a.element.removeEventListener(b,c,!1));return e}());return g},b.prototype.disable=function(){var a,b,c,d,e;for(this.clickableElements.forEach(function(a){return a.classList.remove("dz-clickable")}),this.removeEventListeners(),d=this.files,e=[],b=0,c=d.length;c>b;b++)a=d[b],e.push(this.cancelUpload(a));return e},b.prototype.enable=function(){return this.clickableElements.forEach(function(a){return a.classList.add("dz-clickable")}),this.setupEventListeners()},b.prototype.filesize=function(a){var b;return a>=1e11?(a/=1e11,b="TB"):a>=1e8?(a/=1e8,b="GB"):a>=1e5?(a/=1e5,b="MB"):a>=100?(a/=100,b="KB"):(a=10*a,b="b"),"<strong>"+Math.round(a)/10+"</strong> "+b},b.prototype._updateMaxFilesReachedClass=function(){return this.options.maxFiles&&this.getAcceptedFiles().length>=this.options.maxFiles?this.element.classList.add("dz-max-files-reached"):this.element.classList.remove("dz-max-files-reached")},b.prototype.drop=function(a){var b,c;a.dataTransfer&&(this.emit("drop",a),b=a.dataTransfer.files,this.emit("selectedfiles",b),b.length&&(c=a.dataTransfer.items,c&&c.length&&(null!=c[0].webkitGetAsEntry||null!=c[0].getAsEntry)?this.handleItems(c):this.handleFiles(b)))},b.prototype.handleFiles=function(a){var b,c,d,e;for(e=[],c=0,d=a.length;d>c;c++)b=a[c],e.push(this.addFile(b));return e},b.prototype.handleItems=function(a){var b,c,d,e;for(d=0,e=a.length;e>d;d++)c=a[d],null!=c.webkitGetAsEntry?(b=c.webkitGetAsEntry(),b.isFile?this.addFile(c.getAsFile()):b.isDirectory&&this.addDirectory(b,b.name)):this.addFile(c.getAsFile())},b.prototype.accept=function(a,c){return a.size>1024*1024*this.options.maxFilesize?c(this.options.dictFileTooBig.replace("{{filesize}}",Math.round(a.size/1024/10.24)/100).replace("{{maxFilesize}}",this.options.maxFilesize)):b.isValidFile(a,this.options.acceptedFiles)?this.options.maxFiles&&this.getAcceptedFiles().length>=this.options.maxFiles?(c(this.options.dictMaxFilesExceeded.replace("{{maxFiles}}",this.options.maxFiles)),this.emit("maxfilesexceeded",a)):this.options.accept.call(this,a,c):c(this.options.dictInvalidFileType)},b.prototype.addFile=function(a){var c=this;return a.upload={progress:0,total:a.size,bytesSent:0},this.files.push(a),a.status=b.ADDED,this.emit("addedfile",a),this.options.createImageThumbnails&&a.type.match(/image.*/)&&a.size<=1024*1024*this.options.maxThumbnailFilesize&&this.createThumbnail(a),this.accept(a,function(b){return b?(a.accepted=!1,c._errorProcessing([a],b)):c.enqueueFile(a)})},b.prototype.enqueueFiles=function(a){var b,c,d;for(c=0,d=a.length;d>c;c++)b=a[c],this.enqueueFile(b);return null},b.prototype.enqueueFile=function(a){var c=this;if(a.accepted=!0,a.status!==b.ADDED)throw new Error("This file can't be queued because it has already been processed or was rejected.");return a.status=b.QUEUED,this.options.autoProcessQueue?setTimeout(function(){return c.processQueue()},1):void 0},b.prototype.addDirectory=function(a,b){var c,d,e=this;return c=a.createReader(),d=function(c){var d,f;for(d=0,f=c.length;f>d;d++)a=c[d],a.isFile?a.file(function(a){return e.options.ignoreHiddenFiles&&"."===a.name.substring(0,1)?void 0:(a.fullPath=""+b+"/"+a.name,e.addFile(a))}):a.isDirectory&&e.addDirectory(a,""+b+"/"+a.name)},c.readEntries(d,function(a){return"undefined"!=typeof console&&null!==console?"function"==typeof console.log?console.log(a):void 0:void 0})},b.prototype.removeFile=function(a){return a.status===b.UPLOADING&&this.cancelUpload(a),this.files=h(this.files,a),this.emit("removedfile",a),0===this.files.length?this.emit("reset"):void 0},b.prototype.removeAllFiles=function(a){var c,d,e,f;for(null==a&&(a=!1),f=this.files.slice(),d=0,e=f.length;e>d;d++)c=f[d],(c.status!==b.UPLOADING||a)&&this.removeFile(c);return null},b.prototype.createThumbnail=function(a){var b,c=this;return b=new FileReader,b.onload=function(){var d;return d=new Image,d.onload=function(){var b,e,f,g,h,i,j,k;return a.width=d.width,a.height=d.height,f=c.options.resize.call(c,a),null==f.trgWidth&&(f.trgWidth=c.options.thumbnailWidth),null==f.trgHeight&&(f.trgHeight=c.options.thumbnailHeight),b=document.createElement("canvas"),e=b.getContext("2d"),b.width=f.trgWidth,b.height=f.trgHeight,e.drawImage(d,null!=(h=f.srcX)?h:0,null!=(i=f.srcY)?i:0,f.srcWidth,f.srcHeight,null!=(j=f.trgX)?j:0,null!=(k=f.trgY)?k:0,f.trgWidth,f.trgHeight),g=b.toDataURL("image/png"),c.emit("thumbnail",a,g)},d.src=b.result},b.readAsDataURL(a)},b.prototype.processQueue=function(){var a,b,c,d;if(b=this.options.parallelUploads,c=this.getUploadingFiles().length,a=c,!(c>=b)&&(d=this.getQueuedFiles(),d.length>0)){if(this.options.uploadMultiple)return this.processFiles(d.slice(0,b-c));for(;b>a;){if(!d.length)return;this.processFile(d.shift()),a++}}},b.prototype.processFile=function(a){return this.processFiles([a])},b.prototype.processFiles=function(a){var c,d,e;for(d=0,e=a.length;e>d;d++)c=a[d],c.processing=!0,c.status=b.UPLOADING,this.emit("processing",c);return this.options.uploadMultiple&&this.emit("processingmultiple",a),this.uploadFiles(a)},b.prototype._getFilesWithXhr=function(a){var b,c;return c=function(){var c,d,e,f;for(e=this.files,f=[],c=0,d=e.length;d>c;c++)b=e[c],b.xhr===a&&f.push(b);return f}.call(this)},b.prototype.cancelUpload=function(a){var c,d,e,f,g,h,i;if(a.status===b.UPLOADING){for(d=this._getFilesWithXhr(a.xhr),e=0,g=d.length;g>e;e++)c=d[e],c.status=b.CANCELED;for(a.xhr.abort(),f=0,h=d.length;h>f;f++)c=d[f],this.emit("canceled",c);this.options.uploadMultiple&&this.emit("canceledmultiple",d)}else((i=a.status)===b.ADDED||i===b.QUEUED)&&(a.status=b.CANCELED,this.emit("canceled",a),this.options.uploadMultiple&&this.emit("canceledmultiple",[a]));return this.options.autoProcessQueue?this.processQueue():void 0},b.prototype.uploadFile=function(a){return this.uploadFiles([a])},b.prototype.uploadFiles=function(a){var d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,A,B,C,D,E=this;for(r=new XMLHttpRequest,s=0,w=a.length;w>s;s++)d=a[s],d.xhr=r;r.open(this.options.method,this.options.url,!0),r.withCredentials=!!this.options.withCredentials,o=null,f=function(){var b,c,e;for(e=[],b=0,c=a.length;c>b;b++)d=a[b],e.push(E._errorProcessing(a,o||E.options.dictResponseError.replace("{{statusCode}}",r.status),r));return e},p=function(b){var c,e,f,g,h,i,j,k,l;if(null!=b)for(e=100*b.loaded/b.total,f=0,i=a.length;i>f;f++)d=a[f],d.upload={progress:e,total:b.total,bytesSent:b.loaded};else{for(c=!0,e=100,g=0,j=a.length;j>g;g++)d=a[g],(100!==d.upload.progress||d.upload.bytesSent!==d.upload.total)&&(c=!1),d.upload.progress=e,d.upload.bytesSent=d.upload.total;if(c)return}for(l=[],h=0,k=a.length;k>h;h++)d=a[h],l.push(E.emit("uploadprogress",d,e,d.upload.bytesSent));return l},r.onload=function(c){var d;if(a[0].status!==b.CANCELED&&4===r.readyState){if(o=r.responseText,r.getResponseHeader("content-type")&&~r.getResponseHeader("content-type").indexOf("application/json"))try{o=JSON.parse(o)}catch(e){c=e,o="Invalid JSON response from server."}return p(),200<=(d=r.status)&&300>d?E._finished(a,o,c):f()}},r.onerror=function(){return a[0].status!==b.CANCELED?f():void 0},n=null!=(A=r.upload)?A:r,n.onprogress=p,i={Accept:"application/json","Cache-Control":"no-cache","X-Requested-With":"XMLHttpRequest"},this.options.headers&&c(i,this.options.headers);for(g in i)h=i[g],r.setRequestHeader(g,h);if(e=new FormData,this.options.params){B=this.options.params;for(m in B)q=B[m],e.append(m,q)}for(t=0,x=a.length;x>t;t++)d=a[t],this.emit("sending",d,r,e);if(this.options.uploadMultiple&&this.emit("sendingmultiple",a,r,e),"FORM"===this.element.tagName)for(C=this.element.querySelectorAll("input, textarea, select, button"),u=0,y=C.length;y>u;u++)j=C[u],k=j.getAttribute("name"),l=j.getAttribute("type"),(!l||"checkbox"!==(D=l.toLowerCase())&&"radio"!==D||j.checked)&&e.append(k,j.value);for(v=0,z=a.length;z>v;v++)d=a[v],e.append(""+this.options.paramName+(this.options.uploadMultiple?"[]":""),d,d.name);return r.send(e)},b.prototype._finished=function(a,c,d){var e,f,g;for(f=0,g=a.length;g>f;f++)e=a[f],e.status=b.SUCCESS,this.emit("success",e,c,d),this.emit("complete",e);return this.options.uploadMultiple&&(this.emit("successmultiple",a,c,d),this.emit("completemultiple",a)),this.options.autoProcessQueue?this.processQueue():void 0},b.prototype._errorProcessing=function(a,c,d){var e,f,g;for(f=0,g=a.length;g>f;f++)e=a[f],e.status=b.ERROR,this.emit("error",e,c,d),this.emit("complete",e);return this.options.uploadMultiple&&(this.emit("errormultiple",a,c,d),this.emit("completemultiple",a)),this.options.autoProcessQueue?this.processQueue():void 0},b}(d),a.version="3.7.1",a.options={},a.optionsForElement=function(b){return b.id?a.options[e(b.id)]:void 0},a.instances=[],a.forElement=function(a){if("string"==typeof a&&(a=document.querySelector(a)),null==(null!=a?a.dropzone:void 0))throw new Error("No Dropzone found for given element. This is probably because you're trying to access it before Dropzone had the time to initialize. Use the `init` option to setup any additional observers on your Dropzone.");return a.dropzone},a.autoDiscover=!0,a.discover=function(){var b,c,d,e,f,g;for(document.querySelectorAll?d=document.querySelectorAll(".dropzone"):(d=[],b=function(a){var b,c,e,f;for(f=[],c=0,e=a.length;e>c;c++)b=a[c],/(^| )dropzone($| )/.test(b.className)?f.push(d.push(b)):f.push(void 0);return f},b(document.getElementsByTagName("div")),b(document.getElementsByTagName("form"))),g=[],e=0,f=d.length;f>e;e++)c=d[e],a.optionsForElement(c)!==!1?g.push(new a(c)):g.push(void 0);return g},a.blacklistedBrowsers=[/opera.*Macintosh.*version\/12/i],a.isBrowserSupported=function(){var b,c,d,e,f;if(b=!0,window.File&&window.FileReader&&window.FileList&&window.Blob&&window.FormData&&document.querySelector)if("classList"in document.createElement("a"))for(f=a.blacklistedBrowsers,d=0,e=f.length;e>d;d++)c=f[d],c.test(navigator.userAgent)&&(b=!1);else b=!1;else b=!1;return b},h=function(a,b){var c,d,e,f;for(f=[],d=0,e=a.length;e>d;d++)c=a[d],c!==b&&f.push(c);return f},e=function(a){return a.replace(/[\-_](\w)/g,function(a){return a[1].toUpperCase()})},a.createElement=function(a){var b;return b=document.createElement("div"),b.innerHTML=a,b.childNodes[0]},a.elementInside=function(a,b){if(a===b)return!0;for(;a=a.parentNode;)if(a===b)return!0;return!1},a.getElement=function(a,b){var c;if("string"==typeof a?c=document.querySelector(a):null!=a.nodeType&&(c=a),null==c)throw new Error("Invalid `"+b+"` option provided. Please provide a CSS selector or a plain HTML element.");return c},a.getElements=function(a,b){var c,d,e,f,g,h,i,j;if(a instanceof Array){e=[];try{for(f=0,h=a.length;h>f;f++)d=a[f],e.push(this.getElement(d,b))}catch(k){c=k,e=null}}else if("string"==typeof a)for(e=[],j=document.querySelectorAll(a),g=0,i=j.length;i>g;g++)d=j[g],e.push(d);else null!=a.nodeType&&(e=[a]);if(null==e||!e.length)throw new Error("Invalid `"+b+"` option provided. Please provide a CSS selector, a plain HTML element or a list of those.");return e},a.confirm=function(a,b,c){return window.confirm(a)?b():null!=c?c():void 0},a.isValidFile=function(a,b){var c,d,e,f,g;if(!b)return!0;for(b=b.split(","),d=a.type,c=d.replace(/\/.*$/,""),f=0,g=b.length;g>f;f++)if(e=b[f],e=e.trim(),"."===e.charAt(0)){if(-1!==a.name.indexOf(e,a.name.length-e.length))return!0}else if(/\/\*$/.test(e)){if(c===e.replace(/\/.*$/,""))return!0}else if(d===e)return!0;return!1},"undefined"!=typeof jQuery&&null!==jQuery&&(jQuery.fn.dropzone=function(b){return this.each(function(){return new a(this,b)})}),"undefined"!=typeof c&&null!==c?c.exports=a:window.Dropzone=a,a.ADDED="added",a.QUEUED="queued",a.ACCEPTED=a.QUEUED,a.UPLOADING="uploading",a.PROCESSING=a.UPLOADING,a.CANCELED="canceled",a.ERROR="error",a.SUCCESS="success",f=function(a,b){var c,d,e,f,g,h,i,j,k;if(e=!1,k=!0,d=a.document,j=d.documentElement,c=d.addEventListener?"addEventListener":"attachEvent",i=d.addEventListener?"removeEventListener":"detachEvent",h=d.addEventListener?"":"on",f=function(c){return"readystatechange"!==c.type||"complete"===d.readyState?(("load"===c.type?a:d)[i](h+c.type,f,!1),!e&&(e=!0)?b.call(a,c.type||c):void 0):void 0},g=function(){var a;try{j.doScroll("left")}catch(b){return a=b,setTimeout(g,50),void 0}return f("poll")},"complete"!==d.readyState){if(d.createEventObject&&j.doScroll){try{k=!a.frameElement}catch(l){}k&&g()}return d[c](h+"DOMContentLoaded",f,!1),d[c](h+"readystatechange",f,!1),a[c](h+"load",f,!1)}},a._autoDiscoverFunction=function(){return a.autoDiscover?a.discover():void 0},f(window,a._autoDiscoverFunction)}.call(this)}),a.alias("component-emitter/index.js","dropzone/deps/emitter/index.js"),a.alias("component-emitter/index.js","emitter/index.js"),"object"==typeof exports?module.exports=a("dropzone"):"function"==typeof define&&define.amd?define(function(){return a("dropzone")}):this.Dropzone=a("dropzone")}();

/**
 * AjaxFileUpload
 * @param  {[type]} id                [description]
 * @param  {[type]} uri)                            {                             var frameId = 'jUploadFrame' + id;            var iframeHtml = '<iframe id="' + frameId + '" name="' + frameId + '" style="position:absolute; top:-9999px; left:-9999px"';      if(window.ActiveXObject)      {                if(typeof uri== 'boolean'){          iframeHtml += ' src="' + 'javascript:false' + '"';                }                else if(typeof uri== 'string'){          iframeHtml += ' src="' + uri + '"';                }       }      iframeHtml += ' />';      jQuery(iframeHtml).appendTo(document.body);            return jQuery('#' + frameId).get(0);          } [description]
 * @param  {[type]} createUploadForm: function(id   [description]
 * @param  {[type]} fileElementId     [description]
 * @param  {[type]} data              [description]
 * @return {[type]}                   [description]
 */
jQuery.extend({
  

    createUploadIframe: function(id, uri)
  {
      //create frame
            var frameId = 'jUploadFrame' + id;
            var iframeHtml = '<iframe id="' + frameId + '" name="' + frameId + '" style="position:absolute; top:-9999px; left:-9999px"';
      if(window.ActiveXObject)
      {
                if(typeof uri== 'boolean'){
          iframeHtml += ' src="' + 'javascript:false' + '"';

                }
                else if(typeof uri== 'string'){
          iframeHtml += ' src="' + uri + '"';

                } 
      }
      iframeHtml += ' />';
      jQuery(iframeHtml).appendTo(document.body);

            return jQuery('#' + frameId).get(0);      
    },
    createUploadForm: function(id, fileElementId, data)
  {
    //create form 
    var formId = 'jUploadForm' + id;
    var fileId = 'jUploadFile' + id;
    var form = jQuery('<form  action="" method="POST" name="' + formId + '" id="' + formId + '" enctype="multipart/form-data"></form>');  
    if(data)
    {
      for(var i in data)
      {
        jQuery('<input type="hidden" name="' + i + '" value="' + data[i] + '" />').appendTo(form);
      }     
    }   
    var oldElement = jQuery('#' + fileElementId);
    var newElement = jQuery(oldElement).clone();
    jQuery(oldElement).attr('id', fileId);
    jQuery(oldElement).before(newElement);
    jQuery(oldElement).appendTo(form);


    
    //set attributes
    jQuery(form).css('position', 'absolute');
    jQuery(form).css('top', '-1200px');
    jQuery(form).css('left', '-1200px');
    jQuery(form).appendTo('body');    
    return form;
    },

    ajaxFileUpload: function(s) {
        // TODO introduce global settings, allowing the client to modify them for all requests, not only timeout    
        s = jQuery.extend({}, jQuery.ajaxSettings, s);
        var id = new Date().getTime()        
    var form = jQuery.createUploadForm(id, s.fileElementId, (typeof(s.data)=='undefined'?false:s.data));
    var io = jQuery.createUploadIframe(id, s.secureuri);
    var frameId = 'jUploadFrame' + id;
    var formId = 'jUploadForm' + id;    
        // Watch for a new set of requests
        if ( s.global && ! jQuery.active++ )
    {
      jQuery.event.trigger( "ajaxStart" );
    }            
        var requestDone = false;
        // Create the request object
        var xml = {}   
        if ( s.global )
            jQuery.event.trigger("ajaxSend", [xml, s]);
        // Wait for a response to come back
        var uploadCallback = function(isTimeout)
    {     
      var io = document.getElementById(frameId);
            try 
      {       
        if(io.contentWindow)
        {
           xml.responseText = io.contentWindow.document.body?io.contentWindow.document.body.innerHTML:null;
                   xml.responseXML = io.contentWindow.document.XMLDocument?io.contentWindow.document.XMLDocument:io.contentWindow.document;
           
        }else if(io.contentDocument)
        {
           xml.responseText = io.contentDocument.document.body?io.contentDocument.document.body.innerHTML:null;
                  xml.responseXML = io.contentDocument.document.XMLDocument?io.contentDocument.document.XMLDocument:io.contentDocument.document;
        }           
            }catch(e)
      {
        jQuery.handleError(s, xml, null, e);
      }
            if ( xml || isTimeout == "timeout") 
      {       
                requestDone = true;
                var status;
                try {
                    status = isTimeout != "timeout" ? "success" : "error";
                    // Make sure that the request was successful or notmodified
                    if ( status != "error" )
          {
                        // process the data (runs the xml through httpData regardless of callback)
                        var data = jQuery.uploadHttpData( xml, s.dataType );    
                        // If a local callback was specified, fire it and pass it the data
                        if ( s.success )
                            s.success( data, status );
    
                        // Fire the global callback
                        if( s.global )
                            jQuery.event.trigger( "ajaxSuccess", [xml, s] );
                    } else
                        jQuery.handleError(s, xml, status);
                } catch(e) 
        {
                    status = "error";
                    jQuery.handleError(s, xml, status, e);
                }

                // The request was completed
                if( s.global )
                    jQuery.event.trigger( "ajaxComplete", [xml, s] );

                // Handle the global AJAX counter
                if ( s.global && ! --jQuery.active )
                    jQuery.event.trigger( "ajaxStop" );

                // Process result
                if ( s.complete )
                    s.complete(xml, status);

                jQuery(io).unbind()

                setTimeout(function()
                  { try 
                    {
                      jQuery(io).remove();
                      jQuery(form).remove();  
                      
                    } catch(e) 
                    {
                      jQuery.handleError(s, xml, null, e);
                    }                 

                  }, 100)

                xml = null

            }
        }
        // Timeout checker
        if ( s.timeout > 0 ) 
    {
            setTimeout(function(){
                // Check to see if the request is still happening
                if( !requestDone ) uploadCallback( "timeout" );
            }, s.timeout);
        }
        try 
    {

      var form = jQuery('#' + formId);
      jQuery(form).attr('action', s.url);
      jQuery(form).attr('method', 'POST');
      jQuery(form).attr('target', frameId);
            if(form.encoding)
      {
        jQuery(form).attr('encoding', 'multipart/form-data');           
            }
            else
      { 
        jQuery(form).attr('enctype', 'multipart/form-data');      
            }     
            jQuery(form).submit();

        } catch(e) 
    {     
            jQuery.handleError(s, xml, null, e);
        }
    
    jQuery('#' + frameId).load(uploadCallback );
        return {abort: function () {}}; 

    },

    uploadHttpData: function( r, type ) {
        var data = !type;
        data = type == "xml" || data ? r.responseXML : r.responseText;
        // If the type is "script", eval it in global context
        if ( type == "script" )
            jQuery.globalEval( data );
        // Get the JavaScript object, if JSON is used.
        if ( type == "json" )
            eval( "data = " + data );
        // evaluate scripts within html
        if ( type == "html" )
            jQuery("<div>").html(data).evalScripts();

        return data;
    },

    handleError: function( s, xhr, status, e ) {
    // If a local callback was specified, fire it
    if ( s.error ) {
        s.error.call( s.context || window, xhr, status, e );
    }

    // Fire the global callback
    if ( s.global ) {
        (s.context ? jQuery(s.context) : jQuery.event).trigger( "ajaxError", [xhr, s, e] );
    }
}
});

/*!
  Slimbox v2.05 - The ultimate lightweight Lightbox clone for jQuery
  (c) 2007-2013 Christophe Beyls <http://www.digitalia.be>
  MIT-style license.
*/
(function (w) {
    var E = w(window),
        u, f, F = -1,
        n, x, D, v, y, L, r, m = !window.XMLHttpRequest,
        s = [],
        l = document.documentElement,
        k = {}, t = new Image(),
        J = new Image(),
        H, a, g, p, I, d, G, c, A, K;
    w(function () {
        w("body").append(w([H = w('<div id="lbOverlay" />').click(C)[0], a = w('<div id="lbCenter" />')[0], G = w('<div id="lbBottomContainer" />')[0]]).css("display", "none"));
        g = w('<div id="lbImage" />').appendTo(a).append(p = w('<div style="position: relative;" />').append([I = w('<a id="lbPrevLink" href="#" />').click(B)[0], d = w('<a id="lbNextLink" href="#" />').click(e)[0]])[0])[0];
        c = w('<div id="lbBottom" />').appendTo(G).append([w('<a id="lbCloseLink" href="#" />').click(C)[0], A = w('<div id="lbCaption" />')[0], K = w('<div id="lbNumber" />')[0], w('<div style="clear: both;" />')[0]])[0]
        cl = w('<div class="lbclose"/>').click(C).appendTo(a);
    });
    w.slimbox = function (O, N, M) {
        u = w.extend({
            loop: false,
            overlayOpacity: 0.8,
            overlayFadeDuration: 400,
            resizeDuration: 400,
            resizeEasing: "swing",
            initialWidth: 250,
            initialHeight: 250,
            imageFadeDuration: 400,
            captionAnimationDuration: 400,
            counterText: "Image {x} of {y}",
            closeKeys: [27, 88, 67],
            previousKeys: [37, 80],
            nextKeys: [39, 78]
        }, M);
        if (typeof O == "string") {
            O = [
                [O, N]
            ];
            N = 0
        }
        y = E.scrollTop() + (E.height() / 2);
        L = u.initialWidth;
        r = u.initialHeight;
        w(a).css({
            top: Math.max(0, y - (r / 2)),
            width: L,
            height: r,
            marginLeft: -L / 2
        }).show();
        v = m || (H.currentStyle && (H.currentStyle.position != "fixed"));
        if (v) {
            H.style.position = "absolute"
        }
        w(H).css("opacity", u.overlayOpacity).fadeIn(u.overlayFadeDuration);
        z();
        j(1);
        f = O;
        u.loop = u.loop && (f.length > 1);
        return b(N)
    };
    w.fn.slimbox = function (M, P, O) {
        P = P || function (Q) {
            return [Q.href, Q.title]
        };
        O = O || function () {
            return true
        };
        var N = this;
        return N.unbind("click").click(function () {
            var S = this,
                U = 0,
                T, Q = 0,
                R;
            T = w.grep(N, function (W, V) {
                return O.call(S, W, V)
            });
            for (R = T.length; Q < R; ++Q) {
                if (T[Q] == S) {
                    U = Q
                }
                T[Q] = P(T[Q], Q)
            }
            return w.slimbox(T, U, M)
        })
    };

    function z() {
        var N = E.scrollLeft(),
            M = E.width();
        w([a, G]).css("left", N + (M / 2));
        if (v) {
            w(H).css({
                left: N,
                top: E.scrollTop(),
                width: M,
                height: E.height()
            })
        }
    }

    function j(M) {
        if (M) {
            w("object").add(m ? "select" : "embed").each(function (O, P) {
                s[O] = [P, P.style.visibility];
                P.style.visibility = "hidden"
            })
        } else {
            w.each(s, function (O, P) {
                P[0].style.visibility = P[1]
            });
            s = []
        }
        var N = M ? "bind" : "unbind";
        E[N]("scroll resize", z);
        w(document)[N]("keydown", o)
    }

    function o(O) {
        var N = O.which,
            M = w.inArray;
        return (M(N, u.closeKeys) >= 0) ? C() : (M(N, u.nextKeys) >= 0) ? e() : (M(N, u.previousKeys) >= 0) ? B() : null
    }

    function B() {
        return b(x)
    }

    function e() {
        return b(D)
    }

    function b(M) {
        if (M >= 0) {
            F = M;
            n = f[F][0];
            x = (F || (u.loop ? f.length : 0)) - 1;
            D = ((F + 1) % f.length) || (u.loop ? 0 : -1);
            q();
            a.className = "lbLoading";
            k = new Image();
            k.onload = i;
            k.src = n
        }
        return false
    }

    function i() {
        a.className = "";
        w(g).css({
            backgroundImage: "url(" + n + ")",
            visibility: "hidden",
            display: ""
        });
        w(p).width(k.width);
        w([p, I, d]).height(k.height);
        w(A).html(f[F][1] || "");
        w(K).html((((f.length > 1) && u.counterText) || "").replace(/{x}/, F + 1).replace(/{y}/, f.length));
        if (x >= 0) {
            t.src = f[x][0]
        }
        if (D >= 0) {
            J.src = f[D][0]
        }
        L = g.offsetWidth;
        r = g.offsetHeight;
        var M = Math.max(0, y - (r / 2));
        if (a.offsetHeight != r) {
            w(a).animate({
                height: r,
                top: M
            }, u.resizeDuration, u.resizeEasing)
        }
        if (a.offsetWidth != L) {
            w(a).animate({
                width: L,
                marginLeft: -L / 2
            }, u.resizeDuration, u.resizeEasing)
        }
        w(a).queue(function () {
            w(G).css({
                width: L,
                top: M + r,
                marginLeft: -L / 2,
                visibility: "hidden",
                display: ""
            });
            w(g).css({
                display: "none",
                visibility: "",
                opacity: ""
            }).fadeIn(u.imageFadeDuration, h)
        })
    }

    function h() {
        if (x >= 0) {
            w(I).show()
        }
        if (D >= 0) {
            w(d).show()
        }
        w(c).css("marginTop", -c.offsetHeight).animate({
            marginTop: 0
        }, u.captionAnimationDuration);
        G.style.visibility = ""
    }

    function q() {
        k.onload = null;
        k.src = t.src = J.src = n;
        w([a, g, c]).stop(true);
        w([I, d, g, G]).hide()
    }

    function C() {
        if (F >= 0) {
            q();
            F = x = D = -1;
            w(a).hide();
            w(H).stop().fadeOut(u.overlayFadeDuration, j)
        }
        return false
    }
})(jQuery);
// AUTOLOAD CODE BLOCK (MAY BE CHANGED OR REMOVED)
if (!/android|iphone|ipod|series60|symbian|windows ce|blackberry/i.test(navigator.userAgent)) {
  jQuery(function($) {
    $("a[rel^='lightbox']").slimbox({/* Put custom options here */}, null, function(el) {
      return (this == el) || ((this.rel.length > 8) && (this.rel == el.rel));
    });
  });
}

/**
 * BxSlider v4.1.1 - Fully loaded, responsive content slider
 * http://bxslider.com
 *
 * Copyright 2012, Steven Wanderski - http://stevenwanderski.com - http://bxcreative.com
 * Written while drinking Belgian ales and listening to jazz
 *
 * Released under the WTFPL license - http://sam.zoy.org/wtfpl/
 */
!function(t){var e={},s={mode:"horizontal",slideSelector:"",infiniteLoop:!0,hideControlOnEnd:!1,speed:500,easing:null,slideMargin:0,startSlide:0,randomStart:!1,captions:!1,ticker:!1,tickerHover:!1,adaptiveHeight:!1,adaptiveHeightSpeed:500,video:!1,useCSS:!0,preloadImages:"visible",responsive:!0,touchEnabled:!0,swipeThreshold:50,oneToOneTouch:!0,preventDefaultSwipeX:!0,preventDefaultSwipeY:!1,pager:!0,pagerType:"full",pagerShortSeparator:" / ",pagerSelector:null,buildPager:null,pagerCustom:null,controls:!0,nextText:"Next",prevText:"Prev",nextSelector:null,prevSelector:null,autoControls:!1,startText:"Start",stopText:"Stop",autoControlsCombine:!1,autoControlsSelector:null,auto:!1,pause:4e3,autoStart:!0,autoDirection:"next",autoHover:!1,autoDelay:0,minSlides:1,maxSlides:1,moveSlides:0,slideWidth:0,onSliderLoad:function(){},onSlideBefore:function(){},onSlideAfter:function(){},onSlideNext:function(){},onSlidePrev:function(){}};t.fn.bxSlider=function(n){if(0==this.length)return this;if(this.length>1)return this.each(function(){t(this).bxSlider(n)}),this;var o={},r=this;e.el=this;var a=t(window).width(),l=t(window).height(),d=function(){o.settings=t.extend({},s,n),o.settings.slideWidth=parseInt(o.settings.slideWidth),o.children=r.children(o.settings.slideSelector),o.children.length<o.settings.minSlides&&(o.settings.minSlides=o.children.length),o.children.length<o.settings.maxSlides&&(o.settings.maxSlides=o.children.length),o.settings.randomStart&&(o.settings.startSlide=Math.floor(Math.random()*o.children.length)),o.active={index:o.settings.startSlide},o.carousel=o.settings.minSlides>1||o.settings.maxSlides>1,o.carousel&&(o.settings.preloadImages="all"),o.minThreshold=o.settings.minSlides*o.settings.slideWidth+(o.settings.minSlides-1)*o.settings.slideMargin,o.maxThreshold=o.settings.maxSlides*o.settings.slideWidth+(o.settings.maxSlides-1)*o.settings.slideMargin,o.working=!1,o.controls={},o.interval=null,o.animProp="vertical"==o.settings.mode?"top":"left",o.usingCSS=o.settings.useCSS&&"fade"!=o.settings.mode&&function(){var t=document.createElement("div"),e=["WebkitPerspective","MozPerspective","OPerspective","msPerspective"];for(var i in e)if(void 0!==t.style[e[i]])return o.cssPrefix=e[i].replace("Perspective","").toLowerCase(),o.animProp="-"+o.cssPrefix+"-transform",!0;return!1}(),"vertical"==o.settings.mode&&(o.settings.maxSlides=o.settings.minSlides),r.data("origStyle",r.attr("style")),r.children(o.settings.slideSelector).each(function(){t(this).data("origStyle",t(this).attr("style"))}),c()},c=function(){r.wrap('<div class="bx-wrapper"><div class="bx-viewport"></div></div>'),o.viewport=r.parent(),o.loader=t('<div class="bx-loading" />'),o.viewport.prepend(o.loader),r.css({width:"horizontal"==o.settings.mode?100*o.children.length+215+"%":"auto",position:"relative"}),o.usingCSS&&o.settings.easing?r.css("-"+o.cssPrefix+"-transition-timing-function",o.settings.easing):o.settings.easing||(o.settings.easing="swing"),f(),o.viewport.css({width:"100%",overflow:"hidden",position:"relative"}),o.viewport.parent().css({maxWidth:v()}),o.settings.pager||o.viewport.parent().css({margin:"0 auto 0px"}),o.children.css({"float":"horizontal"==o.settings.mode?"left":"none",listStyle:"none",position:"relative"}),o.children.css("width",u()),"horizontal"==o.settings.mode&&o.settings.slideMargin>0&&o.children.css("marginRight",o.settings.slideMargin),"vertical"==o.settings.mode&&o.settings.slideMargin>0&&o.children.css("marginBottom",o.settings.slideMargin),"fade"==o.settings.mode&&(o.children.css({position:"absolute",zIndex:0,display:"none"}),o.children.eq(o.settings.startSlide).css({zIndex:50,display:"block"})),o.controls.el=t('<div class="bx-controls" />'),o.settings.captions&&P(),o.active.last=o.settings.startSlide==x()-1,o.settings.video&&r.fitVids();var e=o.children.eq(o.settings.startSlide);"all"==o.settings.preloadImages&&(e=o.children),o.settings.ticker?o.settings.pager=!1:(o.settings.pager&&T(),o.settings.controls&&C(),o.settings.auto&&o.settings.autoControls&&E(),(o.settings.controls||o.settings.autoControls||o.settings.pager)&&o.viewport.after(o.controls.el)),g(e,h)},g=function(e,i){var s=e.find("img, iframe").length;if(0==s)return i(),void 0;var n=0;e.find("img, iframe").each(function(){t(this).is("img")&&t(this).attr("src",t(this).attr("src")+"?timestamp="+(new Date).getTime()),t(this).load(function(){setTimeout(function(){++n==s&&i()},0)})})},h=function(){if(o.settings.infiniteLoop&&"fade"!=o.settings.mode&&!o.settings.ticker){var e="vertical"==o.settings.mode?o.settings.minSlides:o.settings.maxSlides,i=o.children.slice(0,e).clone().addClass("bx-clone"),s=o.children.slice(-e).clone().addClass("bx-clone");r.append(i).prepend(s)}o.loader.remove(),S(),"vertical"==o.settings.mode&&(o.settings.adaptiveHeight=!0),o.viewport.height(p()),r.redrawSlider(),o.settings.onSliderLoad(o.active.index),o.initialized=!0,o.settings.responsive&&t(window).bind("resize",B),o.settings.auto&&o.settings.autoStart&&H(),o.settings.ticker&&L(),o.settings.pager&&I(o.settings.startSlide),o.settings.controls&&W(),o.settings.touchEnabled&&!o.settings.ticker&&O()},p=function(){var e=0,s=t();if("vertical"==o.settings.mode||o.settings.adaptiveHeight)if(o.carousel){var n=1==o.settings.moveSlides?o.active.index:o.active.index*m();for(s=o.children.eq(n),i=1;i<=o.settings.maxSlides-1;i++)s=n+i>=o.children.length?s.add(o.children.eq(i-1)):s.add(o.children.eq(n+i))}else s=o.children.eq(o.active.index);else s=o.children;return"vertical"==o.settings.mode?(s.each(function(){e+=t(this).outerHeight()}),o.settings.slideMargin>0&&(e+=o.settings.slideMargin*(o.settings.minSlides-1))):e=Math.max.apply(Math,s.map(function(){return t(this).outerHeight(!1)}).get()),e},v=function(){var t="100%";return o.settings.slideWidth>0&&(t="horizontal"==o.settings.mode?o.settings.maxSlides*o.settings.slideWidth+(o.settings.maxSlides-1)*o.settings.slideMargin:o.settings.slideWidth),t},u=function(){var t=o.settings.slideWidth,e=o.viewport.width();return 0==o.settings.slideWidth||o.settings.slideWidth>e&&!o.carousel||"vertical"==o.settings.mode?t=e:o.settings.maxSlides>1&&"horizontal"==o.settings.mode&&(e>o.maxThreshold||e<o.minThreshold&&(t=(e-o.settings.slideMargin*(o.settings.minSlides-1))/o.settings.minSlides)),t},f=function(){var t=1;if("horizontal"==o.settings.mode&&o.settings.slideWidth>0)if(o.viewport.width()<o.minThreshold)t=o.settings.minSlides;else if(o.viewport.width()>o.maxThreshold)t=o.settings.maxSlides;else{var e=o.children.first().width();t=Math.floor(o.viewport.width()/e)}else"vertical"==o.settings.mode&&(t=o.settings.minSlides);return t},x=function(){var t=0;if(o.settings.moveSlides>0)if(o.settings.infiniteLoop)t=o.children.length/m();else for(var e=0,i=0;e<o.children.length;)++t,e=i+f(),i+=o.settings.moveSlides<=f()?o.settings.moveSlides:f();else t=Math.ceil(o.children.length/f());return t},m=function(){return o.settings.moveSlides>0&&o.settings.moveSlides<=f()?o.settings.moveSlides:f()},S=function(){if(o.children.length>o.settings.maxSlides&&o.active.last&&!o.settings.infiniteLoop){if("horizontal"==o.settings.mode){var t=o.children.last(),e=t.position();b(-(e.left-(o.viewport.width()-t.width())),"reset",0)}else if("vertical"==o.settings.mode){var i=o.children.length-o.settings.minSlides,e=o.children.eq(i).position();b(-e.top,"reset",0)}}else{var e=o.children.eq(o.active.index*m()).position();o.active.index==x()-1&&(o.active.last=!0),void 0!=e&&("horizontal"==o.settings.mode?b(-e.left,"reset",0):"vertical"==o.settings.mode&&b(-e.top,"reset",0))}},b=function(t,e,i,s){if(o.usingCSS){var n="vertical"==o.settings.mode?"translate3d(0, "+t+"px, 0)":"translate3d("+t+"px, 0, 0)";r.css("-"+o.cssPrefix+"-transition-duration",i/1e3+"s"),"slide"==e?(r.css(o.animProp,n),r.bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",function(){r.unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd"),D()})):"reset"==e?r.css(o.animProp,n):"ticker"==e&&(r.css("-"+o.cssPrefix+"-transition-timing-function","linear"),r.css(o.animProp,n),r.bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",function(){r.unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd"),b(s.resetValue,"reset",0),N()}))}else{var a={};a[o.animProp]=t,"slide"==e?r.animate(a,i,o.settings.easing,function(){D()}):"reset"==e?r.css(o.animProp,t):"ticker"==e&&r.animate(a,speed,"linear",function(){b(s.resetValue,"reset",0),N()})}},w=function(){for(var e="",i=x(),s=0;i>s;s++){var n="";o.settings.buildPager&&t.isFunction(o.settings.buildPager)?(n=o.settings.buildPager(s),o.pagerEl.addClass("bx-custom-pager")):(n=s+1,o.pagerEl.addClass("bx-default-pager")),e+='<div class="bx-pager-item"><a href="" data-slide-index="'+s+'" class="bx-pager-link">'+n+"</a></div>"}o.pagerEl.html(e)},T=function(){o.settings.pagerCustom?o.pagerEl=t(o.settings.pagerCustom):(o.pagerEl=t('<div class="bx-pager" />'),o.settings.pagerSelector?t(o.settings.pagerSelector).html(o.pagerEl):o.controls.el.addClass("bx-has-pager").append(o.pagerEl),w()),o.pagerEl.delegate("a","click",q)},C=function(){o.controls.next=t('<a class="bx-next" href="">'+o.settings.nextText+"</a>"),o.controls.prev=t('<a class="bx-prev" href="">'+o.settings.prevText+"</a>"),o.controls.next.bind("click",y),o.controls.prev.bind("click",z),o.settings.nextSelector&&t(o.settings.nextSelector).append(o.controls.next),o.settings.prevSelector&&t(o.settings.prevSelector).append(o.controls.prev),o.settings.nextSelector||o.settings.prevSelector||(o.controls.directionEl=t('<div class="bx-controls-direction" />'),o.controls.directionEl.append(o.controls.prev).append(o.controls.next),o.controls.el.addClass("bx-has-controls-direction").append(o.controls.directionEl))},E=function(){o.controls.start=t('<div class="bx-controls-auto-item"><a class="bx-start" href="">'+o.settings.startText+"</a></div>"),o.controls.stop=t('<div class="bx-controls-auto-item"><a class="bx-stop" href="">'+o.settings.stopText+"</a></div>"),o.controls.autoEl=t('<div class="bx-controls-auto" />'),o.controls.autoEl.delegate(".bx-start","click",k),o.controls.autoEl.delegate(".bx-stop","click",M),o.settings.autoControlsCombine?o.controls.autoEl.append(o.controls.start):o.controls.autoEl.append(o.controls.start).append(o.controls.stop),o.settings.autoControlsSelector?t(o.settings.autoControlsSelector).html(o.controls.autoEl):o.controls.el.addClass("bx-has-controls-auto").append(o.controls.autoEl),A(o.settings.autoStart?"stop":"start")},P=function(){o.children.each(function(){var e=t(this).find("img:first").attr("title");void 0!=e&&(""+e).length&&t(this).append('<div class="bx-caption"><span>'+e+"</span></div>")})},y=function(t){o.settings.auto&&r.stopAuto(),r.goToNextSlide(),t.preventDefault()},z=function(t){o.settings.auto&&r.stopAuto(),r.goToPrevSlide(),t.preventDefault()},k=function(t){r.startAuto(),t.preventDefault()},M=function(t){r.stopAuto(),t.preventDefault()},q=function(e){o.settings.auto&&r.stopAuto();var i=t(e.currentTarget),s=parseInt(i.attr("data-slide-index"));s!=o.active.index&&r.goToSlide(s),e.preventDefault()},I=function(e){var i=o.children.length;return"short"==o.settings.pagerType?(o.settings.maxSlides>1&&(i=Math.ceil(o.children.length/o.settings.maxSlides)),o.pagerEl.html(e+1+o.settings.pagerShortSeparator+i),void 0):(o.pagerEl.find("a").removeClass("active"),o.pagerEl.each(function(i,s){t(s).find("a").eq(e).addClass("active")}),void 0)},D=function(){if(o.settings.infiniteLoop){var t="";0==o.active.index?t=o.children.eq(0).position():o.active.index==x()-1&&o.carousel?t=o.children.eq((x()-1)*m()).position():o.active.index==o.children.length-1&&(t=o.children.eq(o.children.length-1).position()),"horizontal"==o.settings.mode?b(-t.left,"reset",0):"vertical"==o.settings.mode&&b(-t.top,"reset",0)}o.working=!1,o.settings.onSlideAfter(o.children.eq(o.active.index),o.oldIndex,o.active.index)},A=function(t){o.settings.autoControlsCombine?o.controls.autoEl.html(o.controls[t]):(o.controls.autoEl.find("a").removeClass("active"),o.controls.autoEl.find("a:not(.bx-"+t+")").addClass("active"))},W=function(){1==x()?(o.controls.prev.addClass("disabled"),o.controls.next.addClass("disabled")):!o.settings.infiniteLoop&&o.settings.hideControlOnEnd&&(0==o.active.index?(o.controls.prev.addClass("disabled"),o.controls.next.removeClass("disabled")):o.active.index==x()-1?(o.controls.next.addClass("disabled"),o.controls.prev.removeClass("disabled")):(o.controls.prev.removeClass("disabled"),o.controls.next.removeClass("disabled")))},H=function(){o.settings.autoDelay>0?setTimeout(r.startAuto,o.settings.autoDelay):r.startAuto(),o.settings.autoHover&&r.hover(function(){o.interval&&(r.stopAuto(!0),o.autoPaused=!0)},function(){o.autoPaused&&(r.startAuto(!0),o.autoPaused=null)})},L=function(){var e=0;if("next"==o.settings.autoDirection)r.append(o.children.clone().addClass("bx-clone"));else{r.prepend(o.children.clone().addClass("bx-clone"));var i=o.children.first().position();e="horizontal"==o.settings.mode?-i.left:-i.top}b(e,"reset",0),o.settings.pager=!1,o.settings.controls=!1,o.settings.autoControls=!1,o.settings.tickerHover&&!o.usingCSS&&o.viewport.hover(function(){r.stop()},function(){var e=0;o.children.each(function(){e+="horizontal"==o.settings.mode?t(this).outerWidth(!0):t(this).outerHeight(!0)});var i=o.settings.speed/e,s="horizontal"==o.settings.mode?"left":"top",n=i*(e-Math.abs(parseInt(r.css(s))));N(n)}),N()},N=function(t){speed=t?t:o.settings.speed;var e={left:0,top:0},i={left:0,top:0};"next"==o.settings.autoDirection?e=r.find(".bx-clone").first().position():i=o.children.first().position();var s="horizontal"==o.settings.mode?-e.left:-e.top,n="horizontal"==o.settings.mode?-i.left:-i.top,a={resetValue:n};b(s,"ticker",speed,a)},O=function(){o.touch={start:{x:0,y:0},end:{x:0,y:0}},o.viewport.bind("touchstart",X)},X=function(t){if(o.working)t.preventDefault();else{o.touch.originalPos=r.position();var e=t.originalEvent;o.touch.start.x=e.changedTouches[0].pageX,o.touch.start.y=e.changedTouches[0].pageY,o.viewport.bind("touchmove",Y),o.viewport.bind("touchend",V)}},Y=function(t){var e=t.originalEvent,i=Math.abs(e.changedTouches[0].pageX-o.touch.start.x),s=Math.abs(e.changedTouches[0].pageY-o.touch.start.y);if(3*i>s&&o.settings.preventDefaultSwipeX?t.preventDefault():3*s>i&&o.settings.preventDefaultSwipeY&&t.preventDefault(),"fade"!=o.settings.mode&&o.settings.oneToOneTouch){var n=0;if("horizontal"==o.settings.mode){var r=e.changedTouches[0].pageX-o.touch.start.x;n=o.touch.originalPos.left+r}else{var r=e.changedTouches[0].pageY-o.touch.start.y;n=o.touch.originalPos.top+r}b(n,"reset",0)}},V=function(t){o.viewport.unbind("touchmove",Y);var e=t.originalEvent,i=0;if(o.touch.end.x=e.changedTouches[0].pageX,o.touch.end.y=e.changedTouches[0].pageY,"fade"==o.settings.mode){var s=Math.abs(o.touch.start.x-o.touch.end.x);s>=o.settings.swipeThreshold&&(o.touch.start.x>o.touch.end.x?r.goToNextSlide():r.goToPrevSlide(),r.stopAuto())}else{var s=0;"horizontal"==o.settings.mode?(s=o.touch.end.x-o.touch.start.x,i=o.touch.originalPos.left):(s=o.touch.end.y-o.touch.start.y,i=o.touch.originalPos.top),!o.settings.infiniteLoop&&(0==o.active.index&&s>0||o.active.last&&0>s)?b(i,"reset",200):Math.abs(s)>=o.settings.swipeThreshold?(0>s?r.goToNextSlide():r.goToPrevSlide(),r.stopAuto()):b(i,"reset",200)}o.viewport.unbind("touchend",V)},B=function(){var e=t(window).width(),i=t(window).height();(a!=e||l!=i)&&(a=e,l=i,r.redrawSlider())};return r.goToSlide=function(e,i){if(!o.working&&o.active.index!=e)if(o.working=!0,o.oldIndex=o.active.index,o.active.index=0>e?x()-1:e>=x()?0:e,o.settings.onSlideBefore(o.children.eq(o.active.index),o.oldIndex,o.active.index),"next"==i?o.settings.onSlideNext(o.children.eq(o.active.index),o.oldIndex,o.active.index):"prev"==i&&o.settings.onSlidePrev(o.children.eq(o.active.index),o.oldIndex,o.active.index),o.active.last=o.active.index>=x()-1,o.settings.pager&&I(o.active.index),o.settings.controls&&W(),"fade"==o.settings.mode)o.settings.adaptiveHeight&&o.viewport.height()!=p()&&o.viewport.animate({height:p()},o.settings.adaptiveHeightSpeed),o.children.filter(":visible").fadeOut(o.settings.speed).css({zIndex:0}),o.children.eq(o.active.index).css("zIndex",51).fadeIn(o.settings.speed,function(){t(this).css("zIndex",50),D()});else{o.settings.adaptiveHeight&&o.viewport.height()!=p()&&o.viewport.animate({height:p()},o.settings.adaptiveHeightSpeed);var s=0,n={left:0,top:0};if(!o.settings.infiniteLoop&&o.carousel&&o.active.last)if("horizontal"==o.settings.mode){var a=o.children.eq(o.children.length-1);n=a.position(),s=o.viewport.width()-a.outerWidth()}else{var l=o.children.length-o.settings.minSlides;n=o.children.eq(l).position()}else if(o.carousel&&o.active.last&&"prev"==i){var d=1==o.settings.moveSlides?o.settings.maxSlides-m():(x()-1)*m()-(o.children.length-o.settings.maxSlides),a=r.children(".bx-clone").eq(d);n=a.position()}else if("next"==i&&0==o.active.index)n=r.find("> .bx-clone").eq(o.settings.maxSlides).position(),o.active.last=!1;else if(e>=0){var c=e*m();n=o.children.eq(c).position()}if("undefined"!=typeof n){var g="horizontal"==o.settings.mode?-(n.left-s):-n.top;b(g,"slide",o.settings.speed)}}},r.goToNextSlide=function(){if(o.settings.infiniteLoop||!o.active.last){var t=parseInt(o.active.index)+1;r.goToSlide(t,"next")}},r.goToPrevSlide=function(){if(o.settings.infiniteLoop||0!=o.active.index){var t=parseInt(o.active.index)-1;r.goToSlide(t,"prev")}},r.startAuto=function(t){o.interval||(o.interval=setInterval(function(){"next"==o.settings.autoDirection?r.goToNextSlide():r.goToPrevSlide()},o.settings.pause),o.settings.autoControls&&1!=t&&A("stop"))},r.stopAuto=function(t){o.interval&&(clearInterval(o.interval),o.interval=null,o.settings.autoControls&&1!=t&&A("start"))},r.getCurrentSlide=function(){return o.active.index},r.getSlideCount=function(){return o.children.length},r.redrawSlider=function(){o.children.add(r.find(".bx-clone")).outerWidth(u()),o.viewport.css("height",p()),o.settings.ticker||S(),o.active.last&&(o.active.index=x()-1),o.active.index>=x()&&(o.active.last=!0),o.settings.pager&&!o.settings.pagerCustom&&(w(),I(o.active.index))},r.destroySlider=function(){o.initialized&&(o.initialized=!1,t(".bx-clone",this).remove(),o.children.each(function(){void 0!=t(this).data("origStyle")?t(this).attr("style",t(this).data("origStyle")):t(this).removeAttr("style")}),void 0!=t(this).data("origStyle")?this.attr("style",t(this).data("origStyle")):t(this).removeAttr("style"),t(this).unwrap().unwrap(),o.controls.el&&o.controls.el.remove(),o.controls.next&&o.controls.next.remove(),o.controls.prev&&o.controls.prev.remove(),o.pagerEl&&o.pagerEl.remove(),t(".bx-caption",this).remove(),o.controls.autoEl&&o.controls.autoEl.remove(),clearInterval(o.interval),o.settings.responsive&&t(window).unbind("resize",B))},r.reloadSlider=function(t){void 0!=t&&(n=t),r.destroySlider(),d()},d(),this}}(jQuery);

/*
* jQuery Cycle2; v20130801
* http://jquery.malsup.com/cycle2/
* Copyright (c) 2013 M. Alsup; Dual licensed: MIT/GPL
*/
(function(e){"use strict";function t(e){return(e||"").toLowerCase()}var i="20130725";e.fn.cycle=function(i){var n;return 0!==this.length||e.isReady?this.each(function(){var n,s,o,c,r=e(this),l=e.fn.cycle.log;if(!r.data("cycle.opts")){(r.data("cycle-log")===!1||i&&i.log===!1||s&&s.log===!1)&&(l=e.noop),l("--c2 init--"),n=r.data();for(var a in n)n.hasOwnProperty(a)&&/^cycle[A-Z]+/.test(a)&&(c=n[a],o=a.match(/^cycle(.*)/)[1].replace(/^[A-Z]/,t),l(o+":",c,"("+typeof c+")"),n[o]=c);s=e.extend({},e.fn.cycle.defaults,n,i||{}),s.timeoutId=0,s.paused=s.paused||!1,s.container=r,s._maxZ=s.maxZ,s.API=e.extend({_container:r},e.fn.cycle.API),s.API.log=l,s.API.trigger=function(e,t){return s.container.trigger(e,t),s.API},r.data("cycle.opts",s),r.data("cycle.API",s.API),s.API.trigger("cycle-bootstrap",[s,s.API]),s.API.addInitialSlides(),s.API.preInitSlideshow(),s.slides.length&&s.API.initSlideshow()}}):(n={s:this.selector,c:this.context},e.fn.cycle.log("requeuing slideshow (dom not ready)"),e(function(){e(n.s,n.c).cycle(i)}),this)},e.fn.cycle.API={opts:function(){return this._container.data("cycle.opts")},addInitialSlides:function(){var t=this.opts(),i=t.slides;t.slideCount=0,t.slides=e(),i=i.jquery?i:t.container.find(i),t.random&&i.sort(function(){return Math.random()-.5}),t.API.add(i)},preInitSlideshow:function(){var t=this.opts();t.API.trigger("cycle-pre-initialize",[t]);var i=e.fn.cycle.transitions[t.fx];i&&e.isFunction(i.preInit)&&i.preInit(t),t._preInitialized=!0},postInitSlideshow:function(){var t=this.opts();t.API.trigger("cycle-post-initialize",[t]);var i=e.fn.cycle.transitions[t.fx];i&&e.isFunction(i.postInit)&&i.postInit(t)},initSlideshow:function(){var t,i=this.opts(),n=i.container;i.API.calcFirstSlide(),"static"==i.container.css("position")&&i.container.css("position","relative"),e(i.slides[i.currSlide]).css("opacity",1).show(),i.API.stackSlides(i.slides[i.currSlide],i.slides[i.nextSlide],!i.reverse),i.pauseOnHover&&(i.pauseOnHover!==!0&&(n=e(i.pauseOnHover)),n.hover(function(){i.API.pause(!0)},function(){i.API.resume(!0)})),i.timeout&&(t=i.API.getSlideOpts(i.nextSlide),i.API.queueTransition(t,t.timeout+i.delay)),i._initialized=!0,i.API.updateView(!0),i.API.trigger("cycle-initialized",[i]),i.API.postInitSlideshow()},pause:function(t){var i=this.opts(),n=i.API.getSlideOpts(),s=i.hoverPaused||i.paused;t?i.hoverPaused=!0:i.paused=!0,s||(i.container.addClass("cycle-paused"),i.API.trigger("cycle-paused",[i]).log("cycle-paused"),n.timeout&&(clearTimeout(i.timeoutId),i.timeoutId=0,i._remainingTimeout-=e.now()-i._lastQueue,(0>i._remainingTimeout||isNaN(i._remainingTimeout))&&(i._remainingTimeout=void 0)))},resume:function(e){var t=this.opts(),i=!t.hoverPaused&&!t.paused;e?t.hoverPaused=!1:t.paused=!1,i||(t.container.removeClass("cycle-paused"),0===t.slides.filter(":animated").length&&t.API.queueTransition(t.API.getSlideOpts(),t._remainingTimeout),t.API.trigger("cycle-resumed",[t,t._remainingTimeout]).log("cycle-resumed"))},add:function(t,i){var n,s=this.opts(),o=s.slideCount,c=!1;"string"==e.type(t)&&(t=e.trim(t)),e(t).each(function(){var t,n=e(this);i?s.container.prepend(n):s.container.append(n),s.slideCount++,t=s.API.buildSlideOpts(n),s.slides=i?e(n).add(s.slides):s.slides.add(n),s.API.initSlide(t,n,--s._maxZ),n.data("cycle.opts",t),s.API.trigger("cycle-slide-added",[s,t,n])}),s.API.updateView(!0),c=s._preInitialized&&2>o&&s.slideCount>=1,c&&(s._initialized?s.timeout&&(n=s.slides.length,s.nextSlide=s.reverse?n-1:1,s.timeoutId||s.API.queueTransition(s)):s.API.initSlideshow())},calcFirstSlide:function(){var e,t=this.opts();e=parseInt(t.startingSlide||0,10),(e>=t.slides.length||0>e)&&(e=0),t.currSlide=e,t.reverse?(t.nextSlide=e-1,0>t.nextSlide&&(t.nextSlide=t.slides.length-1)):(t.nextSlide=e+1,t.nextSlide==t.slides.length&&(t.nextSlide=0))},calcNextSlide:function(){var e,t=this.opts();t.reverse?(e=0>t.nextSlide-1,t.nextSlide=e?t.slideCount-1:t.nextSlide-1,t.currSlide=e?0:t.nextSlide+1):(e=t.nextSlide+1==t.slides.length,t.nextSlide=e?0:t.nextSlide+1,t.currSlide=e?t.slides.length-1:t.nextSlide-1)},calcTx:function(t,i){var n,s=t;return i&&s.manualFx&&(n=e.fn.cycle.transitions[s.manualFx]),n||(n=e.fn.cycle.transitions[s.fx]),n||(n=e.fn.cycle.transitions.fade,s.API.log('Transition "'+s.fx+'" not found.  Using fade.')),n},prepareTx:function(e,t){var i,n,s,o,c,r=this.opts();return 2>r.slideCount?(r.timeoutId=0,void 0):(!e||r.busy&&!r.manualTrump||(r.API.stopTransition(),r.busy=!1,clearTimeout(r.timeoutId),r.timeoutId=0),r.busy||(0!==r.timeoutId||e)&&(n=r.slides[r.currSlide],s=r.slides[r.nextSlide],o=r.API.getSlideOpts(r.nextSlide),c=r.API.calcTx(o,e),r._tx=c,e&&void 0!==o.manualSpeed&&(o.speed=o.manualSpeed),r.nextSlide!=r.currSlide&&(e||!r.paused&&!r.hoverPaused&&r.timeout)?(r.API.trigger("cycle-before",[o,n,s,t]),c.before&&c.before(o,n,s,t),i=function(){r.busy=!1,r.container.data("cycle.opts")&&(c.after&&c.after(o,n,s,t),r.API.trigger("cycle-after",[o,n,s,t]),r.API.queueTransition(o),r.API.updateView(!0))},r.busy=!0,c.transition?c.transition(o,n,s,t,i):r.API.doTransition(o,n,s,t,i),r.API.calcNextSlide(),r.API.updateView()):r.API.queueTransition(o)),void 0)},doTransition:function(t,i,n,s,o){var c=t,r=e(i),l=e(n),a=function(){l.animate(c.animIn||{opacity:1},c.speed,c.easeIn||c.easing,o)};l.css(c.cssBefore||{}),r.animate(c.animOut||{},c.speed,c.easeOut||c.easing,function(){r.css(c.cssAfter||{}),c.sync||a()}),c.sync&&a()},queueTransition:function(t,i){var n=this.opts(),s=void 0!==i?i:t.timeout;return 0===n.nextSlide&&0===--n.loop?(n.API.log("terminating; loop=0"),n.timeout=0,s?setTimeout(function(){n.API.trigger("cycle-finished",[n])},s):n.API.trigger("cycle-finished",[n]),n.nextSlide=n.currSlide,void 0):(s&&(n._lastQueue=e.now(),void 0===i&&(n._remainingTimeout=t.timeout),n.paused||n.hoverPaused||(n.timeoutId=setTimeout(function(){n.API.prepareTx(!1,!n.reverse)},s))),void 0)},stopTransition:function(){var e=this.opts();e.slides.filter(":animated").length&&(e.slides.stop(!1,!0),e.API.trigger("cycle-transition-stopped",[e])),e._tx&&e._tx.stopTransition&&e._tx.stopTransition(e)},advanceSlide:function(e){var t=this.opts();return clearTimeout(t.timeoutId),t.timeoutId=0,t.nextSlide=t.currSlide+e,0>t.nextSlide?t.nextSlide=t.slides.length-1:t.nextSlide>=t.slides.length&&(t.nextSlide=0),t.API.prepareTx(!0,e>=0),!1},buildSlideOpts:function(i){var n,s,o=this.opts(),c=i.data()||{};for(var r in c)c.hasOwnProperty(r)&&/^cycle[A-Z]+/.test(r)&&(n=c[r],s=r.match(/^cycle(.*)/)[1].replace(/^[A-Z]/,t),o.API.log("["+(o.slideCount-1)+"]",s+":",n,"("+typeof n+")"),c[s]=n);c=e.extend({},e.fn.cycle.defaults,o,c),c.slideNum=o.slideCount;try{delete c.API,delete c.slideCount,delete c.currSlide,delete c.nextSlide,delete c.slides}catch(l){}return c},getSlideOpts:function(t){var i=this.opts();void 0===t&&(t=i.currSlide);var n=i.slides[t],s=e(n).data("cycle.opts");return e.extend({},i,s)},initSlide:function(t,i,n){var s=this.opts();i.css(t.slideCss||{}),n>0&&i.css("zIndex",n),isNaN(t.speed)&&(t.speed=e.fx.speeds[t.speed]||e.fx.speeds._default),t.sync||(t.speed=t.speed/2),i.addClass(s.slideClass)},updateView:function(e){var t=this.opts();if(t._initialized){var i=t.API.getSlideOpts(),n=t.slides[t.currSlide];!e&&(t.API.trigger("cycle-update-view-before",[t,i,n]),0>t.updateView)||(t.slideActiveClass&&t.slides.removeClass(t.slideActiveClass).eq(t.currSlide).addClass(t.slideActiveClass),e&&t.hideNonActive&&t.slides.filter(":not(."+t.slideActiveClass+")").hide(),t.API.trigger("cycle-update-view",[t,i,n,e]),t.API.trigger("cycle-update-view-after",[t,i,n]))}},getComponent:function(t){var i=this.opts(),n=i[t];return"string"==typeof n?/^\s*[\>|\+|~]/.test(n)?i.container.find(n):e(n):n.jquery?n:e(n)},stackSlides:function(t,i,n){var s=this.opts();t||(t=s.slides[s.currSlide],i=s.slides[s.nextSlide],n=!s.reverse),e(t).css("zIndex",s.maxZ);var o,c=s.maxZ-2,r=s.slideCount;if(n){for(o=s.currSlide+1;r>o;o++)e(s.slides[o]).css("zIndex",c--);for(o=0;s.currSlide>o;o++)e(s.slides[o]).css("zIndex",c--)}else{for(o=s.currSlide-1;o>=0;o--)e(s.slides[o]).css("zIndex",c--);for(o=r-1;o>s.currSlide;o--)e(s.slides[o]).css("zIndex",c--)}e(i).css("zIndex",s.maxZ-1)},getSlideIndex:function(e){return this.opts().slides.index(e)}},e.fn.cycle.log=function(){window.console&&console.log&&console.log("[cycle2] "+Array.prototype.join.call(arguments," "))},e.fn.cycle.version=function(){return"Cycle2: "+i},e.fn.cycle.transitions={custom:{},none:{before:function(e,t,i,n){e.API.stackSlides(i,t,n),e.cssBefore={opacity:1,display:"block"}}},fade:{before:function(t,i,n,s){var o=t.API.getSlideOpts(t.nextSlide).slideCss||{};t.API.stackSlides(i,n,s),t.cssBefore=e.extend(o,{opacity:0,display:"block"}),t.animIn={opacity:1},t.animOut={opacity:0}}},fadeout:{before:function(t,i,n,s){var o=t.API.getSlideOpts(t.nextSlide).slideCss||{};t.API.stackSlides(i,n,s),t.cssBefore=e.extend(o,{opacity:1,display:"block"}),t.animOut={opacity:0}}},scrollHorz:{before:function(e,t,i,n){e.API.stackSlides(t,i,n);var s=e.container.css("overflow","hidden").width();e.cssBefore={left:n?s:-s,top:0,opacity:1,display:"block"},e.cssAfter={zIndex:e._maxZ-2,left:0},e.animIn={left:0},e.animOut={left:n?-s:s}}}},e.fn.cycle.defaults={allowWrap:!0,autoSelector:".cycle-slideshow[data-cycle-auto-init!=false]",delay:0,easing:null,fx:"fade",hideNonActive:!0,loop:0,manualFx:void 0,manualSpeed:void 0,manualTrump:!0,maxZ:100,pauseOnHover:!1,reverse:!1,slideActiveClass:"cycle-slide-active",slideClass:"cycle-slide",slideCss:{position:"absolute",top:0,left:0},slides:"> img",speed:500,startingSlide:0,sync:!0,timeout:4e3,updateView:-1},e(document).ready(function(){e(e.fn.cycle.defaults.autoSelector).cycle()})})(jQuery),function(e){"use strict";function t(t,n){var s,o,c,r=n.autoHeight;if("container"==r)o=e(n.slides[n.currSlide]).outerHeight(),n.container.height(o);else if(n._autoHeightRatio)n.container.height(n.container.width()/n._autoHeightRatio);else if("calc"===r||"number"==e.type(r)&&r>=0){if(c="calc"===r?i(t,n):r>=n.slides.length?0:r,c==n._sentinelIndex)return;n._sentinelIndex=c,n._sentinel&&n._sentinel.remove(),s=e(n.slides[c].cloneNode(!0)),s.removeAttr("id name rel").find("[id],[name],[rel]").removeAttr("id name rel"),s.css({position:"static",visibility:"hidden",display:"block"}).prependTo(n.container).addClass("cycle-sentinel cycle-slide").removeClass("cycle-slide-active"),s.find("*").css("visibility","hidden"),n._sentinel=s}}function i(t,i){var n=0,s=-1;return i.slides.each(function(t){var i=e(this).height();i>s&&(s=i,n=t)}),n}function n(t,i,n,s){var o=e(s).outerHeight(),c=i.sync?i.speed/2:i.speed;i.container.animate({height:o},c)}function s(i,o){o._autoHeightOnResize&&(e(window).off("resize orientationchange",o._autoHeightOnResize),o._autoHeightOnResize=null),o.container.off("cycle-slide-added cycle-slide-removed",t),o.container.off("cycle-destroyed",s),o.container.off("cycle-before",n),o._sentinel&&(o._sentinel.remove(),o._sentinel=null)}e.extend(e.fn.cycle.defaults,{autoHeight:0}),e(document).on("cycle-initialized",function(i,o){function c(){t(i,o)}var r,l=o.autoHeight,a=e.type(l),d=null;("string"===a||"number"===a)&&(o.container.on("cycle-slide-added cycle-slide-removed",t),o.container.on("cycle-destroyed",s),"container"==l?o.container.on("cycle-before",n):"string"===a&&/\d+\:\d+/.test(l)&&(r=l.match(/(\d+)\:(\d+)/),r=r[1]/r[2],o._autoHeightRatio=r),"number"!==a&&(o._autoHeightOnResize=function(){clearTimeout(d),d=setTimeout(c,50)},e(window).on("resize orientationchange",o._autoHeightOnResize)),setTimeout(c,30))})}(jQuery),function(e){"use strict";e.extend(e.fn.cycle.defaults,{caption:"> .cycle-caption",captionTemplate:"{{slideNum}} / {{slideCount}}",overlay:"> .cycle-overlay",overlayTemplate:"<div>{{title}}</div><div>{{desc}}</div>",captionModule:"caption"}),e(document).on("cycle-update-view",function(t,i,n,s){"caption"===i.captionModule&&e.each(["caption","overlay"],function(){var e=this,t=n[e+"Template"],o=i.API.getComponent(e);o.length&&t?(o.html(i.API.tmpl(t,n,i,s)),o.show()):o.hide()})}),e(document).on("cycle-destroyed",function(t,i){var n;e.each(["caption","overlay"],function(){var e=this,t=i[e+"Template"];i[e]&&t&&(n=i.API.getComponent("caption"),n.empty())})})}(jQuery),function(e){"use strict";var t=e.fn.cycle;e.fn.cycle=function(i){var n,s,o,c=e.makeArray(arguments);return"number"==e.type(i)?this.cycle("goto",i):"string"==e.type(i)?this.each(function(){var r;return n=i,o=e(this).data("cycle.opts"),void 0===o?(t.log('slideshow must be initialized before sending commands; "'+n+'" ignored'),void 0):(n="goto"==n?"jump":n,s=o.API[n],e.isFunction(s)?(r=e.makeArray(c),r.shift(),s.apply(o.API,r)):(t.log("unknown command: ",n),void 0))}):t.apply(this,arguments)},e.extend(e.fn.cycle,t),e.extend(t.API,{next:function(){var e=this.opts();if(!e.busy||e.manualTrump){var t=e.reverse?-1:1;e.allowWrap===!1&&e.currSlide+t>=e.slideCount||(e.API.advanceSlide(t),e.API.trigger("cycle-next",[e]).log("cycle-next"))}},prev:function(){var e=this.opts();if(!e.busy||e.manualTrump){var t=e.reverse?1:-1;e.allowWrap===!1&&0>e.currSlide+t||(e.API.advanceSlide(t),e.API.trigger("cycle-prev",[e]).log("cycle-prev"))}},destroy:function(){this.stop();var t=this.opts(),i=e.isFunction(e._data)?e._data:e.noop;clearTimeout(t.timeoutId),t.timeoutId=0,t.API.stop(),t.API.trigger("cycle-destroyed",[t]).log("cycle-destroyed"),t.container.removeData(),i(t.container[0],"parsedAttrs",!1),t.retainStylesOnDestroy||(t.container.removeAttr("style"),t.slides.removeAttr("style"),t.slides.removeClass(t.slideActiveClass)),t.slides.each(function(){e(this).removeData(),i(this,"parsedAttrs",!1)})},jump:function(e){var t,i=this.opts();if(!i.busy||i.manualTrump){var n=parseInt(e,10);if(isNaN(n)||0>n||n>=i.slides.length)return i.API.log("goto: invalid slide index: "+n),void 0;if(n==i.currSlide)return i.API.log("goto: skipping, already on slide",n),void 0;i.nextSlide=n,clearTimeout(i.timeoutId),i.timeoutId=0,i.API.log("goto: ",n," (zero-index)"),t=i.currSlide<i.nextSlide,i.API.prepareTx(!0,t)}},stop:function(){var t=this.opts(),i=t.container;clearTimeout(t.timeoutId),t.timeoutId=0,t.API.stopTransition(),t.pauseOnHover&&(t.pauseOnHover!==!0&&(i=e(t.pauseOnHover)),i.off("mouseenter mouseleave")),t.API.trigger("cycle-stopped",[t]).log("cycle-stopped")},reinit:function(){var e=this.opts();e.API.destroy(),e.container.cycle()},remove:function(t){for(var i,n,s=this.opts(),o=[],c=1,r=0;s.slides.length>r;r++)i=s.slides[r],r==t?n=i:(o.push(i),e(i).data("cycle.opts").slideNum=c,c++);n&&(s.slides=e(o),s.slideCount--,e(n).remove(),t==s.currSlide?s.API.advanceSlide(1):s.currSlide>t?s.currSlide--:s.currSlide++,s.API.trigger("cycle-slide-removed",[s,t,n]).log("cycle-slide-removed"),s.API.updateView())}}),e(document).on("click.cycle","[data-cycle-cmd]",function(t){t.preventDefault();var i=e(this),n=i.data("cycle-cmd"),s=i.data("cycle-context")||".cycle-slideshow";e(s).cycle(n,i.data("cycle-arg"))})}(jQuery),function(e){"use strict";function t(t,i){var n;return t._hashFence?(t._hashFence=!1,void 0):(n=window.location.hash.substring(1),t.slides.each(function(s){return e(this).data("cycle-hash")==n?(i===!0?t.startingSlide=s:(t.nextSlide=s,t.API.prepareTx(!0,!1)),!1):void 0}),void 0)}e(document).on("cycle-pre-initialize",function(i,n){t(n,!0),n._onHashChange=function(){t(n,!1)},e(window).on("hashchange",n._onHashChange)}),e(document).on("cycle-update-view",function(e,t,i){i.hash&&"#"+i.hash!=window.location.hash&&(t._hashFence=!0,window.location.hash=i.hash)}),e(document).on("cycle-destroyed",function(t,i){i._onHashChange&&e(window).off("hashchange",i._onHashChange)})}(jQuery),function(e){"use strict";e.extend(e.fn.cycle.defaults,{loader:!1}),e(document).on("cycle-bootstrap",function(t,i){function n(t,n){function o(t){var o;"wait"==i.loader?(r.push(t),0===a&&(r.sort(c),s.apply(i.API,[r,n]),i.container.removeClass("cycle-loading"))):(o=e(i.slides[i.currSlide]),s.apply(i.API,[t,n]),o.show(),i.container.removeClass("cycle-loading"))}function c(e,t){return e.data("index")-t.data("index")}var r=[];if("string"==e.type(t))t=e.trim(t);else if("array"===e.type(t))for(var l=0;t.length>l;l++)t[l]=e(t[l])[0];t=e(t);var a=t.length;a&&(t.hide().appendTo("body").each(function(t){function c(){0===--l&&(--a,o(d))}var l=0,d=e(this),u=d.is("img")?d:d.find("img");return d.data("index",t),u=u.filter(":not(.cycle-loader-ignore)").filter(':not([src=""])'),u.length?(l=u.length,u.each(function(){this.complete?c():e(this).load(function(){c()}).error(function(){0===--l&&(i.API.log("slide skipped; img not loaded:",this.src),0===--a&&"wait"==i.loader&&s.apply(i.API,[r,n]))})}),void 0):(--a,r.push(d),void 0)}),a&&i.container.addClass("cycle-loading"))}var s;i.loader&&(s=i.API.add,i.API.add=n)})}(jQuery),function(e){"use strict";function t(t,i,n){var s,o=t.API.getComponent("pager");o.each(function(){var o=e(this);if(i.pagerTemplate){var c=t.API.tmpl(i.pagerTemplate,i,t,n[0]);s=e(c).appendTo(o)}else s=o.children().eq(t.slideCount-1);s.on(t.pagerEvent,function(e){e.preventDefault(),t.API.page(o,e.currentTarget)})})}function i(e,t){var i=this.opts();if(!i.busy||i.manualTrump){var n=e.children().index(t),s=n,o=s>i.currSlide;i.currSlide!=s&&(i.nextSlide=s,i.API.prepareTx(!0,o),i.API.trigger("cycle-pager-activated",[i,e,t]))}}e.extend(e.fn.cycle.defaults,{pager:"> .cycle-pager",pagerActiveClass:"cycle-pager-active",pagerEvent:"click.cycle",pagerTemplate:"<span>&bull;</span>"}),e(document).on("cycle-bootstrap",function(e,i,n){n.buildPagerLink=t}),e(document).on("cycle-slide-added",function(e,t,n,s){t.pager&&(t.API.buildPagerLink(t,n,s),t.API.page=i)}),e(document).on("cycle-slide-removed",function(t,i,n){if(i.pager){var s=i.API.getComponent("pager");s.each(function(){var t=e(this);e(t.children()[n]).remove()})}}),e(document).on("cycle-update-view",function(t,i){var n;i.pager&&(n=i.API.getComponent("pager"),n.each(function(){e(this).children().removeClass(i.pagerActiveClass).eq(i.currSlide).addClass(i.pagerActiveClass)}))}),e(document).on("cycle-destroyed",function(e,t){var i=t.API.getComponent("pager");i&&(i.children().off(t.pagerEvent),t.pagerTemplate&&i.empty())})}(jQuery),function(e){"use strict";e.extend(e.fn.cycle.defaults,{next:"> .cycle-next",nextEvent:"click.cycle",disabledClass:"disabled",prev:"> .cycle-prev",prevEvent:"click.cycle",swipe:!1}),e(document).on("cycle-initialized",function(e,t){if(t.API.getComponent("next").on(t.nextEvent,function(e){e.preventDefault(),t.API.next()}),t.API.getComponent("prev").on(t.prevEvent,function(e){e.preventDefault(),t.API.prev()}),t.swipe){var i=t.swipeVert?"swipeUp.cycle":"swipeLeft.cycle swipeleft.cycle",n=t.swipeVert?"swipeDown.cycle":"swipeRight.cycle swiperight.cycle";t.container.on(i,function(){t.API.next()}),t.container.on(n,function(){t.API.prev()})}}),e(document).on("cycle-update-view",function(e,t){if(!t.allowWrap){var i=t.disabledClass,n=t.API.getComponent("next"),s=t.API.getComponent("prev"),o=t._prevBoundry||0,c=void 0!==t._nextBoundry?t._nextBoundry:t.slideCount-1;t.currSlide==c?n.addClass(i).prop("disabled",!0):n.removeClass(i).prop("disabled",!1),t.currSlide===o?s.addClass(i).prop("disabled",!0):s.removeClass(i).prop("disabled",!1)}}),e(document).on("cycle-destroyed",function(e,t){t.API.getComponent("prev").off(t.nextEvent),t.API.getComponent("next").off(t.prevEvent),t.container.off("swipeleft.cycle swiperight.cycle swipeLeft.cycle swipeRight.cycle swipeUp.cycle swipeDown.cycle")})}(jQuery),function(e){"use strict";e.extend(e.fn.cycle.defaults,{progressive:!1}),e(document).on("cycle-pre-initialize",function(t,i){if(i.progressive){var n,s,o=i.API,c=o.next,r=o.prev,l=o.prepareTx,a=e.type(i.progressive);if("array"==a)n=i.progressive;else if(e.isFunction(i.progressive))n=i.progressive(i);else if("string"==a){if(s=e(i.progressive),n=e.trim(s.html()),!n)return;if(/^(\[)/.test(n))try{n=e.parseJSON(n)}catch(d){return o.log("error parsing progressive slides",d),void 0}else n=n.split(RegExp(s.data("cycle-split")||"\n")),n[n.length-1]||n.pop()}l&&(o.prepareTx=function(e,t){var s,o;return e||0===n.length?(l.apply(i.API,[e,t]),void 0):(t&&i.currSlide==i.slideCount-1?(o=n[0],n=n.slice(1),i.container.one("cycle-slide-added",function(e,t){setTimeout(function(){t.API.advanceSlide(1)},50)}),i.API.add(o)):t||0!==i.currSlide?l.apply(i.API,[e,t]):(s=n.length-1,o=n[s],n=n.slice(0,s),i.container.one("cycle-slide-added",function(e,t){setTimeout(function(){t.currSlide=1,t.API.advanceSlide(-1)},50)}),i.API.add(o,!0)),void 0)}),c&&(o.next=function(){var e=this.opts();if(n.length&&e.currSlide==e.slideCount-1){var t=n[0];n=n.slice(1),e.container.one("cycle-slide-added",function(e,t){c.apply(t.API),t.container.removeClass("cycle-loading")}),e.container.addClass("cycle-loading"),e.API.add(t)}else c.apply(e.API)}),r&&(o.prev=function(){var e=this.opts();if(n.length&&0===e.currSlide){var t=n.length-1,i=n[t];n=n.slice(0,t),e.container.one("cycle-slide-added",function(e,t){t.currSlide=1,t.API.advanceSlide(-1),t.container.removeClass("cycle-loading")}),e.container.addClass("cycle-loading"),e.API.add(i,!0)}else r.apply(e.API)})}})}(jQuery),function(e){"use strict";e.extend(e.fn.cycle.defaults,{tmplRegex:"{{((.)?.*?)}}"}),e.extend(e.fn.cycle.API,{tmpl:function(t,i){var n=RegExp(i.tmplRegex||e.fn.cycle.defaults.tmplRegex,"g"),s=e.makeArray(arguments);return s.shift(),t.replace(n,function(t,i){var n,o,c,r,l=i.split(".");for(n=0;s.length>n;n++)if(c=s[n]){if(l.length>1)for(r=c,o=0;l.length>o;o++)c=r,r=r[l[o]]||i;else r=c[i];if(e.isFunction(r))return r.apply(c,s);if(void 0!==r&&null!==r&&r!=i)return r}return i})}})}(jQuery);

/*
  Masked Input plugin for jQuery
  Copyright (c) 2007-2013 Josh Bush (digitalbush.com)
  Licensed under the MIT license (http://digitalbush.com/projects/masked-input-plugin/#license)
  Version: 1.3.1
*/
(function(e){function t(){var e=document.createElement("input"),t="onpaste";return e.setAttribute(t,""),"function"==typeof e[t]?"paste":"input"}var n,a=t()+".mask",r=navigator.userAgent,i=/iphone/i.test(r),o=/android/i.test(r);e.mask={definitions:{9:"[0-9]",a:"[A-Za-z]","*":"[A-Za-z0-9]"},dataName:"rawMaskFn",placeholder:"_"},e.fn.extend({caret:function(e,t){var n;if(0!==this.length&&!this.is(":hidden"))return"number"==typeof e?(t="number"==typeof t?t:e,this.each(function(){this.setSelectionRange?this.setSelectionRange(e,t):this.createTextRange&&(n=this.createTextRange(),n.collapse(!0),n.moveEnd("character",t),n.moveStart("character",e),n.select())})):(this[0].setSelectionRange?(e=this[0].selectionStart,t=this[0].selectionEnd):document.selection&&document.selection.createRange&&(n=document.selection.createRange(),e=0-n.duplicate().moveStart("character",-1e5),t=e+n.text.length),{begin:e,end:t})},unmask:function(){return this.trigger("unmask")},mask:function(t,r){var c,l,s,u,f,h;return!t&&this.length>0?(c=e(this[0]),c.data(e.mask.dataName)()):(r=e.extend({placeholder:e.mask.placeholder,completed:null},r),l=e.mask.definitions,s=[],u=h=t.length,f=null,e.each(t.split(""),function(e,t){"?"==t?(h--,u=e):l[t]?(s.push(RegExp(l[t])),null===f&&(f=s.length-1)):s.push(null)}),this.trigger("unmask").each(function(){function c(e){for(;h>++e&&!s[e];);return e}function d(e){for(;--e>=0&&!s[e];);return e}function m(e,t){var n,a;if(!(0>e)){for(n=e,a=c(t);h>n;n++)if(s[n]){if(!(h>a&&s[n].test(R[a])))break;R[n]=R[a],R[a]=r.placeholder,a=c(a)}b(),x.caret(Math.max(f,e))}}function p(e){var t,n,a,i;for(t=e,n=r.placeholder;h>t;t++)if(s[t]){if(a=c(t),i=R[t],R[t]=n,!(h>a&&s[a].test(i)))break;n=i}}function g(e){var t,n,a,r=e.which;8===r||46===r||i&&127===r?(t=x.caret(),n=t.begin,a=t.end,0===a-n&&(n=46!==r?d(n):a=c(n-1),a=46===r?c(a):a),k(n,a),m(n,a-1),e.preventDefault()):27==r&&(x.val(S),x.caret(0,y()),e.preventDefault())}function v(t){var n,a,i,l=t.which,u=x.caret();t.ctrlKey||t.altKey||t.metaKey||32>l||l&&(0!==u.end-u.begin&&(k(u.begin,u.end),m(u.begin,u.end-1)),n=c(u.begin-1),h>n&&(a=String.fromCharCode(l),s[n].test(a)&&(p(n),R[n]=a,b(),i=c(n),o?setTimeout(e.proxy(e.fn.caret,x,i),0):x.caret(i),r.completed&&i>=h&&r.completed.call(x))),t.preventDefault())}function k(e,t){var n;for(n=e;t>n&&h>n;n++)s[n]&&(R[n]=r.placeholder)}function b(){x.val(R.join(""))}function y(e){var t,n,a=x.val(),i=-1;for(t=0,pos=0;h>t;t++)if(s[t]){for(R[t]=r.placeholder;pos++<a.length;)if(n=a.charAt(pos-1),s[t].test(n)){R[t]=n,i=t;break}if(pos>a.length)break}else R[t]===a.charAt(pos)&&t!==u&&(pos++,i=t);return e?b():u>i+1?(x.val(""),k(0,h)):(b(),x.val(x.val().substring(0,i+1))),u?t:f}var x=e(this),R=e.map(t.split(""),function(e){return"?"!=e?l[e]?r.placeholder:e:void 0}),S=x.val();x.data(e.mask.dataName,function(){return e.map(R,function(e,t){return s[t]&&e!=r.placeholder?e:null}).join("")}),x.attr("readonly")||x.one("unmask",function(){x.unbind(".mask").removeData(e.mask.dataName)}).bind("focus.mask",function(){clearTimeout(n);var e;S=x.val(),e=y(),n=setTimeout(function(){b(),e==t.length?x.caret(0,e):x.caret(e)},10)}).bind("blur.mask",function(){y(),x.val()!=S&&x.change()}).bind("keydown.mask",g).bind("keypress.mask",v).bind(a,function(){setTimeout(function(){var e=y(!0);x.caret(e),r.completed&&e==x.val().length&&r.completed.call(x)},0)}),y()}))}})})(jQuery);