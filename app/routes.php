<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/**
 * Rotas de autenticação
 */
Route::get('painel/logout',  array('as' => 'painel.logout',      'uses' => 'App\Controllers\Painel\AuthController@getLogout'));
Route::get('painel/login',   array('as' => 'painel.login',       'uses' => 'App\Controllers\Painel\AuthController@getLogin'));
Route::post('painel/login',  array('as' => 'painel.login.post',  'uses' => 'App\Controllers\Painel\AuthController@postLogin'));
Route::resource('groups', 'App\Controllers\Painel\GroupController');

/**
 * Rotas do painel
 */
Route::group(array('prefix' => 'painel', 'before' => 'auth.admin'), function() {
    Route::any('/', 'App\Controllers\Painel\PaginasController@index');
    Route::resource('paginas', 'App\Controllers\Painel\PaginasController');
    Route::resource('colaboradores', 'App\Controllers\Painel\ColaboradorsController');
    Route::resource('atuacoes', 'App\Controllers\Painel\AtuacoesController');
    Route::resource('categorias', 'App\Controllers\Painel\CategoriasController');
    Route::resource('produtos', 'App\Controllers\Painel\ProdutosController');
    Route::resource('aplicacoes', 'App\Controllers\Painel\FeaturesController');
    Route::resource('noticias', 'App\Controllers\Painel\NoticiasController');
    Route::resource('midias', 'App\Controllers\Painel\MidiasController');
    Route::resource('midiatipos', 'App\Controllers\Painel\MidiatiposController');
    Route::resource('locais', 'App\Controllers\Painel\LocalsController');
    Route::resource('especificas', 'App\Controllers\Painel\EspecificasController');
    Route::get('especificas/json/{geralId}', 'App\Controllers\Painel\EspecificasController@getEspecificas');
    Route::resource('arquivos', 'App\Controllers\Painel\ArquivosController');
    Route::resource('arquivosEn', 'App\Controllers\Painel\ArquivosEnController');
    Route::resource('socials', 'App\Controllers\Painel\SocialsController');
    Route::resource('users', 'App\Controllers\Painel\UsersController');
    Route::resource('popups', 'App\Controllers\Painel\PopupsController');
    Route::resource('popup', 'App\Controllers\Painel\PopupsController');

    //Ordenamento de ítens
    Route::post('ordenar', 'App\Controllers\Painel\OrdenadorController@ordenar');
});

/**
 * Rota da home
 */
Route::get('/', function(){
    $atuacoes = Atuacao::ordered()->get();

    $textoHome = Pagina::where('slug', 'home')->first();
    $textoHome = $textoHome->traduz('texto');

    $noticias = Noticia::published()->limit(3)->get();

    $idioma = \Config::get('app.locale');
    $getPopup = Popup::publicado()->periodoAtual()->idioma($idioma)->first();

    if($getPopup)
        $popup = $getPopup->toJson();
    else
        $popup = false;

    View::share('popup', $popup);

    return View::make('site.home.index', compact('atuacoes', 'textoHome', 'noticias', 'popup'));
});

/**
 * Rotas do front-end
 */
Route::get('empresa', 'App\Controllers\Site\PaginasController@empresa');
Route::get('solucoes', 'App\Controllers\Site\AtuacoesController@lista');
Route::get('solucoes/area/{solucaSlug}', 'App\Controllers\Site\AtuacoesController@area');
Route::get('solucoes/produto/{produtoSlug}', 'App\Controllers\Site\AtuacoesController@produto');
Route::get('parceiros', 'App\Controllers\Site\PaginasController@parceiros');
Route::get('noticias', 'App\Controllers\Site\NoticiasController@lista');
Route::get('noticias/detalhe/{noticiaSlug}', 'App\Controllers\Site\NoticiasController@detalhe');
Route::get('midia', 'App\Controllers\Site\MidiasController@lista');
Route::get('midia/lista/{midiaTipo}', 'App\Controllers\Site\MidiasController@lista');
Route::get('midia/video/{videoId}', 'App\Controllers\Site\MidiasController@video');
Route::get('suporte', function(){
    return View::make('site.suporte.index');
});
Route::get('contato', 'App\Controllers\Site\ContatoController@index');
Route::post('contato', 'App\Controllers\Site\ContatoController@sendMail');
Route::get('ativar', 'App\Controllers\Site\ContatoController@ativacao');
Route::post('ativar', 'App\Controllers\Site\ContatoController@enviarAtivacao');

Route::post('busca', function(){
    // solucoes/produto/slug
    // solucoes/area/slug
    $termo = Input::get('busca');
    $query = <<<QRY
(SELECT
produtos.titulo as 'titulo',
produtos.titulo_en as 'titulo_en',
CONCAT('solucoes/produto/', produtos.slug) as 'url',
produtos.descricao as 'olho',
produtos.descricao_en as 'olho_en'
FROM produtos WHERE produtos.titulo like '%{$termo}%' OR produtos.titulo_en like '%{$termo}%')
UNION
(SELECT
atuacaos.titulo as 'titulo',
atuacaos.titulo_en as 'titulo_en',
CONCAT('solucoes/area/', atuacaos.slug) as 'url',
atuacaos.descricao as 'olho',
atuacaos.descricao_en as 'olho_en'
FROM atuacaos WHERE atuacaos.titulo like '%{$termo}%' OR atuacaos.titulo_en like '%{$termo}%')
QRY;
    $resultados = \DB::select(\DB::raw($query));
    return View::make('site.busca')->with('resultados', $resultados)->with('termo', $termo);
});

/**
 * Composers
 */
View::composer('site.layout.default', 'App\Composers\ContactsComposer');
View::composer('site.layout.default', 'App\Composers\SocialsComposer');

/* LOCALIZAÇÃO */
Route::get('idioma/{sigla_idioma?}', function($sigla_idioma){
    if($sigla_idioma == 'pt-br' || $sigla_idioma == 'en')
        Session::put('locale', $sigla_idioma);

    //return Redirect::back();
    return Redirect::to('/');
});