<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase {

	/**
	 * Creates the application.
	 *
	 * @return Symfony\Component\HttpKernel\HttpKernelInterface
	 */
	public function createApplication()
	{
		$unitTesting = true;

		$testEnvironment = 'testing';

		return require __DIR__.'/../../bootstrap/start.php';
	}

	public function setUp()
	{
	    parent::setUp();
	    $user = Sentry::getUserProvider()->findById(1);
	    Sentry::login($user);
	}

	public function tearDown()
	{
	    Sentry::logout();
	}

}
