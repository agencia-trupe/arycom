<?php namespace App\Composers;

/**
 * SocialsComposer.php
 *
 * SocialsComposer
 *
 * Compartilha os dados de redes sociais com a view de layout.
 *
 * @package     Arycom
 * @category    Composers
 * @framework   Laravel
 * @author      Nilton de Freitas - Trupe Agência Criativa
 * @link        http://trupe.net
 * @version     0.1
 * @todo        -
 */

use Social;

class SocialsComposer
{
    public function compose($view)
    {
        $socials = Social::ordered()->get();
        $view->with('socials', $socials);
    }

}
