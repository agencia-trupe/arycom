<?php

class Arquivo extends \EloquentBaseModel\Base {
	protected $guarded = array();

	public static $rules = array();

	public function produto()
	{
		return $this->belongsTo('Produto');
	}
}
