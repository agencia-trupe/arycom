<?php

class Social extends \EloquentBaseModel\Base
{
    protected $guarded = array();

    public static $rules = array(
        'titulo' => 'required',
        'link' => 'required',
    );
}
