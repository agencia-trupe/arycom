<?php

class Pagina extends \EloquentBaseModel\Base
{
    protected $guarded = array();

    public static $rules = array(
        'titulo' => 'required',
        'user_id' => 'required'
    );
}
