<?php

class Local extends \EloquentBaseModel\Base
{
    protected $guarded = array();

    public static $rules = array(
        'titulo' => 'required',
        'endereco' => 'required',
        'numero' => 'required',
        'complemento' => 'required',
        'bairro' => 'required',
        'cidade' => 'required',
        'cep' => 'required',
        'uf' => 'required',
        'email' => 'required',
    );
}
