<?php

class Midia extends \EloquentBaseModel\Base
{
    protected $guarded = array();

    public static $rules = array(
        'titulo' => 'required',
        'slug' => 'required',
        'descricao' => 'required',
    );

    public function midiatipo()
    {
        return $this->belongsTo('Midiatipo');
    }

    public function getPathAttribute( $value )
    {
        if (strpos($value, 'http') === 0) {
            return Video::getVideoId($value);
        } else {
            return $value;
        }
    }
}
