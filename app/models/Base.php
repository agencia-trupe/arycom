<?php namespace EloquentBaseModel;

class Base extends \Eloquent
{

	public function traduz($campo)
	{
		$idioma = \Config::get('app.locale');

		if($idioma == 'en')
			return $this->{$campo.'_en'};
		else
			return $this->{$campo};
	}

    public function scopeOrdered($query)
    {
        return $query->orderBy('ordem', 'asc');
    }
}
