<?php

use Carbon\Carbon;

class Noticia extends \EloquentBaseModel\Base
{
    protected $guarded = array();

    public static $rules = array(
        'titulo' => 'required',
        'slug' => 'required',
        'published_time' => 'required',
    );

    public function scopePublished($query)
    {
        return $query->orderBy('published', 'desc')->where('published', '<=', Carbon::now('America/Sao_Paulo'));
    }

    public function scopeFiltroLinguagem($query){
        $idioma = \Config::get('app.locale');

        if($idioma == 'en')
            return $query->where('descricao_en', '!=', '');
        else
            return $query->where('descricao', '!=', '');
    }

}
