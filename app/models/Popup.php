<?php

use Carbon\Carbon;

class Popup extends \EloquentBaseModel\Base
{
    public $timestamps = false;

    protected $guarded = array();

    public function setDataInicioAttribute($value)
    {
        list($dia,$mes,$ano) = explode('/', $value);
        $this->attributes['data_inicio'] = $ano.'-'.$mes.'-'.$dia;
    }

    public function setDataTerminoAttribute($value)
    {
        list($dia,$mes,$ano) = explode('/', $value);
        $this->attributes['data_termino'] = $ano.'-'.$mes.'-'.$dia;
    }

    public function getDataInicioAttribute($value)
    {
        return date("d/m/Y", strtotime($value));
    }

    public function getDataTerminoAttribute($value)
    {
        return date("d/m/Y", strtotime($value));
    }

    public function scopePt($query)
    {
        return $query->where('idioma', '=', 'pt-br');
    }

    public function scopeEn($query)
    {
        return $query->where('idioma', '=', 'en');
    }

    public function scopeIdioma($query, $idioma)
    {
        return $query->where('idioma', '=', $idioma);
    }

    public function scopePublicado($query)
    {
        return $query->where('publicar', '=', '1');
    }

    public function scopePeriodoAtual($query)
    {
        return $query->where('data_inicio', '<=', Date('Y-m-d'))
                     ->where('data_termino', '>', Date('Y-m-d'));
    }

    public function scopeFiltroLinguagem($query){
        $idioma = \Config::get('app.locale');

        if($idioma == 'en')
            return $query->where('descricao_en', '!=', '');
        else
            return $query->where('descricao', '!=', '');
    }

}
