<?php

class Atuacao extends \EloquentBaseModel\Base
{
    protected $guarded = array();

    public static $rules = array(
        'titulo' => 'required',
        'slug' => 'unique:atuacaos,slug',
        'resumo' => 'required',
        'descricao' => 'required',
    );

    public function produtos()
    {
        return $this->belongsToMany('Produto');
    }

    public function especificas()
    {
        return $this->hasMany('Especifica')->orderBy('ordem', 'asc');
    }
}
