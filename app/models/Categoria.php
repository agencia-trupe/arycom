<?php

class Categoria extends \EloquentBaseModel\Base
{
    protected $guarded = array();

    public static $rules = array(
        'titulo' => 'required',
        'slug' => 'unique:atuacaos,slug',
        'descricao' => 'required',
    );

    public function produtos()
    {
        return $this->hasMany('Produto')->orderBy('ordem', 'asc');
    }
}
