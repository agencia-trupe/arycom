<?php

class Produto extends \EloquentBaseModel\Base
{
    protected $guarded = array();

    public static $rules = array(
        'titulo' => 'required',
        'slug' => 'required',
        'descricao' => 'required',
        'categoria_id' => 'required'
    );

    public function categoria()
    {
        return $this->belongsTo('Categoria');
    }

    public function atuacoes()
    {
        return $this->belongsToMany('Atuacao');
    }

    public function features()
    {
        return $this->belongsToMany('Feature');
    }

    public function arquivos()
    {
        return $this->hasMany('Arquivo');
    }

    public function arquivosEn()
    {
        return $this->hasMany('ArquivoEn');
    }

    public function embed($width = '390', $height = '238', $retorna_id = FALSE){

        $url = $this->attributes['video'];

        if(!$url)
            return '';

        if (strpos($url, 'youtube.com') !== FALSE) {
            $fonte = 'youtube';
        } elseif(strpos($url, 'youtu.be') !== FALSE) {
            $fonte = 'youtu.be';
        }elseif(strpos($url, 'vimeo.com') !== FALSE){
            $fonte = 'vimeo';
        }else{
            $fonte = 'id_video';
        }

        if($fonte == 'youtube'){
            parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
            $video_id = $my_array_of_vars['v'];
            if($video_id){
                if($retorna_id)
                    return $video_id;
                $embed_str = "<iframe width='$width' height='$height' src='http://www.youtube.com/embed/".$video_id."' frameborder='0' allowfullscreen></iframe>";
                return $embed_str;
            }else{
                return "Erro ao incorporar o vídeo do Youtube";
            }
        }elseif($fonte == 'vimeo'){
            preg_match('@vimeo.com/([0-9]*)$@i', $url, $found);
            $video_id = $found[1];
            if($video_id){
                if($retorna_id)
                    return $video_id;
                $embed_str = "<iframe src='http://player.vimeo.com/video/".$video_id."' width='$width' height='$height' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>";
                return $embed_str;
            }else{
                return "Erro ao incorporar o vídeo do Vimeo";
            }
        }elseif($fonte == 'youtu.be'){
            preg_match('@youtu.be/(.*)$@i', $url, $found);
            $video_id = $found[1];
            if($video_id){
                if($retorna_id)
                    return $video_id;
                $embed_str = "<iframe width='$width' height='$height' src='http://www.youtube.com/embed/".$video_id."' frameborder='0' allowfullscreen></iframe>";
                return $embed_str;
            }else{
                return "Erro ao incorporar o vídeo do Youtube";
            }
        }elseif($fonte == 'id_video'){
            $embed_str = "<iframe width='$width' height='$height' src='http://www.youtube.com/embed/".$url."' frameborder='0' allowfullscreen></iframe>";
            return $embed_str;
        }else{
            return "Erro ao incorporar o Vídeo";
        }
    }
}
