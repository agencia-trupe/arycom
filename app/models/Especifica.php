<?php

class Especifica extends \EloquentBaseModel\Base
{
    protected $guarded = array();

    public static $rules = array(
        'titulo' => 'required',
        'descricao' => 'required',
        'atuacao_id' => 'required',
    );

    public function atuacao()
    {
        return $this->belongsTo('Atuacao');
    }

}
