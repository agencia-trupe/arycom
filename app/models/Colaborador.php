<?php

class Colaborador extends EloquentBaseModel\Base
{
    protected $guarded = array();

    public static $rules = array(
        'nome' => 'required',
        'descricao' => 'required',
    );

}
