<?php

class Feature extends \EloquentBaseModel\Base
{
    protected $guarded = array();

    public static $rules = array(
        'titulo' => 'required',
        'slug' => 'required',
    );

    public function produtos()
    {
        return $this->belongsToMany('Produto');
    }
}
