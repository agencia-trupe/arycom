<?php

class Midiatipo extends \EloquentBaseModel\Base
{
    protected $guarded = array();

    public static $rules = array(
        'titulo' => 'required',
        'descricao' => 'required',
        'user_id' => 'required'
    );

    public function midias()
    {
        return $this->hasMany('Midia');
    }
}
