<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	if ( Session::has('locale'))
        App::setLocale(Session::get('locale'));
    else{
    	// Se o acesso vier do BR - lang:pt
    	// Se o acesso vier de fora - lang:en
    	$ip = $_SERVER['REMOTE_ADDR'];

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://www.geoplugin.net/json.gp?ip={$ip}");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$details = curl_exec($ch);
		curl_close($ch);

		$details_xml = json_decode($details);

		if(!isset($details_xml->geoplugin_countryCode) || $details_xml->geoplugin_countryCode == 'BR'){
			Session::set('locale', 'pt-br');
			App::setLocale(Session::get('locale'));
		}else{
			Session::set('locale', 'en');
			App::setLocale(Session::get('locale'));
		}

    }
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest()) return Redirect::guest('login');
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

Route::filter('auth.admin', function()
{
    if ( ! Sentry::check())
    {
        return Redirect::route('painel.login');
    }
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});