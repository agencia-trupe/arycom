<?php namespace App\Services;

use File, Parser;

class Templates
{
	protected $templatesDir = '/views/site/paginas/templates';
	protected $defaultHeaders = array('nome' => 'Template Name');
	private   $templates;

	public function __construct()
	{
		$this->templates = File::files( app_path() . $this->templatesDir );
	}

	public function getTemplates()
	{
		$templatesNames = array();
		foreach ($this->templates as $template) {
			$templateName = Parser::getFileData($template, $this->defaultHeaders);
			$templatesNames[] = $templateName['nome'];
		}

		return $templatesNames;
	}
}