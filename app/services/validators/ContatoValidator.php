<?php namespace App\Services\Validators;

class ContatoValidator extends Validator
{
    public static $rules = array(
        'nome' => 'required',
        'email' => 'required|email',
        'telefone' => 'required',
        'mensagem' => 'required',
        'setor' => 'required'
    );
}
