<?php namespace App\Services\Validators;

class AtivacaoValidator extends Validator
{
    public static $rules = array(
		'nome' => 'required',
		'empresa' => 'required',
		'email' => 'required|email',
		'telefone' => 'required',
		'equipamentos' => 'required',
		'servicos' => 'required',
    );
}
