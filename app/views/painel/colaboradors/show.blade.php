@extends('painel._layouts.default')

@section('main')

<h1>Colaborador</h1>

<p>{{ link_to_route('painel.colaboradores.index', 'Listar colaboradores',
NULL, array('class' => 'btn btn-info btn-mini')) }}</p>

<table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Email</th>
                <th class="span5">Descricao</th>
                <th>Imagem</th>
                <th>Criado por</th>
                <th><i class="iconic-cog"></i></th>
            </tr>
        </thead>

    <tbody>
        <tr>
            <td>{{{ $colaborador->nome }}}</td>
            <td>{{{ $colaborador->email }}}</td>
            <td>{{ $colaborador->descricao }}</td>
            <td><img src="{{ URL::asset( $colaborador->imagem ) }}" alt=""></td>
            <td>{{{ Sentry::getUserProvider()->findById($colaborador->user_id)->first_name }}}</td>
            <td>{{ link_to_route('painel.colaboradores.edit', 'Editar', array($colaborador->id), array('class' => 'btn btn-info btn-mini')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.colaboradores.destroy', $colaborador->id))) }}
                    {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                {{ Form::close() }}
            </td>
        </tr>
    </tbody>
</table>

@stop
