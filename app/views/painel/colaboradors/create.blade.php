@extends('painel._layouts.default')

@section('main')

<h1>Novo Colaborador</h1>

{{ Form::open(array('route' => 'painel.colaboradores.store', 'files' =>  TRUE)) }}
    <div class="control-group {{ $errors->first('nome') ? 'error' : '' }}">
        {{ Form::label('nome', 'Nome:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('nome', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('nome') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('email') ? 'error' : '' }}">
        {{ Form::label('email', 'Título/Cargo:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('email', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('email') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('email_en') ? 'error' : '' }}">
        {{ Form::label('email_en', 'Título/Cargo (inglês):', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('email_en', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('email_en') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('descricao') ? 'error' : '' }}">
        {{ Form::label('descricao', 'Descriçao:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('descricao', NULL, array('class' => 'span6 ckeditor', 'rows' => 7)) }}
        </div>
        <h5>{{ $errors->first('descricao') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('descricao_en') ? 'error' : '' }}">
        {{ Form::label('descricao_en', 'Descriçao (inglês):', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('descricao_en', NULL, array('class' => 'span6 ckeditor', 'rows' => 7)) }}
        </div>
        <h5>{{ $errors->first('descricao_en') }}<h5>
    </div>
    <div class="control-group">
        {{ Form::label('imagem', 'Imagem') }}

        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;">
                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=Nenhuma+imagem">
            </div>
            <div>
                <span class="btn btn-file"><span class="fileupload-new">Selecionar imagem</span><span class="fileupload-exists">Alterar</span>{{ Form::file('imagem') }}</span>
            </div>
        </div>
    </div>
    {{ Form::submit('Salvar', array('class' => 'btn')) }}
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


