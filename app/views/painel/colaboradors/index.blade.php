@extends('painel._layouts.default')

@section('main')

<h1>Colaboradores</h1>

<p>{{ link_to_route('painel.colaboradores.create', 'Novo colaborador', NULL,
    array('class' => 'btn btn-mini btn-info')) }}
    <a href="#" data-model="colaborador" class="ordenar btn btn-mini btn-info">Ordenar ítens</a></p>

@if ($colaboradors->count())
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Título/Cargo</th>
                <th class="span5">Descricao</th>
                <th>Imagem</th>
                <th>Criado por</th>
                <th><i class="iconic-cog"></i></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($colaboradors as $colaborador)
                <tr id="colaborador_{{ $colaborador->id }}">
                    <td>{{{ $colaborador->nome }}}</td>
                    <td>{{{ $colaborador->email }}}</td>
                    <td>{{ substr( $colaborador->descricao, 0, 100 ) }} (...)</td>
                    <td><img width="100" src="{{ URL::asset( $colaborador->imagem ) }}" alt=""></td>
                    <td>{{{ Sentry::getUserProvider()->findById($colaborador->user_id)->first_name }}}</td>
                    <td>{{ link_to_route('painel.colaboradores.edit', 'Editar', array($colaborador->id), array('class' => 'btn btn-info btn-mini')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.colaboradores.destroy', $colaborador->id))) }}
                            {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {{ $colaboradors->links() }}
@else
    There are no colaboradors
@endif

@stop
