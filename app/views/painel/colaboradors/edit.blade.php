@extends('painel._layouts.default')

@section('main')

<h1>Editar Colaborador</h1>
{{ Form::model($colaborador, array('method' => 'PATCH', 'files' => TRUE, 'route' => array('painel.colaboradores.update', $colaborador->id))) }}
	<div class="control-group {{ $errors->first('nome') ? 'error' : '' }}">
        {{ Form::label('nome', 'Nome:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('nome', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('nome') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('email') ? 'error' : '' }}">
        {{ Form::label('email', 'Título/Cargo:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('email', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('email') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('email_en') ? 'error' : '' }}">
        {{ Form::label('email_en', 'Título/Cargo (inglês):', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('email_en', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('email_en') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('descricao') ? 'error' : '' }}">
        {{ Form::label('descricao', 'Descrição:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('descricao', NULL, array('class' => 'span6 ckeditor', 'rows' => 7)) }}
        </div>
        <h5>{{ $errors->first('descricao') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('descricao_en') ? 'error' : '' }}">
        {{ Form::label('descricao_en', 'Descrição (inglês):', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('descricao_en', NULL, array('class' => 'span6 ckeditor', 'rows' => 7)) }}
        </div>
        <h5>{{ $errors->first('descricao_en') }}<h5>
    </div>
    <div class="control-group">
        {{ Form::label('imagem', 'Imagem') }}
        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;">
                @if ($colaborador->imagem)
                    <a href="<?php echo $colaborador->imagem; ?>"><img src="<?php echo asset($colaborador->imagem); ?>" alt=""></a>
                @else
                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image">
                @endif
            </div>
            <div>
                <span class="btn btn-file"><span class="fileupload-new">Selecionar imagem</span><span class="fileupload-exists">Alterar</span>{{ Form::file('imagem') }}</span>
                <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remover</a>
            </div>
        </div>
    </div>
    {{ Form::submit('Salvar', array('class' => 'btn')) }}
	{{ link_to_route('painel.colaboradores.show', 'Cancel', $colaborador->id, array('class' => 'btn')) }}
	</ul>
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop