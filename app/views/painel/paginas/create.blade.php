@extends('painel._layouts.default')

@section('main')

<h1>Criar Página</h1>

{{ Form::open(array('route' => 'painel.paginas.store', 'files' => TRUE)) }}
    <div class="control-group {{ $errors->first('titulo') ? 'error' : '' }}">
        {{ Form::label('titulo', 'Titulo:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('titulo', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('titulo') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('titulo_en') ? 'error' : '' }}">
        {{ Form::label('titulo_en', 'Titulo (inglês):', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('titulo_en', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('titulo_en') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('slug') ? 'error' : '' }}">
        {{ Form::label('slug', 'Slug:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('slug', NULL, array('class' => 'span4', 'readonly')) }}
        </div>
        <h5>{{ $errors->first('slug') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('resumo') ? 'error' : '' }}">
        {{ Form::label('resumo', 'Resumo:',  array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('resumo', NULL, array('class' => 'span4 ckeditor', 'rows' => 5)) }}
        </div>
        <h5>{{ $errors->first('resumo') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('resumo_en') ? 'error' : '' }}">
        {{ Form::label('resumo_en', 'Resumo (inglês):',  array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('resumo_en', NULL, array('class' => 'span4 ckeditor', 'rows' => 5)) }}
        </div>
        <h5>{{ $errors->first('resumo_en') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('texto') ? 'error' : '' }}">
        {{ Form::label('texto', 'Texto:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('texto', NULL, array('class' => 'span6 ckeditor', 'rows' => 7)) }}
        </div>
        <h5>{{ $errors->first('texto') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('texto_en') ? 'error' : '' }}">
        {{ Form::label('texto_en', 'Texto (inglês):', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('texto_en', NULL, array('class' => 'span6 ckeditor', 'rows' => 7)) }}
        </div>
        <h5>{{ $errors->first('texto_en') }}<h5>
    </div>

    <div class="control-group">
        {{ Form::label('imagem', 'Imagem') }}

        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;">
                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=Nenhuma+imagem">
            </div>
            <div>
                <span class="btn btn-file"><span class="fileupload-new">Selecionar imagem</span><span class="fileupload-exists">Alterar</span>{{ Form::file('imagem') }}</span>
            </div>
        </div>
    </div>
		{{ Form::submit('Salvar', array('class' => 'btn')) }}
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


