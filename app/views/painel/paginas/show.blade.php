@extends('painel._layouts.default')

@section('main')

<h1>Página</h1>

<p>{{ link_to_route('painel.paginas.index', 'Listar páginas', null, array('class' => 'btn btn-info btn-mini')) }}</p>

<table class="table table-striped table-condensed">
    <thead>
        <tr>
            <th>Titulo</th>
                <th>Resumo</th>
                <th>Texto</th>
                <th>Imagem</th>
                <th>Criado por</th>
                <th class="span1"><i class="iconic-cog"></i></th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td>{{ $pagina->titulo }}</td>
                    <td>{{strip_tags($pagina->resumo) }}</td>
                    <td>{{strip_tags($pagina->texto) }}</td>
                    <td>
                        @if ( $pagina->imagem )
                            <img src="{{ URL::asset($pagina->imagem) }}" alt="{{ $pagina->titulo }}">
                        @endif
                    </td>
                    <td>{{ Sentry::getUserProvider()->findById($pagina->user_id)->first_name }}</td>
                    <td>{{ link_to_route('painel.paginas.edit', 'Editar', array($pagina->id), array('class' => 'btn btn-info btn-mini')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.paginas.destroy', $pagina->id))) }}
                            {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                        {{ Form::close() }}
                    </td>
        </tr>
    </tbody>
</table>

@stop
