@extends('painel._layouts.default')

@section('main')

<h1>Criar Local</h1>

{{ Form::open(array('route' => 'painel.locais.store', 'files' => true)) }}
    <div class="control-group {{ $errors->first('titulo') ? 'error' : '' }}">
        {{ Form::label('titulo', 'Titulo:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('titulo', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('titulo') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('titulo_en') ? 'error' : '' }}">
        {{ Form::label('titulo_en', 'Título (inglês):', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('titulo_en', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('titulo_en') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('endereco') ? 'error' : '' }}">
        {{ Form::label('endereco', 'Endereço:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('endereco', NULL, array('class' => 'span4 ckeditor')) }}
        </div>
        <h5>{{ $errors->first('endereco') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('numero') ? 'error' : '' }}">
        {{ Form::label('numero', 'Número:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('numero', NULL, array('class' => 'input-small')) }}
        </div>
        <h5>{{ $errors->first('numero') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('titulo') ? 'error' : '' }}">
        {{ Form::label('complemento', 'Complemento:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('complemento') }}
        </div>
        <h5>{{ $errors->first('complemento') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('bairro') ? 'error' : '' }}">
        {{ Form::label('bairro', 'Bairro:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('bairro') }}
        </div>
        <h5>{{ $errors->first('bairro') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('cidade') ? 'error' : '' }}">
        {{ Form::label('cidade', 'Cidade:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('cidade') }}
        </div>
        <h5>{{ $errors->first('cidade') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('cep') ? 'error' : '' }}">
        {{ Form::label('cep', 'CEP:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('cep') }}
        </div>
        <h5>{{ $errors->first('cep') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('uf') ? 'error' : '' }}">
        {{ Form::label('uf', 'UF:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('uf', null, array('class' => 'input-small ')) }}
        </div>
        <h5>{{ $errors->first('uf') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('link_mapa') ? 'error' : '' }}">
        {{ Form::label('link_mapa', 'Código para mapa:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('link_mapa', null, array('class' => 'span4', 'rows' => '3')) }}
        </div>
        <h5>{{ $errors->first('link_mapa') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('ddd1') ? 'error' : '' }}">
        {{ Form::label('ddd1', 'DDD 1:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('ddd1', null, array('class' => 'input-small')) }}
        </div>
        <h5>{{ $errors->first('ddd1') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('telefone1') ? 'error' : '' }}">
        {{ Form::label('telefone1', 'Telefone 1:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('telefone1') }}
        </div>
        <h5>{{ $errors->first('telefone1') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('ddd2') ? 'error' : '' }}">
        {{ Form::label('ddd2', 'DDD 2:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('ddd2', null, array('class' => 'input-small')) }}
        </div>
        <h5>{{ $errors->first('ddd2') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('telefone2') ? 'error' : '' }}">
        {{ Form::label('telefone2', 'Telefone 2:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('telefone2') }}
        </div>
        <h5>{{ $errors->first('telefone2') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('email') ? 'error' : '' }}">
        {{ Form::label('email', 'Email:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('email') }}
        </div>
        <h5>{{ $errors->first('email') }}<h5>
    </div>
    <div class="control-group">
        {{ Form::label('imagem', 'Imagem') }}

        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;">
                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=Nenhuma+imagem">
            </div>
            <div>
                <span class="btn btn-file"><span class="fileupload-new">Selecionar imagem</span><span class="fileupload-exists">Alterar</span>{{ Form::file('imagem') }}</span>
            </div>
        </div>
    </div>
    {{ Form::submit('Salvar', array('class' => 'btn')) }}
{{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@stop
