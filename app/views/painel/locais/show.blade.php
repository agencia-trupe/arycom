@extends('painel._layouts.default')

@section('main')

<h1>Show Local</h1>

<p>{{ link_to_route('painel.locais.index', 'Listar locais', null, array('class' => 'btn btn-info btn-mini')) }}</p>

<table class="table table-striped table-condensed">
	<thead>
		<tr>
			<th>Titulo</th>
			<th>Imagem</th>
			<th>Criado por</th>
			<th class="span1"><i class="iconic-cog"></i></th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $local->titulo }}}</td>
			<td><img src="{{ URL::asset($local->imagem) }}" alt="{{ $local->titulo }}"></td>
			<td>{{{ Sentry::getUserProvider()->findById($local->user_id)->first_name }}}</td>
            <td>{{ link_to_route('painel.locais.edit', 'Editar', array($local->id), array('class' => 'btn btn-info btn-mini')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.locais.destroy', $local->id))) }}
                    {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                {{ Form::close() }}
            </td>
		</tr>
	</tbody>
</table>

@stop