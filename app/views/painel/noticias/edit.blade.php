@extends('painel._layouts.default')

@section('main')

<h1>Editar Noticia</h1>
{{ Form::model($noticia, array('method' => 'PATCH', 'files' => TRUE, 'route' => array('painel.noticias.update', $noticia->id))) }}
    <div class="control-group {{ $errors->first('titulo') ? 'error' : '' }}">
        {{ Form::label('titulo', 'Titulo:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('titulo', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('titulo') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('titulo_en') ? 'error' : '' }}">
        {{ Form::label('titulo_en', 'Titulo (inglês):', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('titulo_en', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('titulo_en') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('slug') ? 'error' : '' }}">
        {{ Form::label('slug', 'Slug:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('slug', NULL, array('class' => 'span4', 'readonly')) }}
        </div>
        <h5>{{ $errors->first('slug') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('published_data') ? 'error' : '' }}">
        {{ Form::label('published_data', 'Data de publicação:', array('class' => 'control-label')) }}
        <div class="controls">
            <div class="input-append">
                {{ Form::text('published_data', date('d/m/Y', strtotime($noticia->published)), array('class' => 'datepicker input-small')) }}
                <div class="add-on"><i class="datesetter iconic-calendar-alt"></i></div>
            </div>
        </div>
        <h5>{{ $errors->first('published_data') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('published_time') ? 'error' : '' }}">
        {{ Form::label('published_time', 'Hora de publicação:', array('class' => 'control-label')) }}
        <div class="controls">
            <div class="input-append">
                {{ Form::text('published_time', date('H:i', strtotime($noticia->published)), array('class' => 'timepicker input-small')) }}
                <span class="add-on"><i class="timesetter iconic-clock"></i></span>
            </div>
        </div>
        <h5>{{ $errors->first('published_time') }}<h5>
    </div>
     <div class="control-group {{ $errors->first('resumo') ? 'error' : '' }}">
        {{ Form::label('resumo', 'Resumo:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('resumo', NULL, array('class' => 'span4 ckeditor')) }}
        </div>
        <h5>{{ $errors->first('resumo') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('resumo_en') ? 'error' : '' }}">
        {{ Form::label('resumo_en', 'Resumo (inglês):', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('resumo_en', NULL, array('class' => 'span4 ckeditor')) }}
        </div>
        <h5>{{ $errors->first('resumo_en') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('descricao') ? 'error' : '' }}">
        {{ Form::label('descricao', 'Descrição:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('descricao', NULL, array('class' => 'span4 ckeditor')) }}
        </div>
        <h5>{{ $errors->first('descricao') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('descricao_en') ? 'error' : '' }}">
        {{ Form::label('descricao_en', 'Descrição (inglês):', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('descricao_en', NULL, array('class' => 'span4 ckeditor')) }}
        </div>
        <h5>{{ $errors->first('descricao_en') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('status') ? 'error' : '' }}">
        {{ Form::label('status', 'Pública:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::checkbox('status') }}
        </div>
        <h5>{{ $errors->first('status') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('destaque') ? 'error' : '' }}">
        {{ Form::label('destaque', 'Destaque:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::checkbox('destaque') }}
        </div>
        <h5>{{ $errors->first('destaque') }}<h5>
    </div>
    <div class="fileupload fileupload-new" data-provides="fileupload">
        <div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;">
            @if ($noticia->imagem)
                <a href="<?php echo $noticia->imagem; ?>"><img src="<?php echo asset($noticia->imagem); ?>" alt=""></a>
            @else
                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image">
            @endif
        </div>
        <div>
            <span class="btn btn-file"><span class="fileupload-new">Selecionar imagem</span><span class="fileupload-exists">Alterar</span>{{ Form::file('imagem') }}</span>
            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remover</a>
        </div>
    </div>
    {{ Form::submit('Atualizar', array('class' => 'btn btn-info')) }}
    {{ link_to_route('painel.noticias.show', 'Cancelar', $noticia->id, array('class' => 'btn')) }}
{{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@stop
