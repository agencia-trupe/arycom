@extends('painel._layouts.default')

@section('main')

<h1>Notícias</h1>

<p>{{ link_to_route('painel.noticias.create', 'Nova notícia', NULL,
    array('class' => 'btn btn-mini btn-info')) }}</p>

@if ($noticias->count())
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Publicada em</th>
                <th>Titulo</th>
                <th>Slug</th>
                <th>Descricao</th>
                <th>Status</th>
                <th>Criada por</th>
                <th class="span1"><i class="iconic-cog"></i></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($noticias as $noticia)
                <tr>
                    <td>{{{ date('d/m/Y H:i', strtotime($noticia->published)) . 'h' }}}</td>
                    <td>{{{ $noticia->titulo }}}</td>
                    <td>{{{ $noticia->slug }}}</td>
                    <td>{{ Str::words($noticia->descricao, 20) }}</td>
                    <td>{{{ ($noticia->status) ? 'pública' : 'privada' }}}</td>
                    <td>{{{ Sentry::getUserProvider()->findById($noticia->user_id)->first_name }}}</td>
                    <td>{{ link_to_route('painel.noticias.edit', 'Editar', array($noticia->id), array('class' => 'btn btn-info btn-mini')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.noticias.destroy', $noticia->id))) }}
                            {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@else
    Nenhuma notícia cadastrada
@endif

@stop
