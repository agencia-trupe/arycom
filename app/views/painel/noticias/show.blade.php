@extends('painel._layouts.default')

@section('main')

<h1>Noticia</h1>

<p>{{ link_to_route('painel.noticias.index', 'Listar notícias', null, array('class' => 'btn btn-mini btn-info')) }}</p>

<table class="table table-striped table-condensed">
	<thead>
		<tr>
			<th>Titulo</th>
			<th>Slug</th>
			<th>Publicada em</th>
			<th>Descricao</th>
			<th>Status</th>
			<th>Criada por</th>
			<th class="span1"><i class="iconic-cog"></i></th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $noticia->titulo }}}</td>
			<td>{{{ $noticia->slug }}}</td>
			<td>{{{ date('d/m/Y H:i', strtotime($noticia->published)) . 'h' }}}</td>
			<td>{{ $noticia->descricao }}</td>
			<td>{{{ ($noticia->status) ? 'pública' : 'privada' }}}</td>
			<td>{{{ Sentry::getUserProvider()->findById($noticia->user_id)->first_name }}}</td>
            <td>{{ link_to_route('painel.noticias.edit', 'Editar', array($noticia->id), array('class' => 'btn btn-info btn-mini')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.noticias.destroy', $noticia->id))) }}
                    {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                {{ Form::close() }}
            </td>
		</tr>
	</tbody>
</table>

@stop