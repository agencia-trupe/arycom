@extends('painel._layouts.default')

@section('main')

<h1>Tipos de Mídia</h1>

<p>{{ link_to_route('painel.midiatipos.create', 'Novo tipo de mídia', NULL,
    array('class' => 'btn btn-mini btn-info')) }}
    <a href="#" data-model="midiatipo" class="ordenar btn btn-mini btn-info">Ordenar ítens</a>
    </p>

@if ($midiatipos->count())
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Titulo</th>
                <th>Descricao</th>
                <th>Criado por</th>
                <th class="span1"><i class="iconic-cog"></i></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($midiatipos as $midiatipo)
                <tr id="midiatipo_{{ $midiatipo->id }}">
                    <td>{{{ $midiatipo->titulo }}}</td>
                    <td>{{ $midiatipo->descricao }}</td>
                    <td>{{{ Sentry::getUserProvider()->findById($midiatipo->user_id)->first_name }}}</td>
                    <td>{{ link_to_route('painel.midiatipos.edit', 'Editar', array($midiatipo->id), array('class' => 'btn btn-info btn-mini')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.midiatipos.destroy', $midiatipo->id))) }}
                            {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $midiatipos->links() }}
@else
    Nenhum tipo de mídia cadastrado
@endif

@stop
