@extends('painel._layouts.default')

@section('main')

<h1>Tipo de Mídia</h1>

<p>{{ link_to_route('painel.midiatipos.index', 'Listar tipos de mídia', null, array('class' => 'btn btn-info btn-mini')) }}</p>

<table class="table table-striped table-condensed">
	<thead>
		<tr>
			<th>Titulo</th>
				<th>Descricao</th>
				<th>Criado por</th>
				<th class="span1"><i class="iconic-cog"></i></th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $midiatipo->titulo }}}</td>
			<td>{{ $midiatipo->descricao }}</td>
			<td>{{{ Sentry::getUserProvider()->findById($midiatipo->user_id)->first_name }}}</td>
            <td>{{ link_to_route('painel.midiatipos.edit', 'Editar', array($midiatipo->id), array('class' => 'btn btn-info btn-mini')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.midiatipos.destroy', $midiatipo->id))) }}
                    {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                {{ Form::close() }}
            </td>
		</tr>
	</tbody>
</table>

@stop