@extends('painel._layouts.default')

@section('main')

<h1>Áreas de Atuação</h1>

<p><a href="#" data-model="atuacao" class="ordenar btn btn-mini btn-info">Ordenar ítens</a></p>

@if ($atuacoes->count())
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Titulo</th>
                <th>Slug</th>
                <th>Resumo</th>
                <th>Descricao</th>
                <th>Imagem</th>
                <th>Criado por</th>
                <th class="span1"><i class="iconic-cog"></i></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($atuacoes as $atuacao)
                <tr id="atuacao_{{ $atuacao->id }}">
                    <td>{{{ $atuacao->titulo }}}</td>
                    <td>{{{ $atuacao->slug }}}</td>
                    <td>{{ $atuacao->resumo }}</td>
                    <td>{{ $atuacao->descricao }}</td>
                    <td><img src="{{ URL::asset($atuacao->imagem) }}" alt="{{ $atuacao->titulo }}"></td>
                    <td>{{{ Sentry::getUserProvider()->findById($atuacao->user_id)->first_name }}}</td>
                    <td>{{ link_to_route('painel.atuacoes.edit', 'Editar', array($atuacao->id), array('class' => 'btn btn-info btn-mini')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.atuacoes.destroy', $atuacao->id))) }}
                            {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

@else
    There are no atuacoes
@endif

@stop
