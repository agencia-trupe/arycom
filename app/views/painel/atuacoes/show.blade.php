@extends('painel._layouts.default')

@section('main')

<h1>Área de Atuação</h1>

<p>{{ link_to_route('painel.atuacoes.index', 'Listar áreas de atuação', null, array('class' => 'btn btn-info btn-mini')) }}</p>

<table class="table table-striped table-condensed">
    <thead>
        <tr>
            <th>Titulo</th>
            <th>Slug</th>
            <th>Resumo</th>
            <th>Descricao</th>
            <th>Imagem</th>
            <th>Criado por</th>
            <th class="span1"><i class="iconic-cog"></i></th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td>{{{ $atuacao->titulo }}}</td>
            <td>{{{ $atuacao->slug }}}</td>
            <td>{{ $atuacao->resumo }}</td>
            <td>{{ $atuacao->descricao }}</td>
            <td><img src="{{ URL::asset($atuacao->imagem) }}" alt="{{ $atuacao->titulo }}"></td>
            <td>{{{ Sentry::getUserProvider()->findById($atuacao->user_id)->first_name }}}</td>
             <td>{{ link_to_route('painel.atuacoes.edit', 'Editar', array($atuacao->id), array('class' => 'btn btn-info btn-mini')) }}</td>
             <td>
                 {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.atuacoes.destroy', $atuacao->id))) }}
                     {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                 {{ Form::close() }}
             </td>
        </tr>
    </tbody>
</table>

@stop
