@extends('painel._layouts.default')

@section('main')

<h1>Nova Área de Atuação</h1>

{{ Form::open(array('route' => 'painel.atuacoes.store', 'files' => TRUE)) }}
    <div class="control-group {{ $errors->first('titulo') ? 'error' : '' }}">
        {{ Form::label('titulo', 'Título:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('titulo', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('titulo') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('titulo_en') ? 'error' : '' }}">
        {{ Form::label('titulo_en', 'Título (inglês):', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('titulo_en', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('titulo_en') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('slug') ? 'error' : '' }}">
        {{ Form::label('slug', 'Slug:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('slug', NULL, array('class' => 'span4', 'readonly')) }}
        </div>
        <h5>{{ $errors->first('slug') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('resumo') ? 'error' : '' }}">
        {{ Form::label('resumo', 'Resumo:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('resumo', NULL, array('class' => 'span5 ckeditor', 'lines' => 4)) }}
        </div>
        <h5>{{ $errors->first('resumo') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('resumo_en') ? 'error' : '' }}">
        {{ Form::label('resumo_en', 'Resumo (inglês):', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('resumo_en', NULL, array('class' => 'span5 ckeditor', 'lines' => 4)) }}
        </div>
        <h5>{{ $errors->first('resumo_en') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('descricao') ? 'error' : '' }}">
        {{ Form::label('descricao', 'Descrição:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('descricao', NULL, array('class' => 'span6 ckeditor', 'lines' => 6)) }}
        </div>
        <h5>{{ $errors->first('descricao') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('descricao_en') ? 'error' : '' }}">
        {{ Form::label('descricao_en', 'Descrição (inglês):', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('descricao_en', NULL, array('class' => 'span6 ckeditor', 'lines' => 6)) }}
        </div>
        <h5>{{ $errors->first('descricao_en') }}<h5>
    </div>

    {{ Form::submit('Submit', array('class' => 'btn')) }}
{{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@stop
