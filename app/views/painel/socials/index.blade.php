@extends('painel._layouts.default')

@section('main')

<h1>Redes Sociais</h1>

<p>{{ link_to_route('painel.socials.create', 'Nova Rede Social', NULL,
    array('class' => 'btn btn-mini btn-info')) }}
    <a href="#" data-model="social" class="ordenar btn btn-mini btn-info">Ordenar ítens</a></p>

@if ($socials->count())
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Titulo</th>
                <th>Link</th>
                <th>Imagem</th>
                <th>Criado Por</th>
                <th class="span1"><i class="iconic-cog"></i></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($socials as $social)
                <tr id="social_{{ $social->id }}">
                    <td>{{{ $social->titulo }}}</td>
                    <td>{{{ $social->link }}}</td>
                    <td><img src="{{ URL::asset( $social->imagem ) }}" alt="{{ $social->titulo }}"></td>
                    <td>{{{ Sentry::getUserProvider()->findById($social->user_id)->first_name }}}</td>
                    <td>{{ link_to_route('painel.socials.edit', 'Editar', array($social->id), array('class' => 'btn btn-info btn-mini')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.socials.destroy', $social->id))) }}
                            {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@else
    <p>Nenhuma rede social cadastrada.</p>
@endif

@stop
