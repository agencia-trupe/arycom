@extends('painel._layouts.default')

@section('main')

<h1>Nova área especifica</h1>

{{ Form::open(array('route' => 'painel.especificas.store')) }}
    <div class="control-group {{ $errors->first('titulo') ? 'error' : '' }}">
        {{ Form::label('titulo', 'Titulo:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('titulo', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('titulo') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('titulo_en') ? 'error' : '' }}">
        {{ Form::label('titulo_en', 'Titulo (inglês):', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('titulo_en', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('titulo_en') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('descricao') ? 'error' : '' }}">
        {{ Form::label('descricao', 'Descrição:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('descricao', NULL, array('class' => 'span4 ckeditor')) }}
        </div>
        <h5>{{ $errors->first('descricao') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('descricao_en') ? 'error' : '' }}">
        {{ Form::label('descricao_en', 'Descrição (inglês):', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('descricao_en', NULL, array('class' => 'span4 ckeditor')) }}
        </div>
        <h5>{{ $errors->first('descricao_en') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('atuacao_id') ? 'error' : '' }}">
        {{ Form::label('atuacao_id', 'Área geral de atuação:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::select('atuacao_id', $atuacoes ) }}
        </div>
        <h5>{{ $errors->first('atuacao_id') }}<h5>
    </div>
    {{ Form::submit('Salvar', array('class' => 'btn btn-info')) }}
{{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@stop
