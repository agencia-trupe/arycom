@extends('painel._layouts.default')

@section('main')

<h1>Área específica</h1>

<p>{{ link_to_route('painel.especificas.index', 'Listar áreas específicas', null, array('class' => 'btn btn-info btn-mini')) }}</p>

<table class="table table-striped table-condensed">
    <thead>
        <tr>
            <th>Titulo</th>
            <th>Descricao</th>
            <th>Área de atuação</th>
            <th>Criado por</th>
            <th class="span1"><i class="iconic-cog"></i></th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td>{{{ $especifica->titulo }}}</td>
            <td>{{ $especifica->descricao }}</td>
            <td>{{{ $especifica->atuacao->titulo }}}</td>
            <td>{{{ Sentry::getUserProvider()->findById($especifica->user_id)->first_name }}}</td>
            <td>{{ link_to_route('painel.especificas.edit', 'Editar', array($especifica->id), array('class' => 'btn btn-info btn-mini')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.especificas.destroy', $especifica->id))) }}
                    {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                {{ Form::close() }}
            </td>
        </tr>
    </tbody>
</table>

@stop
