@extends('painel._layouts.default')

@section('main')

<h1>Áreas específicas</h1>

<p>{{ link_to_route('painel.especificas.create', 'Nova área específica', null, array('class' => 'btn btn-info btn-mini')) }}
    <a href="#" data-model="especifica" class="ordenar btn btn-mini btn-info">Ordenar ítens</a></p>

@if ($especificas->count())
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Titulo</th>
                <th>Descricao</th>
                <th>Área de atuação</th>
                <th>Criado por</th>
                <th class="span1"><i class="iconic-cog"></i></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($especificas as $especifica)
                <tr id="especifica_{{ $especifica->id }}">
                    <td>{{{ $especifica->titulo }}}</td>
                    <td>{{ substr( $especifica->descricao, 0, 100 ) }} (...)</td>
                    <td>{{{ $especifica->atuacao->titulo }}}</td>
                    <td>{{{ Sentry::getUserProvider()->findById($especifica->user_id)->first_name }}}</td>
                    <td>{{ link_to_route('painel.especificas.edit', 'Editar', array($especifica->id), array('class' => 'btn btn-info btn-mini')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.especificas.destroy', $especifica->id))) }}
                            {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    
@else
    Nenhuma área específica cadastrada
@endif

@stop
