@extends('painel._layouts.default')

@section('main')

<h1>Aplicações</h1>

<p>{{ link_to_route('painel.aplicacoes.create', 'Nova Aplicação', NULL,
    array('class' => 'btn btn-mini btn-info')) }}
    <a href="#" data-model="feature" class="ordenar btn btn-mini btn-info">Ordenar ítens</a></p>

@if ($features->count())
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Titulo</th>
                <th>Slug</th>
                <th>Imagem</th>
                <th>Criado Por</th>
                <th class="span1"><i class="iconic-cog"></i></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($features as $feature)
                <tr id="feature_{{ $feature->id }}">
                    <td>{{{ $feature->titulo }}}</td>
                    <td>{{{ $feature->slug }}}</td>
                    <td><img width="100" src="{{ URL::asset( $feature->imagem ) }}" alt="{{ $feature->titulo }}"></td>
                    <td>{{{ Sentry::getUserProvider()->findById($feature->user_id)->first_name }}}</td>
                    <td>{{ link_to_route('painel.aplicacoes.edit', 'Editar', array($feature->id), array('class' => 'btn btn-info btn-mini')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.aplicacoes.destroy', $feature->id))) }}
                            {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $features->links() }}
@else
    <p>Nenhuma aplicação cadastrada.</p>
@endif

@stop
