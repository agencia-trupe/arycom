@extends('painel._layouts.default')

@section('main')

<h1>Editar Aplicação</h1>
{{ Form::model($feature, array('method' => 'PATCH', 'files' => TRUE, 'route' => array('painel.aplicacoes.update', $feature->id))) }}
	<div class="control-group {{ $errors->first('titulo') ? 'error' : '' }}">
        {{ Form::label('titulo', 'Titulo:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('titulo', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('titulo') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('titulo_en') ? 'error' : '' }}">
        {{ Form::label('titulo_en', 'Titulo (inglês):', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('titulo_en', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('titulo_en') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('slug') ? 'error' : '' }}">
        {{ Form::label('slug', 'Slug:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('slug', NULL, array('class' => 'span4', 'readonly')) }}
        </div>
        <h5>{{ $errors->first('slug') }}<h5>
    </div>
    <div class="fileupload fileupload-new" data-provides="fileupload">
        <div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;">
            @if ($feature->imagem)
                <a href="<?php echo $feature->imagem; ?>"><img src="<?php echo asset($feature->imagem); ?>" alt=""></a>
            @else
                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image">
            @endif
        </div>
        <div>
            <span class="btn btn-file"><span class="fileupload-new">Selecionar imagem</span><span class="fileupload-exists">Alterar</span>{{ Form::file('imagem') }}</span>
            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remover</a>
        </div>
    </div>
    <div class="fileupload fileupload-new" data-provides="fileupload">
        <div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;">
            @if ($feature->imagem_en)
                <a href="<?php echo $feature->imagem_en; ?>"><img src="<?php echo asset($feature->imagem_en); ?>" alt=""></a>
            @else
                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image">
            @endif
        </div>
        <div>
            <span class="btn btn-file"><span class="fileupload-new">Selecionar imagem</span><span class="fileupload-exists">Alterar</span>{{ Form::file('imagem_en') }}</span>
            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remover</a>
        </div>
    </div>
	{{ Form::submit('Atualizar', array('class' => 'btn btn-info')) }}
	{{ link_to_route('painel.aplicacoes.show', 'Cancelar', $feature->id, array('class' => 'btn')) }}
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop