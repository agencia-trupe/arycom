@extends('painel._layouts.default')

@section('main')

<h1>Categoria</h1>

<p>{{ link_to_route('painel.categorias.index', 'Listar Categorias', null, array('class' => 'btn btn-info btn-small')) }}</p>

<table class="table table-striped table-condensed">
    <thead>
        <tr>
            <th>Titulo</th>
                <th>Slug</th>
                <th>Descricao</th>
                <th>Criado por:</th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td>{{{ $categoria->titulo }}}</td>
                    <td>{{{ $categoria->slug }}}</td>
                    <td>{{ $categoria->descricao }}</td>
                    <td>{{{ Sentry::getUserProvider()->findById($categoria->user_id)->first_name }}}</td>
                    <td>{{ link_to_route('painel.categorias.edit', 'Editar', array($categoria->id), array('class' => 'btn btn-info btn-mini')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.categorias.destroy', $categoria->id))) }}
                            {{ Form::submit('Apagar', array('class' => 'btn btn-mini btn-danger')) }}
                        {{ Form::close() }}
                    </td>
        </tr>
    </tbody>
</table>

@stop
