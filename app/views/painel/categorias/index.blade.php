@extends('painel._layouts.default')

@section('main')

<h1>Categorias</h1>

<p>{{ link_to_route('painel.categorias.create', 'Nova Categoria', NULL,
    array('class' => 'btn btn-mini btn-info')) }}
    <a href="#" data-model="categoria" class="ordenar btn btn-mini btn-info">Ordenar ítens</a>
</p>

@if ($categorias->count())
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Titulo</th>
                <th>Slug</th>
                <th>Descricao</th>
                <th>Criado por</th>
                <th class="span1"><i class="iconic-cog"></i></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($categorias as $categoria)
                <tr id="categoria_{{ $categoria->id }}">
                    <td>{{{ $categoria->titulo }}}</td>
                    <td>{{{ $categoria->slug }}}</td>
                    <td>{{ $categoria->descricao }}</td>
                    <td>{{{ Sentry::getUserProvider()->findById($categoria->user_id)->first_name }}}</td>
                    <td>{{ link_to_route('painel.categorias.edit', 'Editar', array($categoria->id), array('class' => 'btn btn-info btn-mini')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.categorias.destroy', $categoria->id))) }}
                            {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    
@else
    There are no categoria
@endif

@stop
