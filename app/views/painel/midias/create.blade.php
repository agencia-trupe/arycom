@extends('painel._layouts.default')

@section('main')

<h1>Nova mídia</h1>

{{ Form::open(array('files' => TRUE, 'id' => 'midia-form', 'route' => 'painel.midias.store')) }}
     <div class="control-group {{ $errors->first('midiatipo_id') ? 'error' : '' }}">
        {{ Form::label('midiatipo_id', 'Tipo de Mídia:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::select('midiatipo_id', $midiatipos, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('midiatipo_id') }}<h5>
    </div>
	<div class="control-group {{ $errors->first('titulo') ? 'error' : '' }}">
        {{ Form::label('titulo', 'Titulo:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('titulo', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('titulo') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('titulo_en') ? 'error' : '' }}">
        {{ Form::label('titulo_en', 'Titulo (inglês):', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('titulo_en', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('titulo_en') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('slug') ? 'error' : '' }}">
        {{ Form::label('slug', 'Slug:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('slug', NULL, array('class' => 'span4', 'readonly')) }}
        </div>
        <h5>{{ $errors->first('slug') }}<h5>
    </div>
    <div style="display:none" id="video-controls" class="control-group {{ $errors->first('video') ? 'error' : '' }}">
        {{ Form::label('video', 'Link para vídeo:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('video', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('video') }}<h5>
    </div>
    <div class="control-group" id="imagem-controls">
        {{ Form::label('imagem', 'Imagem') }}

        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;">
                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=Nenhuma+imagem">
            </div>
            <div>
                <span class="btn btn-file"><span class="fileupload-new">Selecionar imagem</span><span class="fileupload-exists">Alterar</span>{{ Form::file('imagem') }}</span>
            </div>
        </div>
    </div>
    <div class="control-group {{ $errors->first('descricao') ? 'error' : '' }}">
        {{ Form::label('descricao', 'Descrição:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('descricao', NULL, array('class' => 'span4 ckeditor')) }}
        </div>
        <h5>{{ $errors->first('descricao') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('descricao_en') ? 'error' : '' }}">
        {{ Form::label('descricao_en', 'Descrição (inglês):', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('descricao_en', NULL, array('class' => 'span4 ckeditor')) }}
        </div>
        <h5>{{ $errors->first('descricao_en') }}<h5>
    </div>
    {{ Form::submit('Salvar', array('class' => 'btn')) }}
{{ Form::close() }}

@if ($errors->any())
	<ul>
		{{ implode('', $errors->all('<li class="error">:message</li>')) }}
	</ul>
@endif

@stop


