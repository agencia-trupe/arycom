@extends('painel._layouts.default')

@section('main')

<h1>Editar Midia</h1>
{{ Form::model($midia, array('files' => TRUE, 'method' => 'PATCH', 'route' => array('painel.midias.update', $midia->id))) }}
     <div class="control-group {{ $errors->first('midia_tipo_string') ? 'error' : '' }}">
        {{ Form::label('midia_tipo_string', 'Tipo de Mídia:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('midia_tipo_string', $midia->midiatipo->titulo, array('class' => 'span4', 'disabled')) }}
        </div>
        <h5>{{ $errors->first('midia_tipo_string') }}<h5>
    </div>
    {{ Form::hidden('midiatipo_id', $midia->midiatipo_id) }}
    <div class="control-group {{ $errors->first('titulo') ? 'error' : '' }}">
        {{ Form::label('titulo', 'Titulo:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('titulo', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('titulo') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('titulo_en') ? 'error' : '' }}">
        {{ Form::label('titulo_en', 'Titulo (inglês):', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('titulo_en', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('titulo_en') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('slug') ? 'error' : '' }}">
        {{ Form::label('slug', 'Slug:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('slug', NULL, array('class' => 'span4', 'readonly')) }}
        </div>
        <h5>{{ $errors->first('slug') }}<h5>
    </div>
    @if ($midia->midiatipo_id === '2')
    <div id="video-controls" class="control-group {{ $errors->first('video') ? 'error' : '' }}">
        {{ Form::label('video', 'Link para vídeo:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('video', 'http://www.youtube.com/watch?v=' . $midia->path, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('video') }}<h5>
    </div>
    @else
    <div class="control-group" id="imagem-controls">
        {{ Form::label('imagem', 'Imagem') }}

        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;">
                <img src="{{ URL::asset($midia->path) }}">
            </div>
            <div>
                <span class="btn btn-file"><span class="fileupload-new">Selecionar imagem</span><span class="fileupload-exists">Alterar</span>{{ Form::file('imagem') }}</span>
            </div>
        </div>
    </div>
    @endif
    <div class="control-group {{ $errors->first('descricao') ? 'error' : '' }}">
        {{ Form::label('descricao', 'Descrição:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('descricao', NULL, array('class' => 'span4 ckeditor')) }}
        </div>
        <h5>{{ $errors->first('descricao') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('descricao_en') ? 'error' : '' }}">
        {{ Form::label('descricao_en', 'Descrição (inglês):', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('descricao_en', NULL, array('class' => 'span4 ckeditor')) }}
        </div>
        <h5>{{ $errors->first('descricao_en') }}<h5>
    </div>
    {{ Form::submit('Atualizar', array('class' => 'btn btn-info')) }}
    {{ link_to_route('painel.midias.show', 'Cancelar', $midia->id, array('class' => 'btn')) }}
{{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@stop
