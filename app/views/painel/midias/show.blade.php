@extends('painel._layouts.default')

@section('main')

<h1>Mídia</h1>

<p>{{ link_to_route('painel.midias.index', 'Listar mídias', null, array('class' => 'btn btn-info btn-mini')) }}</p>

<table class="table table-striped table-condensed">
	<thead>
		<tr>
			<th>Titulo</th>
			<th>Slug</th>
			<th>Descricao</th>
			<th>Caminho</th>
			<th>Tipo de mídia</th>
			<th>Criado por</th>
			<th class="span1"><i class="iconic-cog"></i></th>
		</tr>
	</thead>

	<tbody>
		<tr>
			<td>{{{ $midia->titulo }}}</td>
			<td>{{{ $midia->slug }}}</td>
			<td>{{ $midia->descricao }}</td>
			<td>{{{ $midia->path }}}</td>
			<td>{{{ $midia->midiatipo->titulo }}}</td>
			<td>{{{ Sentry::getUserProvider()->findById($midia->user_id)->first_name }}}</td>
            <td>{{ link_to_route('painel.midias.edit', 'Editar', array($midia->id), array('class' => 'btn btn-info btn-mini')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.midias.destroy', $midia->id))) }}
                    {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                {{ Form::close() }}
            </td>
		</tr>
	</tbody>
</table>

@stop