@extends('painel._layouts.default')

@section('main')
<div class="row-fluid">
    <div class="span7">
        <h1>Editar Produto</h1>
        {{ Form::model($produto, array('method' => 'PATCH', 'files' => TRUE, 'route' => array('painel.produtos.update', $produto->id))) }}
            <div class="control-group {{ $errors->first('titulo') ? 'error' : '' }}">
                {{ Form::label('titulo', 'Titulo:', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::text('titulo', NULL, array('class' => 'span4')) }}
                </div>
                <h5>{{ $errors->first('titulo') }}<h5>
            </div>
            <div class="control-group {{ $errors->first('titulo_en') ? 'error' : '' }}">
                {{ Form::label('titulo_en', 'Titulo (inglês):', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::text('titulo_en', NULL, array('class' => 'span4')) }}
                </div>
                <h5>{{ $errors->first('titulo_en') }}<h5>
            </div>
            <div class="control-group {{ $errors->first('slug') ? 'error' : '' }}">
                {{ Form::label('slug', 'Slug:', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::text('slug', NULL, array('class' => 'span4', 'readonly')) }}
                </div>
                <h5>{{ $errors->first('slug') }}<h5>
            </div>
            <div class="control-group {{ $errors->first('categoria_id') ? 'error' : '' }}">
                {{ Form::label('categoria_id', 'Categoria:', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::select('categoria_id', $categorias ) }}
                </div>
                <h5>{{ $errors->first('slug') }}<h5>
            </div>
            <div class="control-group {{ $errors->first('atuacao[]') ? 'error' : '' }}">
                {{ Form::label('atuacao[]', 'Área geral de atuação (para selecionar mais de uma, segure o botão ctrl ou cmd enquanto clica):', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::select('atuacao[]', $atuacoes, $produto->atuacoes->lists('id'), array('id' => 'atuacao', 'multiple', 'style' => 'height:147px') ) }}
                </div>
                <h5>{{ $errors->first('atuacao[]') }}<h5>
            </div>

            <div class="control-group {{ $errors->first('feature[]') ? 'error' : '' }}">
                {{ Form::label('feature[]', 'Aplicações (para selecionar mais de uma, segure o botão ctrl ou cmd enquanto clica):', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::select('feature[]', $aplicacoes, $produto->features->lists('id'), array('multiple', 'style' => 'height:147px') ) }}
                </div>
                <h5>{{ $errors->first('feature[]') }}<h5>
            </div>
            <div class="control-group {{ $errors->first('descricao') ? 'error' : '' }}">
                {{ Form::label('descricao', 'Descrição:', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::textarea('descricao', NULL, array('class' => 'span6 ckeditor', 'lines' => 4)) }}
                </div>
                <h5>{{ $errors->first('descricao') }}<h5>
            </div>
            <div class="control-group {{ $errors->first('descricao_en') ? 'error' : '' }}">
                {{ Form::label('descricao_en', 'Descrição (inglês):', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::textarea('descricao_en', NULL, array('class' => 'span6 ckeditor', 'lines' => 4)) }}
                </div>
                <h5>{{ $errors->first('descricao_en') }}<h5>
            </div>
            <div class="control-group {{ $errors->first('video') ? 'error' : '' }}">
                {{ Form::label('video', 'Vídeo (link do youtube):', array('class' => 'control-label')) }}
                <div class="controls">
                    {{ Form::text('video', NULL, array('class' => 'span4', 'lines' => 3)) }}
                </div>
                <h5>{{ $errors->first('video') }}<h5>
            </div>
            @if($produto->video)
                Prévia:<br>
                {{ $produto->embed() . "<br><br>" }}
            @endif
            <div class="fileupload fileupload-new" data-provides="fileupload">
                <div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;">
                    @if ($produto->imagem)
                        <a href="{{ URL::asset($produto->imagem) }}"><img src="{{ URL::asset($produto->imagem) }}" alt=""></a>
                    @else
                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image">
                    @endif
                </div>
                <div>
                    <span class="btn btn-file"><span class="fileupload-new">Selecionar imagem</span><span class="fileupload-exists">Alterar</span>{{ Form::file('imagem') }}</span>
                    <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remover</a>
                </div>
            </div>
            {{ Form::submit('Atualizar', array('class' => 'btn btn-info')) }}
            {{ link_to_route('painel.produtos.show', 'Cancelar', $produto->id, array('class' => 'btn')) }}

        {{ Form::close() }}

        @if ($errors->any())
            <ul>
                {{ implode('', $errors->all('<li class="error">:message</li>')) }}
            </ul>
        @endif
    </div>
    <div class="span5 well">

        <h2>Adicionar arquivos (Português)</h2>
        {{ Form::open(array('id' => 'produtos-file-upload', 'files' =>  true)) }}
            {{ Form::hidden('produto_id', $produto->id, array('id' => 'produto_id'))}}
            {{ Form::label('produtos-file-titulo', 'Título do arquivo') }}
            {{ Form::text('produtos-file-titulo', null, array('class' =>  'input-large')) }}
            {{ Form::label('path', 'Arquivo') }}
            {{ Form::file('path', array('id' =>  'path')) }}
            <br><br>
            {{ Form::submit('Adicionar arquivo', array('class' => 'btn btn-info btn-mini')) }}
        {{ Form::close() }}
        <div style="display:none" id="response" class="alert alert-warning"></div>
        <hr>
        <legend>Arquivos</legend>
        <div class="lista-produtos">
            @if( count($produto->arquivos) )
                @foreach ($produto->arquivos as $arquivo)
                    <div class="produto-arquivo" data-arquivo-id="{{ $arquivo->id }}">
                        <span>{{ $arquivo->titulo }}</span>
                        <a href="#" class="btn btn-danger btn-mini deleta-arquivo pull-right" data-id="{{ $arquivo->id }}">
                            <i class="iconic-trash"></i>
                        </a>
                    </div>
                @endforeach
            @else
                <div class="emptyMessage">Não tem arquivos</div>
            @endif
        </div>

        <hr>

        <h2>Adicionar arquivos (Inglês)</h2>
        {{ Form::open(array('id' => 'produtos-file-upload-en', 'files' =>  true)) }}
            {{ Form::hidden('produto_id', $produto->id, array('id' => 'produto_id'))}}
            {{ Form::label('produtos-file-titulo-en', 'Título do arquivo') }}
            {{ Form::text('produtos-file-titulo-en', null, array('class' =>  'input-large')) }}
            {{ Form::label('path', 'Arquivo') }}
            {{ Form::file('path-en', array('id' =>  'path-en')) }}
            <br><br>
            {{ Form::submit('Adicionar arquivo', array('class' => 'btn btn-info btn-mini')) }}
        {{ Form::close() }}
        <div style="display:none" id="response-en" class="alert alert-warning"></div>
        <hr>
        <legend>Arquivos</legend>
        <div class="lista-produtos-en">
            @if( count($produto->arquivosEn) )
                @foreach ($produto->arquivosEn as $arquivo)
                    <div class="produto-arquivo" data-arquivo-id="{{ $arquivo->id }}">
                        <span>{{ $arquivo->titulo }}</span>
                        <a href="#" class="btn btn-danger btn-mini deleta-arquivo-en pull-right" data-id="{{ $arquivo->id }}">
                            <i class="iconic-trash"></i>
                        </a>
                    </div>
                @endforeach
            @else
                <div class="emptyMessage">Não tem arquivos</div>
            @endif
        </div>

    </div>

@stop
