@extends('painel._layouts.default')

@section('main')

<h1>Produto</h1>

<p>{{ link_to_route('painel.produtos.index', 'Listar produtos', null, array('class' => 'btn btn-info btn-mini')) }}</p>

<table class="table table-striped table-condensed">
    <thead>
        <tr>
            <th>Titulo</th>
            <th>Slug</th>
            <th>Descricao</th>
            <th>Imagem</th>
            <th>Categoria</th>
            <th>Criado por</th>
            <th class="span1"><i class="iconic-cog"></i></th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td>{{{ $produto->titulo }}}</td>
            <td>{{{ $produto->slug }}}</td>
            <td>{{ substr($produto->descricao, 0, 100) }} (...)</td>
            <td><img  width="100" src="{{ URL::asset($produto->imagem) }}" alt="{{ $produto->titulo }}"></td>
            <td>{{{ $produto->categoria->titulo }}}</td>
            <td>{{{ Sentry::getUserProvider()->findById($produto->user_id)->first_name }}}</td>
            <td>{{ link_to_route('painel.produtos.edit', 'Editar', array($produto->id), array('class' => 'btn btn-info btn-mini')) }}</td>
            <td>
                {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.produtos.destroy', $produto->id))) }}
                    {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                {{ Form::close() }}
            </td>
        </tr>
    </tbody>
</table>

@stop
