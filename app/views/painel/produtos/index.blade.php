@extends('painel._layouts.default')

@section('main')

<h1>Produtos</h1>

<p>{{ link_to_route('painel.produtos.create', 'Novo produto', NULL,
    array('class' => 'btn btn-mini btn-info')) }}
    
    @if($filtroCat)
        <a href="#" data-model="produto" class="ordenar btn btn-mini btn-info">Ordenar ítens</a>
    @endif
</p>

@if(sizeof($categorias) > 0)
    <div class="btn-group">
        @foreach($categorias as $c)
            <a href="produtos?cat={{$c->id}}" title="Produtos de {{$c->titulo}}" class="btn btn-mini @if($c->id == $filtroCat) btn-warning @else btn-info @endif">{{$c->titulo}}</a>
        @endforeach
    </div>
@endif

@if ($produtos->count())
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Titulo</th>
                <th>Slug</th>
                <th>Descricao</th>
                <th>Imagem</th>
                <th>Categoria</th>
                <th>Criado por</th>
                <th class="span1"><i class="iconic-cog"></i></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($produtos as $produto)
                <tr id="produto_{{ $produto->id }}">
                    <td>{{{ $produto->titulo }}}</td>
                    <td>{{{ $produto->slug }}}</td>
                    <td>{{ substr( strip_tags($produto->descricao), 0, 100 ) }} (...)</td>
                    <td><img width="100" src="{{ URL::asset($produto->imagem) }}" alt="{{ $produto->titulo }}"></td>
                    <td>
                        @if($produto->categoria)
                            {{{ $produto->categoria->titulo }}}
                        @endif
                    </td>
                    <td>{{{ Sentry::getUserProvider()->findById($produto->user_id)->first_name }}}</td>
                    <td>{{ link_to_route('painel.produtos.edit', 'Editar', array($produto->id), array('class' => 'btn btn-info btn-mini')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.produtos.destroy', $produto->id))) }}
                            {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    
    @if(!$filtroCat)
        {{ $produtos->links() }}
    @endif

@else
    Nenhum produto cadastrado
@endif

@stop
