@extends('painel._layouts.default')

@section('main')

<h1>Novo Produto</h1>

{{ Form::open(array('route' => 'painel.produtos.store', 'files' => TRUE)) }}
    <div class="control-group {{ $errors->first('titulo') ? 'error' : '' }}">
        {{ Form::label('titulo', 'Titulo:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('titulo', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('titulo') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('titulo_en') ? 'error' : '' }}">
        {{ Form::label('titulo_en', 'Titulo (inglês):', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('titulo_en', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('titulo_en') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('slug') ? 'error' : '' }}">
        {{ Form::label('slug', 'Slug:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('slug', NULL, array('class' => 'span4', 'readonly')) }}
        </div>
        <h5>{{ $errors->first('slug') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('categoria_id') ? 'error' : '' }}">
        {{ Form::label('categoria_id', 'Categoria:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::select('categoria_id', $categorias ) }}
        </div>
        <h5>{{ $errors->first('categoria_id') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('atuacao[]') ? 'error' : '' }}">
        {{ Form::label('atuacao[]', 'Área geral de atuação (para selecionar mais de uma, segure o botão ctrl ou cmd enquanto clica):', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::select('atuacao[]', $atuacoes, '', array('id' => 'atuacao', 'multiple', 'style' => 'height:147px') ) }}
        </div>
        <h5>{{ $errors->first('atuacao[]') }}<h5>
    </div>

    <div class="control-group {{ $errors->first('feature[]') ? 'error' : '' }}">
        {{ Form::label('feature[]', 'Aplicações (para selecionar mais de uma, segure o botão ctrl ou cmd enquanto clica):', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::select('feature[]', $aplicacoes, '', array('multiple', 'style' => 'height:147px') ) }}
        </div>
        <h5>{{ $errors->first('feature[]') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('descricao') ? 'error' : '' }}">
        {{ Form::label('descricao', 'Descrição:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('descricao', NULL, array('class' => 'span6 ckeditor', 'lines' => 4)) }}
        </div>
        <h5>{{ $errors->first('descricao') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('descricao_en') ? 'error' : '' }}">
        {{ Form::label('descricao_en', 'Descrição (inglês):', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::textarea('descricao_en', NULL, array('class' => 'span6 ckeditor', 'lines' => 4)) }}
        </div>
        <h5>{{ $errors->first('descricao_en') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('video') ? 'error' : '' }}">
        {{ Form::label('video', 'Vídeo (link do youtube):', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('video', NULL, array('class' => 'span4', 'lines' => 3)) }}
        </div>
        <h5>{{ $errors->first('video') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('arquivo') ? 'error' : '' }}">
        {{ Form::label('arquivo', 'Arquivos:', array('class' => 'control-label')) }}
        <div class="controls">
            <div class="alert alert-info">
                Para upload de arquivos, salve o produto e utilize a opção editar.
            </div>
        </div>
        <h5>{{ $errors->first('arquivo') }}<h5>
    </div>
    <div class="control-group">
        {{ Form::label('imagem', 'Imagem') }}

        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;">
                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=Nenhuma+imagem">
            </div>
            <div>
                <span class="btn btn-file"><span class="fileupload-new">Selecionar imagem</span><span class="fileupload-exists">Alterar</span>{{ Form::file('imagem') }}</span>
            </div>
        </div>
    </div>

    {{ Form::submit('Salvar', array('class' => 'btn')) }}

{{ Form::close() }}
@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@stop
