@extends('painel._layouts.default')

@section('main')

<h1>Novo Pop Up - @if($idioma == 'pt-br') Português @elseif($idioma == 'en') Inglês @endif </h1>

{{ Form::open(array('route' => 'painel.popup.store')) }}

    <div class="control-group {{ $errors->first('titulo') ? 'error' : '' }}">
        {{ Form::label('titulo', 'Titulo:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('titulo', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('titulo') }}<h5>
    </div>

    <div class="control-group {{ $errors->first('data_inicio') ? 'error' : '' }} {{ $errors->first('data_termino') ? 'error' : '' }}">
        <label for="input-exibicao" class='control-label'>
            Exibição
        </label>
        {{ Form::text('data_inicio', NULL, array('class' => 'form-control datepicker', 'id' => 'input-exibicao', 'required' => 'required')) }}
        até
        {{ Form::text('data_termino', NULL, array('class' => 'form-control datepicker', 'id' => 'input-exibicao-termino', 'required' => 'required')) }}
        <h5>{{ $errors->first('data_inicio') }}<h5>
        <h5>{{ $errors->first('data_termino') }}<h5>
    </div>

    <div class="control-group">
        <label for="input-tipo" class='control-label'>
            Tipo
        </label>
        {{ Form::select('tipo', array('' => '','imagem' => 'Imagem', 'video' => 'Vídeo'), null, array('id' => 'input-tipo', 'required' => 'required', 'class' => 'form-control')) }}
    </div>

    <div id="popupInfoLink" style="display:none;">

        <div class="control-group {{ $errors->first('file') ? 'error' : '' }}">
            {{ Form::label('input-file', 'Imagem:', array('class' => 'control-label')) }}
            <div class="controls">
                {{ Form::file('file', array('class' => 'span4', "id" => "input-file")) }}
            </div>
            <h5>{{ $errors->first('file') }}<h5>
        </div>

        <div class="control-group {{ $errors->first('link') ? 'error' : '' }}">
            {{ Form::label('input-link', 'Link:', array('class' => 'control-label')) }}
            <div class="controls">
                {{ Form::text('link', NULL, array('class' => 'span4', "id" => "input-link")) }}
            </div>
            <h5>{{ $errors->first('link') }}<h5>
        </div>

        <div class="control-group {{ $errors->first('destino_link') ? 'error' : '' }}">
            {{ Form::label('input-destino_link', 'Destino do Link:', array('class' => 'control-label')) }}
            <div class="controls">
                {{ Form::select('destino_link', array('' => '','_self' => 'Mesma janela', '_blank' => 'Nova janela'), null, array('id' => 'input-destino_link', 'class' => 'form-control')) }}
            </div>
            <h5>{{ $errors->first('destino_link') }}<h5>
        </div>

    </div>

    <div id="popupInfoVideo" style="display:none;">
        <div class="control-group {{ $errors->first('video_embed') ? 'error' : '' }}">
            {{ Form::label('input-video', 'URL do Vídeo (youtube):', array('class' => 'control-label')) }}
            <div class="controls">
                {{ Form::text('video_embed', NULL, array('class' => 'span4', "id" => "input-video")) }}
            </div>
            <h5>{{ $errors->first('video_embed') }}<h5>
        </div>
    </div>

    <hr>

    <div class="control-group  {{ $errors->first('publicar') ? 'error' : '' }}">
        <label style="width:20%; float:left;">
            <input type="radio" name="publicar" value="1" required>
            Publicar
        </label>

        <label style="width:20%; float:left;">
            <input type="radio" name="publicar" value="0">
            Rascunho
        </label>

        <div style="clear:left"></div>
        <h5>{{ $errors->first('publicar') }}<h5>
    </div>
    <hr>

    <input type="hidden" name="idioma" value="{{ $idioma }}">

    {{ Form::submit('Salvar', array('class' => 'btn')) }}
    <a href="{{ URL::route('painel.popup.index') }}" class="btn" title="Voltar">Voltar</a>
    {{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@stop
