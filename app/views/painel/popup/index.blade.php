@extends('painel._layouts.default')

@section('main')

<h1>Pop Ups</h1>

@if (\Popup::pt()->count() == 0)
    <p><a href="{{ URL::route('painel.popup.create', array('idioma' => 'pt-br')) }}" class="btn btn-mini btn-info">Novo Pop Up - Português</a></p>
@endif

@if (\Popup::en()->count() == 0)
    <p><a href="{{ URL::route('painel.popup.create', array('idioma' => 'en')) }}" class="btn btn-mini btn-info">Novo Pop Up - Inglês</a></p>
@endif

@if ($popups->count())
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Idioma</th>
                <th>Título</th>
                <th>Status</th>
                <th>Data de Exibição</th>
                <th>Ações</th>
                <th class="span1"><i class="iconic-cog"></i></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($popups as $popup)
                <td>
                    @if($popup->idioma == 'pt-br')
                        Português
                    @elseif($popup->idioma == 'en')
                        Inglês
                    @endif
                </td>
                <td>
                    {{ $popup->titulo }}
                </td>
                <td>
                    @if($popup->publicar == '1')
                        Publicado
                    @else
                        Rascunho
                    @endif
                </td>
                <td>
                    {{ $popup->data_inicio }} até {{ $popup->data_termino }}
                </td>
                <td>{{ link_to_route('painel.popup.edit', 'Editar', array($popup->id), array('class' => 'btn btn-info btn-mini')) }}</td>
                <td>
                    {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.popup.destroy', $popup->id))) }}
                        {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                    {{ Form::close() }}
                </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@else
    Nenhum Pop Up cadastrado
@endif

@stop
