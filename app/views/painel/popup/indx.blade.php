@extends('painel._layouts.default')

@section('main')

<h1>Pop Up</h1>

<p>{{ link_to_route('painel.popup.create', 'Nova notícia', NULL,
    array('class' => 'btn btn-mini btn-info')) }}</p>

@if ($popup->count())
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Publicada em</th>
                <th>Titulo</th>
                <th>Slug</th>
                <th>Descricao</th>
                <th>Status</th>
                <th>Criada por</th>
                <th class="span1"><i class="iconic-cog"></i></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($popup as $pop)
                <tr>
                    <td>{{{ date('d/m/Y H:i', strtotime($pop->published)) . 'h' }}}</td>
                    <td>{{{ $pop->titulo }}}</td>
                    <td>{{{ $pop->slug }}}</td>
                    <td>{{ Str::words($pop->descricao, 20) }}</td>
                    <td>{{{ ($pop->status) ? 'pública' : 'privada' }}}</td>
                    <td>{{{ Sentry::getUserProvider()->findById($pop->user_id)->first_name }}}</td>
                    <td>{{ link_to_route('painel.popup.edit', 'Editar', array($pop->id), array('class' => 'btn btn-info btn-mini')) }}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.popup.destroy', $pop->id))) }}
                            {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@else
    Nenhuma notícia cadastrada
@endif

@stop
