@extends('painel._layouts.default')

@section('main')

<h1>Novo usuário</h1>

{{ Form::open(array('files' => TRUE, 'route' => 'painel.users.store')) }}
    <div class="control-group {{ $errors->first('first_name') ? 'error' : '' }}">
        {{ Form::label('first_name', 'Nome:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('first_name', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('first_name') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('username') ? 'error' : '' }}">
        {{ Form::label('username', 'Login:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('username', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('username') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('email') ? 'error' : '' }}">
        {{ Form::label('email', 'Email:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('email', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('email') }}<h5>
    </div>
    <div class="control-group {{ $errors->first('password') ? 'error' : '' }}">
        {{ Form::label('password', 'Senha:', array('class' => 'control-label')) }}
        <div class="controls">
            {{ Form::text('password', NULL, array('class' => 'span4')) }}
        </div>
        <h5>{{ $errors->first('password') }}<h5>
    </div>

    {{ Form::submit('Salvar', array('class' => 'btn')) }}
{{ Form::close() }}

@if ($errors->any())
    <ul>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </ul>
@endif

@stop
