@extends('painel._layouts.default')

@section('main')

<h1>Usuários</h1>

<p>{{ link_to_route('painel.users.create', 'Novo usuário', NULL,
    array('class' => 'btn btn-mini btn-info')) }}</p>

@if ($users->count())
    <table class="table table-striped table-condensed">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Login</th>
                <th>Email</th>
                <th class="span1"><i class="iconic-cog"></i></th>
            </tr>
        </thead>

        <tbody>
            @foreach ($users as $user)
                <tr id="user_{{ $user->id }}">
                    <td>{{{ $user->first_name }}}</td>
                    <td>{{{ $user->username }}}</td>
                    <td>{{{ $user->email }}}</td>
                    <td>
                        {{ Form::open(array('method' => 'DELETE', 'route' => array('painel.users.destroy', $user->id))) }}
                            {{ Form::submit('Apagar', array('class' => 'btn btn-danger btn-mini')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@else
    <p>Nenhuma rede user cadastrada.</p>
@endif

@stop
