@extends('site.layout.default')

@section('main')
<div class="interna conteudo contato">
    <h1>{{ trans('site.contato.index.Contato') }}</h1>
    <div class="clearfix"></div>
    <div class="inner-content">
        <div class="resumo">
            <div class="suporte-form">
                <h2>{{ trans('site.contato.index.Envie-nos uma mensagem') }}</h2>
                {{ Form::open(array('id' => 'contato-form')) }}
                    <div class="inputs">
                        {{ Form::text('nome', null, array('placeholder' => trans('site.contato.index.Nome'), 'id' => 'nome'))}}
                        {{ Form::text('email', null, array('placeholder' => trans('site.contato.index.Email'), 'id' => 'email'))}}
                        {{ Form::text('empresa', null, array('placeholder' => trans('site.contato.index.Empresa'), 'id' => 'empresa'))}}
                        {{ Form::text('telefone', null, array('placeholder' => trans('site.contato.index.Telefone'), 'id' => 'telefone'))}}
                    </div>
                    <div class="textarea">
                        <?php
                        $options = array(
                            '' => trans('site.contato.index.Setor'),
                            'Aeronáutico' => trans('site.contato.index.Aeronáutico'),
                            'Agricultura' => trans('site.contato.index.Agricultura'),
                            'Construção Civil' => trans('site.contato.index.Construção'),
                            'Forças Armadas' => trans('site.contato.index.Forças'),
                            'Governo' => trans('site.contato.index.Governo'),
                            'Marítimo' => trans('site.contato.index.Marítimo'),
                            'Mídia' => trans('site.contato.index.Mídia'),
                            'Mineração' => trans('site.contato.index.Mineração'),
                            'Petróleo' => trans('site.contato.index.Petróleo'),
                            'Segurança' => trans('site.contato.index.Segurança'),
                            'Outros' => trans('site.contato.index.Outros'),
                        );
                        ?>
                        {{ Form::select('setor', $options, '', array('id' => 'setor')) }}
                        {{ Form::textarea('mensagem', null, array('placeholder' => trans('site.contato.index.Mensagem'), 'id' => 'mensagem')) }}
                    </div>
                    <div class="clearfix"></div>
                    <div class="submit-placeholder">
                        {{ Form::submit(trans('site.contato.index.ENVIAR').' &raquo;') }}
                    </div>
                {{ Form::close() }}
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="locais-wrapper">
            <div class="locais">
                @foreach ($locais as $local)
                    <div class="local">
                        <div class="local-imagem">
                            <img src="{{ URL::asset($local->imagem) }}" alt="{{ $local->traduz('titulo') }}">
                        </div>
                        <h2>{{ $local->traduz('titulo') }}</h2>
                        {{ $local->endereco }}
                        @if($local->link_mapa)
                            <a class="link-mapa" href="{{$local->link_mapa}}" target="_blank">{{ trans('site.contato.index.ver mapa') }} &raquo;</a><br>
                        @endif
                        <span class="telefone1"><b>{{ trans('site.contato.index.T') }}.</b> {{ $local->ddd1 }} {{ $local->telefone1 }}</span>
                        @if ($local->telefone2 !== "")
                            <br>
                            <span class="telefone2">
                                <b>{{ trans('site.contato.index.F') }}.</b> {{ $local->ddd2 }} {{ $local->telefone2 }}
                            </span>
                        @endif
                    </div>
                @endforeach
            </div>
            <div class="clearfix"></div>
            <div class="contato-email">
                <img src="{{ URL::asset('assets/img/contato_email.png') }}" alt="{{ trans('site.contato.index.Contato Arycom') }}"> <a target="_blank" href="mailto:arycom@arycom.com">arycom@arycom.com</a>
            </div>
            <div class="clearfix"></div>
            <div id="box-imprensa">
                <div class="coluna coluna-100">
                    <!-- <p>
                        <strong>Contato Marketing</strong>
                        T: +55 11 3051-3001
                        <a target="_blank" href="mailto:marketing@arycom.com" title="Envie um e-mail">marketing@arycom.com</a>
                    </p> -->
                    <p>
                        <a class="link-skype" href="skype:arycom?call" title="Entre em contato via Skype">Skype: arycom</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
@stop
