@extends('site.layout.default')

@section('main')
<div class="interna conteudo contato suporte form-ativacao">
    <h1>{{ trans('site.contato.ativar.Ativar Equipamento') }}</h1>
    <div class="clearfix"></div>
    <div class="inner-content">

    	<div class="resumo low">
            <p>
            	{{ trans('site.contato.ativar.Se você já tem equipamento de comunicação') }}
            </p>
        </div>

        <div class="resumo branco">
            <div class="ativacao-form">
                <form action="ativar" method="post" id="form-ativar">
                	<div class="coluna coluna-50 prim">
                		<input type="text" name="nome" placeholder="{{ trans('site.contato.ativar.Nome') }}" required id="txt-nome">
                	</div>
                	<div class="coluna coluna-50">
                		<input type="text" name="empresa" placeholder="{{ trans('site.contato.ativar.Empresa') }}" required id="txt-empresa">
                	</div>
                	<div class="coluna coluna-50 prim">
                		<input type="email" name="email" placeholder="{{ trans('site.contato.ativar.E-mail') }}" required id="txt-email">
                	</div>
                	<div class="coluna coluna-50">
                		<input type="text" name="telefone" placeholder="{{ trans('site.contato.ativar.Telefone') }}" required id="txt-telefone">
                	</div>
                	<textarea name="equipamentos" class="txtarea-ativacao med" placeholder="{{ trans('site.contato.ativar.Equipamentos') }}" required id="txt-equipamentos"></textarea>
                	<textarea name="servicos" class="txtarea-ativacao high" placeholder="{{ trans('site.contato.ativar.Serviços que deseja realizar') }}" required id="txt-servicos"></textarea>
                	<textarea name="observacoes" class="txtarea-ativacao high" placeholder="{{ trans('site.contato.ativar.Observações (opcional)') }}" id="txt-observacoes"></textarea>
                	<div class="submit-placeholder">
                		<input type="submit" id="link-submit" title="{{ trans('site.contato.ativar.ENVIAR') }} &raquo;" value="{{ trans('site.contato.ativar.ENVIAR') }} &raquo;">
                	</div>
                </form>
                <div class="clearfix"></div>
            </div>
        </div>

    </div>
    <div class="clearfix"></div>
</div>
@stop
