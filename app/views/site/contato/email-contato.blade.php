<!DOCTYPE html>
<html>
<head>
    <title>Mensagem de contato via formulário do site</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'> Nome :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nome }}</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'> E-mail :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $email }}</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'> Empresa :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $empresa }}</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'> Telefone :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $telefone }}</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'> Setor :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $setor }}</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'> Mensagem :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $mensagem }}</span>
</body>
</html>