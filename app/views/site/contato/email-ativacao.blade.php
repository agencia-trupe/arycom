<!DOCTYPE html>
<html>
<head>
    <title>Mensagem de contato via formulário do site</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'> Nome :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nome }}</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'> E-mail :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $email }}</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'> Empresa :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $empresa }}</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'> Telefone :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $telefone }}</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'> Equipamentos :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $equipamentos }}</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'> Serviços :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $servicos }}</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'> Observações :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $observacoes }}</span>
</body>
</html>