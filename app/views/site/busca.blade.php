@extends('site.layout.default')

@section('main')
<div class="interna conteudo noticias">
    <h1 class="tit-secao">{{ trans('site.busca.Resultado da Busca') }}</h1>
    <div class="clearfix"></div>
    <div class="inner-content">
        <div class="busca-lista">
            @if(sizeof($resultados) > 0)
                @foreach ($resultados as $busca)
                    <a href="{{$busca->url}}" class="busca-container">
                        @if(\Config::get('app.locale') == 'en')
                            <div class="busca-lista-titulo">
                                <h1>
                                    {{ $busca->titulo_en }}
                                </h1>
                            </div>
                            <div class="busca-lista-resumo">
                                {{ Str::words(strip_tags($busca->olho_en), 35) }}
                            </div>
                        @else
                            <div class="busca-lista-titulo">
                                <h1>
                                    {{ $busca->titulo }}
                                </h1>
                            </div>
                            <div class="busca-lista-resumo">
                                {{ Str::words(strip_tags($busca->olho), 35) }}
                            </div>
                        @endif
                    </a>
                @endforeach
            @else
                <div class="semresultados">
                    {{ trans('site.busca.Nenhum resultado encontrado para') }} '{{$termo}}'
                </div>
            @endif
        </div>
    </div>
    <div class="clearfix"></div>
</div>
@stop
