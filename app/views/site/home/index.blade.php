@extends('site.layout.default')

@section('main')

    <div class="triangulo-home"></div>

    <div class="content-home">

        <h1>{{ trans('site.home.Comunicação via Satélite') }}</h1>

        <div class="atuacao-home">

            <span>{{ trans('site.home.Áreas de Atuação') }}:</span>

            <ul class="atuacao-home-lista">
                @foreach ($atuacoes as $atuacao)
                        <li>
                            @if($atuacao->traduz('descricao') != '')
                                <a href="{{ URL::to('solucoes/area/' . $atuacao->slug )}}" class="home-atuacao-link" data-area="{{ $atuacao->slug }}">&raquo; {{ $atuacao->traduz('titulo') }}</a>
                            @endif
                        </li>
                @endforeach
            </ul>

            <div class="clearfix"></div>

            <div class="home-atuacao-triangulo"></div>

        </div>

        <div class="texto-home-tablets">
            {{ $textoHome }}
        </div>
    </div>

    <div class="losango-home">

        <div class="texto-home">
            {{ $textoHome }}
        </div>

        <div class="clearfix"></div>

        <div class="noticias-home">

            <h2>{{ trans('site.home.Notícias') }}</h2>

            <ul>
                @foreach ($noticias as $noticia)
                    <li>
                        <a href="{{ URL::to('noticias/detalhe/' . $noticia->slug)}}"><span>{{ $noticia->traduz('titulo') }}</span></a>
                    </li>
                @endforeach
            </ul>

            <div class="clearfix"></div>

            <a href="{{ URL::to('noticias') }}" class="home-ver-todas">{{ trans('site.home.ver todas as notícias') }}</a>

        </div>
    </div>

    <?php $pagina = 'home'; ?>

@stop