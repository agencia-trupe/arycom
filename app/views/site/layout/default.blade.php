<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Arycom - {{ trans('site.layout.Comunicação via Satélite') }}</title>
        <meta name="description" content="A Arycom é uma operadora e provedora de soluções completas em telecomunicações via satélite. Oferecemos as melhores soluções para comunicação móvel ou fixa em ambiente terrestre, aéreo ou marítimo, por meio de uma rede global de satélites.">
        <meta name="keywords" content="internet satelital, Inmarsat, Iridium , Globalstar, Telefonia satelital , Telemetria, satélite , FBB, SBB , BGAN">
        <meta name="viewport" content="width=device-width">

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="{{ URL::asset('assets/css/normalize.css')}}">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/main-mobile.css')}}" media="all and (max-width: 480px)">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/main-tablet.css')}}" media="all and (min-width: 481px) and (max-width: 1024px)">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/main.css')}}" media="all and (min-width: 1025px)">

        <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <script src="{{ URL::asset('assets/js/vendor/modernizr-2.6.2.min.js')}}"></script>

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-51635705-1', 'arycom.com');
            ga('send', 'pageview');
        </script>
    </head>
    <body <?php echo (isset($pagina)) ? 'class="' . $pagina . '"' : '' ?>>
        <div class="direita-wrapper">
            <div class="triangulo-direita"></div>
            <div class="direita"></div>
            <div class="retangulo-branco"></div>
            <div class="cycle-slideshow internas cycle-autoinit"
                data-cycle-fx="scrollHorz"
                data-cycle-speed="100000"
                data-cycle-timeout="1"
                data-cycle-easing="linear"
                >
                <img src="{{ URL::asset('assets/img/home_img-bg.jpg') }}">
                <img src="{{ URL::asset('assets/img/home_img-bg.jpg') }}">
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="triangulo-conteudo"></div>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->
        <header>
            <div class="ativar"></div>

            <a class="link-idioma" href="{{ Lang::get('site.layout.outro-idioma-url') }}" title="{{ Lang::get('site.layout.outro-idioma') }}"><img src="/assets/img/{{ Lang::get('site.layout.outro-idioma-img') }}" alt="{{ Lang::get('site.layout.outro-idioma') }}"></a>

            <a href="ativar" id="ativar-link" title=" {{ trans('site.layout.Ativar equipamento com a Arycom!') }}">{{ trans('site.layout.Já tenho o equipamento! Quero ativar com a ARYCOM!') }}</a>
            <div id="busca">
                <form action="{{URL::to('busca')}}" method="post">
                    <input type="text" name="busca" required placeholder="{{ trans('site.layout.buscar') }}">
                    <input type="submit">
                </form>
            </div>
            <div class="interna">
                <div class="nav-triangle-wrapper">
                    <div class="nav-triangle"></div>
                    <nav>
                        <ul>
                            <li class="{{ Request::is('/') ? 'active' : '' }}">
                                <a href="{{ URL::to('/')}}">{{ trans('site.layout.Home') }}</a>
                            </li>
                            <li class="{{ Request::is('empresa') ? 'active' : '' }}">
                                <a href="{{ URL::to('empresa')}}">{{ trans('site.layout.Empresa') }}</a>
                            </li>
                            <li class="{{ Request::is('solucoes*') || Request::is('produtos*') ? 'active' : '' }}">
                                <a href="{{ URL::to('solucoes')}}">{{ trans('site.layout.Soluções') }}</a>
                            </li>
                            <li class="{{ Request::is('parceiros') ? 'active' : '' }}">
                                <a href="{{ URL::to('parceiros')}}">{{ trans('site.layout.Parceiros') }}</a>
                            </li>
                            <li class="{{ Request::is('noticias*') ? 'active' : '' }}">
                                <a href="{{ URL::to('noticias')}}">{{ trans('site.layout.Notícias') }}</a>
                            </li>
                            <li class="{{ Request::is('midia*') ? 'active' : '' }}">
                                <a href="{{ URL::to('midia')}}">{{ trans('site.layout.Multimídia') }}</a>
                            </li>
                            <li class="{{ Request::is('suporte') ? 'active' : '' }}">
                                <a href="{{ URL::to('suporte')}}">{{ trans('site.layout.Suporte') }}</a>
                            </li>
                            <li class="{{ Request::is('contato') || Request::is('ativar') ? 'active' : '' }}">
                                <a href="{{ URL::to('contato')}}">{{ trans('site.layout.Contato') }}</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="marca-triangle-wrapper">
                    <div class="marca-triangle"></div>
                    <a href="{{ URL::to('/')}}" class="marca {{ trans('site.layout.classe_marca') }}"></a>
                </div>
                <div class="clearfix"></div>
            </div>
        </header>

            <div class="cycle-slideshow home cycle-autoinit"
                data-cycle-fx="scrollHorz"
                data-cycle-speed="190000"
                data-cycle-timeout="1"
                data-cycle-easing="linear"
                >
                <img src="{{ URL::asset('assets/img/home_img-bg.jpg') }}">
                <img src="{{ URL::asset('assets/img/home_img-bg.jpg') }}">
            </div>

        @yield('main')

        <a href="ativar" title="{{ trans('site.layout.Já tenho o equipamento! Quero ativar com a Arycom!') }}" id="ativar-responsive">{{ trans('site.layout.Já tenho o equipamento! <br>Quero ativar com a Arycom!') }}</a>

        <footer>
            <div class="footer-wrapper">
                <nav>
                    <ul>
                             <li><a href="{{ URL::to('/')}}">&raquo; {{ trans('site.layout.Home') }}</a></li>
                             <li><a href="{{ URL::to('empresa')}}">&raquo; {{ trans('site.layout.Empresa') }}</a></li>
                             <li><a href="{{ URL::to('/solucoes')}}">&raquo; {{ trans('site.layout.Soluções') }}</a></li>
                             <li><a href="{{ URL::to('/parceiros')}}">&raquo; {{ trans('site.layout.Parceiros') }}</a></li>
                             <li><a href="{{ URL::to('/noticias')}}">&raquo; {{ trans('site.layout.Notícias') }}</a></li>
                             <li><a href="{{ URL::to('/midia')}}">&raquo; {{ trans('site.layout.Multimídia') }}</a></li>
                             <li><a href="{{ URL::to('/suporte')}}">&raquo; {{ trans('site.layout.Suporte') }}</a></li>
                             <li><a href="{{ URL::to('/contato')}}">&raquo; {{ trans('site.layout.Contato') }}</a></li>
                    </ul>
                </nav>
                <div class="footer-contatos">
                    <span class="footer-contatos-titulo">{{ trans('site.layout.Contato') }}</span>
                    @foreach ($contatos as $contato)
                        <div class="footer-contato">
                            <div class="footer-contato-titulo">{{ $contato->traduz('titulo') }}</div>
                            <div class="footer-contato-telefone">
                                {{$contato->ddd1}} {{$contato->telefone1}}
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="footer-social">
                    <ul>
                        @foreach ($socials as $social)
                            <li><a href="{{ $social->link }}" target="_blank"><img src="{{ URL::asset( $social->imagem ) }}" alt="{{ $social->traduz('titulo') }}"></a></li>
                        @endforeach
                    </ul>
                    <div class="clearfix"></div>
                    <div class="facebook-like">
                    <iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Freference%2Fplugins%2Flike&amp;width=82&amp;height=21&amp;colorscheme=light&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;send=false&amp;locale={{ trans('site.layout.fb_lang') }}&amp;appId=268019046656558" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:82px; height:21px;" allowTransparency="true"></iframe>
                    </div>
                    <div class="clearfix"></div>
                    <div class="copy-sites">
                        <span>&copy; <?=Date('Y')?> Arycom - {{ trans('site.layout.Todos os direitos reservados') }}.</span>
                        <br>
                        <a href="http://trupe.net" target="_blank">Criação de sites:</a>
                        <a href="http://trupe.net" target="_blank">Trupe Agência Criativa <img src="{{ URL::asset('assets/img/trupe.png') }}" alt=""></a>
                    </div>
                </div>
            </div>
        </footer>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="{{ URL::asset('js/vendor/jquery-1.9.1.min.js') }}"><\/script>')</script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
        <script src="{{ URL::asset( 'assets/js/plugins.js' ) }}"></script>
        <script src="{{ URL::asset( 'assets/js/main.js' ) }}"></script>

        @if(isset($popup) && $popup)
            <link rel="stylesheet" href="assets/css/fancybox/fancybox.css">

            <script src="assets/js/fancybox.js"></script>

            <script type="text/javascript">
                $('document').ready( function(){
                    var popup = {{ $popup }}

                    if(popup.tipo == 'imagem'){

                        var box_html = "";
                        box_html += "<a href='"+popup.link+"' target='"+popup.destino_link+"' style='display:block'>";
                            box_html += "<img style='display:block;' src='"+popup.imagem+"'>";
                        box_html += "</a>";

                        $.fancybox({
                            type  : 'html',
                            content : box_html,
                            autoHeight : true,
                            onComplete : function(){
                                var img = new Image;
                                img.onload = function(){
                                    var largura_tela = $( window ).width();
                                    var altura_tela = $( window ).height();

                                    $('#fancybox-content a img').css({
                                        maxWidth : .7 * largura_tela,
                                        maxHeight : .8 * altura_tela
                                    });

                                    var largura = parseInt($('#fancybox-content a img').css('width'));
                                    var altura = parseInt($('#fancybox-content a img').css('height'));

                                    $('#fancybox-content, #fancybox-content a').css({
                                        width : largura,
                                        height : altura
                                    });

                                    $.fancybox.resize();
                                }

                                img.src = popup.imagem;
                            }
                        });

                    }else if(popup.tipo == 'video'){

                        $.fancybox({
                            'padding'       : 0,
                            'autoScale'     : false,
                            'transitionIn'  : 'none',
                            'transitionOut' : 'none',
                            'title'         : false,
                            'width'         : 680,
                            'height'        : 495,
                            'href'          : popup.video_embed.replace(new RegExp("watch\\?v=", "i"), 'v/'),
                            'type'          : 'swf',
                            'swf'           : {
                                 'wmode'        : 'transparent',
                                'allowfullscreen'   : 'true'
                            },
                        });

                    }

                })
            </script>
        @endif
    </body>
</html>
