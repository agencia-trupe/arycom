@extends('site.layout.default')

@section('main')
<div class="interna conteudo midias">
    <h1>{{ $audioVisual->traduz('titulo') }}</h1>
    <div class="clearfix"></div>
    <div class="inner-content">
        <div class="resumo">
            {{ $audioVisual->traduz('resumo') }}
            <div class="clearfix"></div>
        </div>
        <div class="midia-tipo">
            <ul>
                @foreach ($midiaTipos as $midiaTipo)
                    @if ($midiaTipo->id === $tipo->id)
                        <li class="active">
                            <div class="button">
                                {{ $midiaTipo->traduz('titulo')}}
                            </div>
                            <div class="triangulo-midia"></div>
                        </li>
                    @else
                        <li>
                            <a class="button" href="{{ URL::to('midia/lista/' . $midiaTipo->slug) }}">{{$midiaTipo->traduz('titulo')}}</a></li>
                    @endif
                @endforeach
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="midias">
            @if ($tipo->slug === 'videos')
                @foreach ($midias as $midia)
                    <a class="midia-link" href="{{ URL::to('midia/video/' . $midia->slug) }}">
                        <img src="{{Video::videoThumb($midia->path)}}" style="width:180px;">
                        <span class="overlay">
                            <span class="video-icone"></span>
                            <span class="titulo">{{ $midia->traduz('titulo') }}</span>
                        </span>
                    </a>
                @endforeach
            @else
                @foreach ($midias as $midia)
                    <a rel="lightbox-fotos" title="{{ $midia->traduz('descricao') }}" href="{{ URL::asset(Image::resize( $midia->path, 700, 700))}}" class="midia-link">
                        <img src="{{ URL::asset(Image::thumb($midia->path, 180, 130))}}" alt="{{ $midia->traduz('titulo') }}">
                        <span class="overlay">
                                <span class="foto-icone"></span>
                                <span class="titulo">{{ $midia->traduz('titulo') }}</span>
                        </span>
                    </a>
                @endforeach
            @endif
            <div class="clearfix"></div>
        </div>
        <div class="midia-links">{{ $midias->links() }}</div>
        <div id="box-imprensa">
            <div class="coluna coluna-100">
                <p>
                    <strong>{{ trans('site.midias.index.Contato Marketing') }}</strong>
                    {{ trans('site.midias.index.T') }}: +55 11 3051-3001
                    <a target="_blank" href="mailto:marketing@arycom.com" title="{{ trans('site.midias.index.Envie um e-mail') }}">marketing@arycom.com</a>
                </p>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
@stop
