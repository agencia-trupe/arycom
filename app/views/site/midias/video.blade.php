@extends('site.layout.default')

@section('main')
<div class="interna conteudo midias">
    <h1>{{ $audioVisual->traduz('titulo') }}</h1>
    <div class="clearfix"></div>
    <div class="inner-content">
        <div class="resumo">
            {{ $video->traduz('descricao') }}
            <div class="clearfix"></div>
        </div>
        <div class="player">
            {{ Video::buildEmbed( $video->path, null, 755, 460 ) }}
        </div>
        <a href="{{ URL::to('/midia/lista/videos') }}" class="link-voltar">&laquo; {{ trans('site.midias.video.voltar') }}</a>
        <div id="box-imprensa">
            <div class="coluna coluna-100">
                <p>
                    <strong>{{ trans('site.midias.video.Contato Marketing') }}</strong>
                    {{ trans('site.midias.video.T') }}: +55 11 3051-3001
                    <a target="_blank" href="mailto:marketing@arycom.com" title="{{ trans('site.midias.video.Envie um e-mail') }}">marketing@arycom.com</a>
                </p>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
@stop
