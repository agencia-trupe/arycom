@extends('site.layout.default')

@section('main')
<div class="interna conteudo suporte">
    <h1>{{ trans('site.suporte.Suporte') }}</h1>
    <div class="clearfix"></div>
    <div class="inner-content">
        <div class="resumo branco suportemin">
            <div class="coluna coluna-50">
                <div class="contato-suporte">
                    <p>
                        {{ trans('site.suporte.Comunique-se com nossa área de suporte') }}:
                    </p>
                    <p>
                        +55 11 3051-3001
                        <br>
                        <a target="_blank" href="mailto:suporte@arycom.com" class="mail" title="{{ trans('site.suporte.Envie um e-mail') }}!">suporte@arycom.com</a>
                        <br>
                        <a href="skype:arycom?call" title="Skype" class="skype-link">skype: arycom</a>
                    </p>
                </div>
            </div>
            <div class="coluna coluna-50">
                <div class="texto-suporte">
                    <p>
                        {{ trans('site.suporte.Se você é usuário') }}
                    </p>
                    <a href="https://datamanager.vizada.com/" title="Data Manager" target="_blank">datamanager &raquo;</a>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
@stop
