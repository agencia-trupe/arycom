@extends('site.layout.default')

@section('main')
    <div class="conteudo solucoes interna">
        <div class="clearfix"></div>
        <h1>{{ trans('site.produtos.index.Soluções') }}</h1>
        <div class="clearfix"></div>
        <div class="table-wrapper">
        <div class="solucoes-header-image"></div>
            <table cellspacing="0" cellpadding="0" class="produtos">
                <thead>
                    <tr>
                        <th class="table-title">
                            <span>{{ trans('site.produtos.index.Selecione') }}.</span></th>
                        @foreach ( $atuacoes as $atuacao )
                            <th>
                                <div class="thinner">
                                    <div class="thtop"></div>
                                    <a data-area-definition="area-{{ $atuacao->id }}" title="{{ \Config::get('app.locale') }}" href="{{ URL::to('solucoes/area/' . $atuacao->slug) }}" class="{{ $atuacao->slug }} thbottom @if(\Config::get('app.locale') == 'en') espacamento-en @endif">
                                        <span>
                                            {{ $atuacao->traduz('titulo') }}
                                        </span>
                                    </a>
                                    <div class="clearfix"></div>
                                </div>
                            </th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @foreach ($categorias as $categoria)
                        <tr>
                            <td colspan="{{$atuacoes->count() + 1}}">
                                <table cellspacing="0" cellpadding="0" class="accordion-wrapper">
                                    <tbody>
                                        <tr class="accordion">
                                            <td colspan="{{$atuacoes->count() + 1}}"><span>{{ $categoria->traduz('titulo') }}</span></td>
                                        </tr>
                                        @foreach ($categoria->produtos as $produto)
                                            <tr class="produto" data-link="{{ URL::to('solucoes/produto/' . $produto->slug) }}">
                                                <td class="produto-titulo"><span>{{ $produto->traduz('titulo') }}</span></td>
                                                @foreach ($atuacoes as $atuacao)
                                                    @if( in_array($produto->id, $atuacao->produtos->lists('id')))
                                                        <td data-area="area-{{$atuacao->id}}" class="pointer"><span>.</span></td>
                                                    @else
                                                        <td data-area="area-{{$atuacao->id}}"></td>
                                                    @endif
                                                @endforeach
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="clearfix"></div>
        </div>
        <div class="lista-solucoes-responsive">

            <h2>{{ trans('site.produtos.index.Soluções por CATEGORIA') }}</h2>
            <ul>
                @foreach ( $atuacoes as $atuacao )
                    <li><a href="{{ URL::to('solucoes/area/' . $atuacao->slug) }}" title="{{$atuacao->traduz('titulo')}}">{{$atuacao->traduz('titulo')}}</a></li>
                @endforeach
            </ul>

            <h2 class="preto">{{ trans('site.produtos.index.Soluções por PRODUTO') }}</h2>
            <ul>
                @foreach ($categorias as $categoria)
                    @foreach ($categoria->produtos as $produto)
                        <li><a href="{{ URL::to('solucoes/produto/' . $produto->slug) }}" title="{{$produto->traduz('titulo')}}">{{$produto->traduz('titulo')}}</a></li>
                    @endforeach
                @endforeach
            </ul>
        </div>
    </div>
@stop
