@extends('site.layout.default')

@section('main')
    <div class="conteudo solucoes geral interna">
        <div class="inner-content">
            <div class="clearfix"></div>
            <h1>{{ trans('site.produtos.area.Soluções') }}</h1>
            <div class="clearfix"></div>
            <h2>{{ $atuacao->traduz('titulo') }}</h2>
            <div class="atuacao-resumo">
                {{ $atuacao->traduz('resumo') }}
                <div class="clearfix"></div>
            </div>
            <div class="atuacao-main">
                <div class="atuacao-main-left">
                    {{ $atuacao->traduz('descricao') }}
                    <h2>{{ trans('site.produtos.area.Áreas de atuação') }}:</h2>
                    <div class="atuacao-accordion">
                        @foreach ($atuacao->especificas as $especifica)
                            <h3>{{ $especifica->traduz('titulo') }}</h3>
                            <div class="especifica-descricao">
                                {{ str_replace("", "", $especifica->traduz('descricao')) }}
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="atuacao-main-right">
                    <div class="atuacao-imagem">
                        <img src="{{ URL::asset($atuacao->imagem) }}" alt="{{ $atuacao->traduz('titulo') }}">
                    </div>
                    <div class="atuacao-outras">
                        <h3>{{ trans('site.produtos.area.Soluções para outras aplicações') }}</h3>
                        <ul>
                            @foreach ($atuacoes as $value)
                                @if ($value->id !== $atuacao->id)
                                    <li>
                                        <a href="{{ URL::to('solucoes/area/' . $value->slug) }}" class="atuacao-outra">
                                            <span class="clearfix"> </span>
                                            <span class="atuacao-outra-imagem">
                                                <img width="77" height="49" src="{{ URL::asset(Image::resize($value->imagem, 77, 49, false, 100)) }}" alt="{{ $value->traduz('titulo') }}">
                                                <span class="image-wrapper"></span>
                                            </span>
                                            <span class="atuacao-titulo">
                                                &raquo; {{ $value->traduz('titulo') }}
                                            </span>
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
                <a href="{{ URL::to('/solucoes') }}" class="link-voltar">&laquo; {{ trans('site.produtos.area.voltar') }}</a>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
@stop
