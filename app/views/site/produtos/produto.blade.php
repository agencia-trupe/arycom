@extends('site.layout.default')

@section('main')
    <div class="conteudo solucoes geral produto interna">
        <div class="inner-content">

            <div class="clearfix"></div>

            <h1>{{ trans('site.produtos.produto.Soluções') }}</h1>

            <div class="clearfix"></div>

            <div class="title-wrapper">
                <h2 class='preto'>{{ $produto->traduz('titulo') }}</h2>
            </div>

            <div class="imagem-produto-responsive">
                <img src="{{ URL::asset($produto->imagem) }}" alt="{{ $produto->traduz('titulo') }}">
            </div>

            <div class="atuacao-main">
                <ul class="produto-areas">
                    @foreach ($produto->atuacoes as $atuacao)
                        <li>
                            <a href="{{ URL::to('solucoes/area/' . $atuacao->slug) }}" title="{{$atuacao->traduz('titulo')}}">
                                <img src="{{ Image::thumb( URL::asset($atuacao->imagem), 77, 49) }}" alt="{{$atuacao->traduz('titulo')}}">
                                <span class="triangulo"></span>
                            </a>
                        </li>
                    @endforeach
                </ul>

                <div class="clearfix"></div>

                <div class="atuacao-main-left">
                    {{ $produto->traduz('descricao') }}
                    @if ($produto->video)
                        {{ $produto->embed() }}
                    @endif
                </div>

                <div class="atuacao-main-right">
                    <div class="produto-imagem">
                        <img src="{{ URL::asset($produto->imagem) }}" alt="{{ $produto->titulo }}">
                    </div>
                    <div class="produto-arquivos">
                        @if(Config::get('app.locale') == 'en')
                            @if (count($produto->arquivosEn))
                                <h3>Download material</h3>
                                @foreach ($produto->arquivosEn as $arquivo)
                                    <a href="{{ URL::asset($arquivo->path) }}" target="_blank" class="produto-arquivo">
                                        <img src="{{ URL::asset('assets/img/icone-pdf.png')}}" alt="{{ $arquivo->titulo }}">
                                        <span class="arquivo-titulo">
                                            {{ $arquivo->titulo }}
                                        </span>
                                    </a>
                                @endforeach
                            @endif
                        @else
                            @if (count($produto->arquivos))
                                <h3>Materiais para download</h3>
                                @foreach ($produto->arquivos as $arquivo)
                                    <a href="{{ URL::asset($arquivo->path) }}" target="_blank" class="produto-arquivo">
                                        <img src="{{ URL::asset('assets/img/icone-pdf.png')}}" alt="{{ $arquivo->titulo }}">
                                        <span class="arquivo-titulo">
                                            {{ $arquivo->titulo }}
                                        </span>
                                    </a>
                                @endforeach
                            @endif
                        @endif


                        <div class="clearfix"></div>
                    </div>
                    <div class="produto-features">
                        @foreach ($produto->features as $feature)
                            <img src="{{ URL::asset($feature->traduz('imagem')) }}" alt="{{ $feature->traduz('titulo') }}">
                        @endforeach
                    </div>
                </div>

                <div class="clearfix"></div>

                <a href="{{ URL::to('/solucoes') }}" class="link-voltar">&laquo; {{ trans('site.produtos.produto.voltar') }}</a>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
@stop
