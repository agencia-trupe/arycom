@extends('site.layout.default')

@section('main')
<div class="interna conteudo parceiros">
    <h1>{{ $pagina_slug->traduz('titulo') }}</h1>
    <div class="clearfix"></div>
    <div class="inner-content">
        <div class="resumo">
            {{ $parceiros->traduz('resumo') }}
            <div class="clearfix"></div>
        </div>
        <div class="imagem">
            <img class="desktop" src="{{ URL::asset($parceiros->imagem) }}" alt="{{ trans('site.parceiros.Arycom Comunicação via Satélite') }}">
        </div>
    </div>
    <div class="clearfix"></div>
</div>
@stop
