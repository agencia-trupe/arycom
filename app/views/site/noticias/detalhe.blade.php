@extends('site.layout.default')

@section('main')
<div class="interna conteudo noticias detalhe">
    <h1 class="tit-secao">{{ trans('site.noticias.detalhe.Notícias') }}</h1>
    <div class="clearfix"></div>
    <div class="inner-content">
        <div class="noticia-detalhe">
            <div class="noticia-container header">
                <div class="noticia-lista-data">
                    <div class="dia">{{ date('d', strtotime($noticia->published)) }}</div>
                    <div class="mes">
                        {{ mb_strtoupper(utf8_encode(strftime("%B", strtotime($noticia->published)))) }}
                    </div>
                    <div class="ano">
                        {{ date('Y', strtotime($noticia->published))}}
                    </div>
                </div>
                <div class="noticia-lista-titulo">
                    <h1>
                        {{ $noticia->traduz('titulo') }}
                    </h1>
                    <div class="noticia-lista-resumo">
                        {{ $noticia->traduz('resumo') }}
                    </div>
                </div>
            </div>
            <div class="noticia-descricao">
                @if ($noticia->imagem !== "")
                    <div class="noticia-imagem">
                        <img src="{{ URL::asset(Image::resize($noticia->imagem, 714, 476, false, 100)) }}" alt="{{ $noticia->traduz('titulo') }}">
                    </div>
                @endif
                {{ $noticia->traduz('descricao') }}
            </div>
            <a href="{{ URL::to('/noticias') }}" class="link-voltar">&laquo; {{ trans('site.noticias.detalhe.voltar') }}</a>
            <div id="box-imprensa">
            <div class="titulo">{{ trans('site.noticias.detalhe.Informações à imprensa') }}</div>
            <div class="coluna coluna-50">
                <p>
                    <strong>Érika Martins</strong>
                    +55 11 3169-9349
                    <a href="mailto:erika.martins@br.mslgroup.com" title="{{ trans('site.noticias.detalhe.Envie um e-mail') }}">erika.martins@br.mslgroup.com</a>
                </p>
            </div>
            <div class="coluna coluna-50">
                <p>
                    <strong>Camila Holgado</strong>
                    +55 11 3169-9322
                    <a href="mailto:camila.holgado@br.mslgroup.com" title="{{ trans('site.noticias.detalhe.Envie um e-mail') }}">camila.holgado@br.mslgroup.com</a>
                </p>
            </div>
        </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
@stop
