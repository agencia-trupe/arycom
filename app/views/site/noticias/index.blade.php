@extends('site.layout.default')

@section('main')
<div class="interna conteudo noticias">
    <h1 class="tit-secao">{{ trans('site.noticias.index.Notícias') }}</h1>
    <div class="clearfix"></div>
    <div class="inner-content">
        <div class="noticias-lista">
            @if (isset($destaque))
                <a href="{{ URL::to('noticias/detalhe/' . $destaque->slug) }}" class="noticia-container destaque">
                    <div class="noticia-lista-data">
                        <div class="dia">{{ date('d', strtotime($destaque->published)) }}</div>
                        <div class="mes">
                            {{ mb_strtoupper(utf8_encode(strftime("%B", strtotime($destaque->published))))}}
                        </div>
                        <div class="ano">
                            {{ date('Y', strtotime($destaque->published))}}
                        </div>
                    </div>
                    <div class="noticia-lista-titulo">
                        <h1>
                            {{ $destaque->traduz('titulo') }}
                        </h1>
                        <div class="noticia-lista-resumo">
                            {{ $destaque->traduz('resumo') }}
                        </div>
                    </div>
                    <span class="clearfix"></span>
                </a>
            @endif
            @foreach ($noticias as $noticia)
                @if($noticia->traduz('descricao') != '')
                    <a href="{{ URL::to('noticias/detalhe/' . $noticia->slug) }}" class="noticia-container">
                        <div class="noticia-lista-data">
                            <div class="dia">{{ date('d', strtotime($noticia->published)) }}</div>
                            <div class="mes">
                                {{ mb_strtoupper(utf8_encode(strftime("%B", strtotime($noticia->published))))}}
                            </div>
                            <div class="ano">
                                {{ date('Y', strtotime($noticia->published))}}
                            </div>
                        </div>
                        <div class="noticia-lista-titulo">
                            <h1>
                                {{ $noticia->traduz('titulo') }}
                            </h1>
                            <div class="noticia-lista-resumo">
                                {{ $noticia->traduz('resumo') }}
                            </div>
                        </div>
                        <span class="clearfix"></span>
                    </a>
                @endif
            @endforeach
        </div>
        <div class="noticias-links">
            {{ $noticias->links() }}
        </div>
        <div id="box-imprensa">
            <div class="titulo">{{ trans('site.noticias.index.Informações à imprensa') }}</div>
            <div class="coluna coluna-50">
                <p>
                    <strong>Érika Martins</strong>
                    +55 11 3169-9349
                    <a href="mailto:erika.martins@br.mslgroup.com" title="{{ trans('site.noticias.index.Envie um e-mail') }}">erika.martins@br.mslgroup.com</a>
                </p>
            </div>
            <div class="coluna coluna-50">
                <p>
                    <strong>Camila Holgado</strong>
                    +55 11 3169-9322
                    <a href="mailto:camila.holgado@br.mslgroup.com" title="{{ trans('site.noticias.index.Envie um e-mail') }}">camila.holgado@br.mslgroup.com</a>
                </p>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
@stop
