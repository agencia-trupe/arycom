@extends('site.layout.default')

@section('main')
<div class="interna conteudo empresa">
    <h1>{{ $pagina_slug->traduz('titulo') }}</h1>
    <div class="resumo">
        {{ $empresa->traduz('resumo') }}
    </div>
    <div class="clearfix"></div>
    <div class="texto">
        {{ $empresa->traduz('texto') }}
    </div>
    <div class="imagem">
        <img src="{{ URL::asset($empresa->imagem) }}" alt="{{ trans('site.empresa.Arycom Comunicação via Satélite') }}">
    </div>
    <div class="clearfix"></div>

    <div class="separador-horizontal"></div>
    <div class="conteudo-interno">

        <div class="colaboradores">
            <div class="clearfix"></div>
            <h1>{{ trans('site.empresa.Principais Executivos') }}</h1>

            <div class="lista-colab-tablets">
                @foreach ( $colaboradores as $k => $colaborador )
                    <div class="pad">
                        <a href="#colaborador-texto-{{$colaborador->id}}" class="colaborador-link">{{ $colaborador->nome }}</a>
                    </div>
                @endforeach
            </div>

            <div class="colaboradores-lista">

                @foreach ( $colaboradores as $k => $colaborador )

                    <div class="colaborador" id="colaborador-texto-{{$colaborador->id}}">

                        <div class="colaborador-conteudo">

                            <div class="colaborador-foto">
                                <img src="{{ URL::asset($colaborador->imagem) }}" alt="{{ $colaborador->nome }}">
                            </div>

                            <div class="colaborador-nome">{{ $colaborador->nome }}</div>

                            <div class="colaborador-cargo">{{ $colaborador->traduz('email') }}</div>

                            <a href="#colaborador-texto-{{$colaborador->id}}" class="colaborador-link">{{ $colaborador->nome }}</a>

                            <div class="colaborador-cargo mobile">{{ $colaborador->traduz('email') }}</div>

                            <div class="colaborador-descricao">
                                {{ $colaborador->traduz('descricao') }}
                            </div>

                            <div class="clearfix"></div>

                        </div>
                    </div>

                    @if($k+1%3==0 && $k>0)
                        <div class="clearfix"></div>
                    @endif

                @endforeach

            </div>

        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</div>
@stop
