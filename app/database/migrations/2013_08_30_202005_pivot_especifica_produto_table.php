<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PivotEspecificaProdutoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('especifica_produto', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('especifica_id')->unsigned()->index();
			$table->integer('produto_id')->unsigned()->index();
			$table->foreign('especifica_id')->references('id')->on('especificas')->onDelete('cascade');
			$table->foreign('produto_id')->references('id')->on('produtos')->onDelete('cascade');
		});
	}



	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('especifica_produto');
	}

}
