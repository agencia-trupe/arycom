<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTabelasIngles extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('categorias', function(Blueprint $table)
		{
			$table->string('titulo_en')->after('titulo')->nullable();
			$table->text('descricao_en')->after('descricao')->nullable();
		});
		Schema::table('colaboradors', function(Blueprint $table)
		{
			$table->string('email_en')->after('email')->nullable();
			$table->text('descricao_en')->after('descricao')->nullable();
		});
		Schema::table('features', function(Blueprint $table)
		{
			$table->string('titulo_en')->after('titulo')->nullable();
		});
		Schema::table('arquivos', function(Blueprint $table)
		{
			$table->string('titulo_en')->after('titulo')->nullable();
		});
		Schema::table('midias', function(Blueprint $table)
		{
			$table->string('titulo_en')->after('titulo')->nullable();
			$table->text('descricao_en')->after('descricao')->nullable();
		});
		Schema::table('paginas', function(Blueprint $table)
		{
			$table->string('titulo_en')->after('titulo')->nullable();
			$table->text('resumo_en')->after('resumo')->nullable();
			$table->text('texto_en')->after('texto')->nullable();
		});
		Schema::table('produtos', function(Blueprint $table)
		{
			$table->string('titulo_en')->after('titulo')->nullable();
			$table->text('descricao_en')->after('descricao')->nullable();
		});
		Schema::table('especificas', function(Blueprint $table)
		{
			$table->string('titulo_en')->after('titulo')->nullable();
			$table->text('descricao_en')->after('descricao')->nullable();
		});
		Schema::table('atuacaos', function(Blueprint $table)
		{
			$table->string('titulo_en')->after('titulo')->nullable();
			$table->text('resumo_en')->after('resumo')->nullable();
			$table->text('descricao_en')->after('descricao')->nullable();
		});
		Schema::table('socials', function(Blueprint $table)
		{
			$table->string('titulo_en')->after('titulo')->nullable();
		});
		Schema::table('midiatipos', function(Blueprint $table)
		{
			$table->string('titulo_en')->after('titulo')->nullable();
			$table->text('descricao_en')->after('descricao')->nullable();
		});
		Schema::table('noticias', function(Blueprint $table)
		{
			$table->string('titulo_en')->after('titulo')->nullable();
			$table->text('descricao_en')->after('descricao')->nullable();
			$table->text('resumo_en')->after('resumo')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('categorias', function(Blueprint $table)
		{
			$table->dropColumn('titulo_en');
			$table->dropColumn('descricao_en');
		});
		Schema::table('colaboradors', function(Blueprint $table)
		{
			$table->dropColumn('email_en');
			$table->dropColumn('descricao_en');
		});
		Schema::table('features', function(Blueprint $table)
		{
			$table->dropColumn('titulo_en');
		});
		Schema::table('arquivos', function(Blueprint $table)
		{
			$table->dropColumn('titulo_en');
		});
		Schema::table('midias', function(Blueprint $table)
		{
			$table->dropColumn('titulo_en');
			$table->dropColumn('descricao_en');
		});
		Schema::table('paginas', function(Blueprint $table)
		{
			$table->dropColumn('titulo_en');
			$table->dropColumn('resumo_en');
			$table->dropColumn('texto_en');
		});
		Schema::table('produtos', function(Blueprint $table)
		{
			$table->dropColumn('titulo_en');
			$table->dropColumn('descricao_en');
		});
		Schema::table('especificas', function(Blueprint $table)
		{
			$table->dropColumn('titulo_en');
			$table->dropColumn('descricao_en');
		});
		Schema::table('atuacaos', function(Blueprint $table)
		{
			$table->dropColumn('titulo_en');
			$table->dropColumn('descricao_en');
			$table->dropColumn('resumo_en');
		});
		Schema::table('socials', function(Blueprint $table)
		{
			$table->dropColumn('titulo_en');
		});
		Schema::table('midiatipos', function(Blueprint $table)
		{
			$table->dropColumn('titulo_en');
			$table->dropColumn('descricao_en');
		});
		Schema::table('noticias', function(Blueprint $table)
		{
			$table->dropColumn('titulo_en');
			$table->dropColumn('descricao_en');
			$table->dropColumn('resumo_en');
		});
	}

}