<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLocalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('locals', function(Blueprint $table) {
			$table->increments('id');
			$table->string('titulo');
			$table->string('slug');
			$table->string('endereco');
			$table->string('numero');
			$table->string('complemento');
			$table->string('bairro');
			$table->string('cidade');
			$table->integer('cep');
			$table->string('uf');
			$table->text('link_mapa');
			$table->string('dd1');
			$table->string('ddd2');
			$table->string('telefone1');
			$table->string('telefone2');
			$table->string('email');
			$table->string('user_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('locals');
	}

}
