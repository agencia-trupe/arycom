<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAtuacaosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('atuacaos', function(Blueprint $table) {
			$table->increments('id');
			$table->string('titulo');
			$table->string('slug')->unique();
			$table->text('resumo');
			$table->text('descricao');
			$table->string('imagem');
			$table->integer('user_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('atuacaos');
	}

}
