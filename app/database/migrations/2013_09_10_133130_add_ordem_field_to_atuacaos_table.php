<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddOrdemFieldToAtuacaosTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('atuacaos', function(Blueprint $table) {
			$table->integer('ordem');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('atuacaos', function(Blueprint $table) {
			$table->dropColumn('ordem');
		});
	}

}
