<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMidiasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('midias', function(Blueprint $table) {
			$table->increments('id');
			$table->string('titulo');
			$table->string('slug');
			$table->text('descricao');
			$table->string('path');
			$table->integer('mediatipo_id');
			$table->integer('user_id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('midias');
	}

}
