<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		//$this->call('SentrySeeder');
		//$this->call('PaginasTableSeeder');
		//$this->call('PaginasTableSeeder');
		$this->call('ColaboradorsTableSeeder');
		$this->call('AtuacaosTableSeeder');
		$this->call('CategoriaTableSeeder');
		$this->call('CategoriaTableSeeder');
		$this->call('ProdutosTableSeeder');
		$this->call('FeaturesTableSeeder');
		$this->call('NoticiasTableSeeder');
		$this->call('MidiasTableSeeder');
		$this->call('MidiatiposTableSeeder');
		$this->call('LocalsTableSeeder');
		$this->call('EspecificasTableSeeder');
		$this->call('SocialsTableSeeder');
	}

}