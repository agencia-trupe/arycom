<?php

return array(
    'contato' => array(
        'ativar' => array(
	        'Ativar Equipamento' => 'Ativar Equipamento',
	        'Se você já tem equipamento de comunicação' => 'Se você já tem equipamento de comunicação via satélite, e deseja ativar serviços de voz ou dados, preencha o seguinte formulário e em breve entraremos em contato.',
	        'Nome' => 'Nome',
			'Empresa' => 'Empresa',
			'E-mail' => 'E-mail',
			'Telefone' => 'Telefone',
			'Equipamentos' => 'Equipamentos',
			'Serviços que deseja realizar' => 'Serviços que deseja realizar',
			'Observações (opcional)' => 'Observações (opcional)',
			'ENVIAR' => 'ENVIAR',
        ),
        'email' => array(
			'Nome' => 'Nome',
			'E-mail' => 'E-mail',
			'Empresa' => 'Empresa',
			'Telefone' => 'Telefone',
			'Setor' => 'Setor',
			'Mensagem' => 'Mensagem',
			'Equipamentos' => 'Equipamentos',
			'Serviços' => 'Serviços',
			'Observações' => 'Observações',
        ),
        'index' => array(
			'Contato' => 'Contato',
			'Envie-nos uma mensagem' => 'Envie-nos uma mensagem',
			'Nome' => 'Nome',
			'Email' => 'Email',
			'Empresa' => 'Empresa',
			'Telefone' => 'Telefone',
			'Setor' => 'Setor',
			'Aeronáutico' => 'Aeronáutico',
			'Agricultura' => 'Agricultura',
			'Construção' => 'Construção',
			'Forças' => 'Forças',
			'Governo' => 'Governo',
			'Marítimo' => 'Marítimo',
			'Mídia' => 'Mídia',
			'Mineração' => 'Mineração',
			'Petróleo' => 'Petróleo',
			'Segurança' => 'Segurança',
			'Outros' => 'Outros',
			'Mensagem' => 'Mensagem',
			'ENVIAR' => 'ENVIAR',
			'ver mapa' => 'ver mapa',
			'T' => 'T',
			'F' => 'F',
			'Contato Arycom' => 'Contato Arycom',
			'enviado' => 'Mensagem enviada com sucesso!',
        )
	),
    'empresa' => array(
        'Arycom Comunicação via Satélite' => 'Arycom Comunicação via Satélite',
		'Principais Executivos' => 'Principais Executivos'
	),
    'home' => array(
        'Comunicação via Satélite' => 'Comunicação via Satélite',
		'Áreas de Atuação' => 'Áreas de Atuação',
		'Notícias' => 'Notícias',
		'ver todas as notícias' => 'ver todas as notícias',
	),
    'layout' => array(
        'Comunicação via Satélite' => 'Comunicação via Satélite',
		'Ativar equipamento com a Arycom!' => 'Ativar equipamento com a Arycom!',
		'Já tenho o equipamento! Quero ativar com a ARYCOM!' => 'Já tenho o equipamento! Quero ativar com a ARYCOM!',
		'Já tenho o equipamento! <br>Quero ativar com a Arycom!' => 'Já tenho o equipamento! <br>Quero ativar com a Arycom!',
		'buscar' => 'buscar',
		'Home' => 'Home',
		'Empresa' => 'Empresa',
		'Soluções' => 'Soluções',
		'Parceiros' => 'Parceiros',
		'Notícias' => 'Notícias',
		'Suporte' => 'Suporte',
		'Multimídia' => 'Mídias',
		'Contato' => 'Contato',
		'Todos os direitos reservados' => 'Todos os direitos reservados',
		'fb_lang' => 'pt_BR',
		'classe_marca' => 'marca-pt',
		'outro-idioma' => 'english version',
		'outro-idioma-img' => 'idioma-ingles.png',
		'outro-idioma-url' => '/idioma/en'
	),
    'midias' => array(
        'index' => array(
            'Contato Marketing' => 'Contato Marketing',
            'T' => 'T',
            'Envie um e-mail' => 'Envie um e-mail',
        ),
        'video' => array(
            'voltar' => 'voltar',
            'Contato Marketing' => 'Contato Marketing',
            'T' => 'T',
            'Envie um e-mail' => 'Envie um e-mail',
        )
	),
    'noticias' => array(
        'detalhe' => array(
			'voltar' => 'voltar',
			'Notícias' => 'Notícias',
			'Informações à imprensa' => 'Informações à imprensa - MSLGROUP Andreoli',
			'Envie um e-mail' => 'Envie um e-mail',
		),
        'index' => array(
			'Notícias' => 'Notícias',
			'Informações à imprensa' => 'Informações à imprensa - MSLGROUP Andreoli',
			'Envie um e-mail' => 'Envie um e-mail',
		),
	),
    'parceiros' => array(
        'Arycom Comunicação via Satélite' => 'Arycom Comunicação via Satélite',
	),
    'produtos' => array(
        'area' => array(
            'Soluções' => 'Soluções',
			'Áreas de atuação' => 'Áreas de atuação',
			'Soluções para outras aplicações' => 'Soluções para outras aplicações',
			'voltar' => 'voltar',
        ),
        'index' => array(
			'Soluções' => 'Soluções',
			'Selecione' => 'Selecione uma solução abaixo ou uma aplicação ao lado para saber mais',
			'Soluções por CATEGORIA' => 'Soluções por CATEGORIA',
			'Soluções por PRODUTO' => 'Soluções por PRODUTO',
        ),
        'produto' => array(
			'Soluções' => 'Soluções',
			'voltar' => 'voltar',
        ),
	),
    'suporte' => array(
		'Suporte' => 'Suporte',
		'Comunique-se com nossa área de suporte' => 'Comunique-se com nossa área de suporte',
		'Envie um e-mail' => 'Envie um e-mail',
		'Se você é usuário' => 'Se você é usuário de serviços <strong>Inmarsat da Arycom</strong> clique no link abaixo para visualizar tráfegos, status e verificar configurações do terminal.',
	),
    'busca' => array(
		'Resultado da Busca' => 'Resultado da Busca',
		'Nenhum resultado encontrado para' => 'Nenhum resultado encontrado para',
	),
);