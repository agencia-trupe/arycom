<?php

return array(
    'contato' => array(
        'ativar' => array(
            'Ativar Equipamento' => 'Activate Terminal',
            'Se você já tem equipamento de comunicação' => "If you already have a satcom terminal and want to enable the voice or data services, please fill out the form below and we will contact you shortly",
            'Nome' => 'Name',
            'Empresa' => 'Company',
            'E-mail' => 'E-mail',
            'Telefone' => 'Telephone',
            'Equipamentos' => 'Terminal Type',
            'Serviços que deseja realizar' => 'Services',
            'Observações (opcional)' => 'Observations (optional)',
            'ENVIAR' => 'SEND',
        ),
        'index' => array(
            'Contato' => 'Contact',
            'Envie-nos uma mensagem' => 'Send us a message',
            'Nome' => 'Name',
            'Email' => 'Email',
            'Empresa' => 'Company',
            'Telefone' => 'Telephone',
            'Setor' => 'Sector',
            'Aeronáutico' => 'Aviation',
            'Agricultura' => 'Agro',
            'Construção' => 'Civil Construction',
            'Forças' => 'Military',
            'Governo' => 'Government',
            'Marítimo' => 'Maritime',
            'Mídia' => 'Media',
            'Mineração' => 'Mining',
            'Petróleo' => 'Oil and Gas',
            'Segurança' => 'Security',
            'Outros' => 'Others',
            'Mensagem' => 'Message',
            'ENVIAR' => 'SEND',
            'ver mapa' => 'see map',
            'T' => 'P',
            'F' => 'P',
            'Contato Arycom' => 'Arycom Contact',
            'enviado' => "Thank you for contacting Arycom. We'll get back to you ASAP"
        )
    ), //OK
    'empresa' => array(
        'Arycom Comunicação via Satélite' => 'Arycom Satellite Communication',
        'Principais Executivos' => 'Main Executives'
    ), //OK
    'home' => array(
        'Comunicação via Satélite' => 'Satellite Communications',
        'Áreas de Atuação' => 'Sectors',
        'Notícias' => 'News',
        'ver todas as notícias' => 'see all news',
    ), //OK
    'layout' => array(
        'Comunicação via Satélite' => 'Satellite Communications',
        'Ativar equipamento com a Arycom!' => 'Activate equipment with Arycom!',
        'Já tenho o equipamento! Quero ativar com a ARYCOM!' => ' I already have a terminal and would like to activate with Arycom!',
        'Já tenho o equipamento! <br>Quero ativar com a Arycom!' => ' I already have a terminal <br>and would like to activate with Arycom',
        'buscar' => 'search',
        'Home' => 'Home',
        'Empresa' => 'About us',
        'Soluções' => 'Solutions',
        'Parceiros' => 'Partners',
        'Notícias' => 'News',
        'Suporte' => 'Support',
        'Multimídia' => 'Media',
        'Contato' => 'Contact',
        'Todos os direitos reservados' => 'all rights reserved',
        'fb_lang' => 'en_US',
        'classe_marca' => 'marca-en',
        'outro-idioma' => 'versão em português',
        'outro-idioma-img' => 'idioma-portugues.png',
        'outro-idioma-url' => '/idioma/pt-br'
    ), //OK
    'midias' => array(
        'index' => array(
            'Contato Marketing' => 'Marketing Contact',
            'T' => 'P',
            'Envie um e-mail' => 'Send an e-mail',
        ),
        'video' => array(
            'voltar' => 'back',
            'Contato Marketing' => 'Marketing Contact',
            'T' => 'P',
            'Envie um e-mail' => 'Send an e-mail',
        )
    ), //OK
    'noticias' => array(
        'detalhe' => array(
            'voltar' => 'back',
            'Notícias' => 'News',
            'Informações à imprensa' => 'Press Info',
            'Envie um e-mail' => 'Send an e-mail',
        ),
        'index' => array(
            'Notícias' => 'News',
            'Informações à imprensa' => 'Press Info',
            'Envie um e-mail' => 'Send an e-mail',
        ),
    ), //OK
    'parceiros' => array(
        'Arycom Comunicação via Satélite' => 'Arycom Satellite Communication',
    ), //OK
    'produtos' => array(
        'area' => array(
            'Soluções' => 'Solutions',
            'Áreas de atuação' => 'Sectors',
            'Soluções para outras aplicações' => 'Other applications solutions',
            'voltar' => 'back',
        ),
        'index' => array(
            'Soluções' => 'Solutions',
            'Selecione' => 'Select a solution below, or beside an application to learn more.',
            'Soluções por CATEGORIA' => 'Solutions by CATEGORY',
            'Soluções por PRODUTO' => 'Solutions by PRODUCT',
        ),
        'produto' => array(
            'Soluções' => 'Solutions',
            'voltar' => 'back',
        ),
    ), //OK
    'suporte' => array(
        'Suporte' => 'Support',
        'Comunique-se com nossa área de suporte' => 'Get in touch with our Support team:',
        'Envie um e-mail' => 'Send an e-email',
        'Se você é usuário' => "If you're a user of Arycom's Inmarsat services, click on the link below to view your account, verify your status and terminal settings.",
    ), //OK
    'busca' => array(
        'Resultado da Busca' => 'Search Results',
        'Nenhum resultado encontrado para' => 'No results found for',
    ), //OK
);