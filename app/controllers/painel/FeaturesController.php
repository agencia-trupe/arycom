<?php namespace App\Controllers\Painel;

use Auth, BaseController, Form, Input, Redirect, Sentry, View,
    Validator, Image, Feature;

class FeaturesController extends BaseController
{
    /**
     * Feature Repository
     *
     * @var Feature
     */
    protected $feature;

    public function __construct(Feature $feature)
    {
        $this->feature = $feature;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $features = $this->feature->ordered()->paginate(10);

        return View::make('painel.features.index', compact('features'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('painel.features.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        $input['user_id'] = Sentry::getUser()->id;

        $validation = Validator::make($input, Feature::$rules);

        if ($validation->passes()) {
            if (Input::hasFile('imagem')) {
                $imagem = Image::upload(Input::file('imagem'), 'features');
                $input['imagem'] = Image::thumb($imagem, 61, 76);
            }
            if (Input::hasFile('imagem_en')) {
                $imagem_en = Image::upload(Input::file('imagem_en'), 'features');
                $input['imagem_en'] = Image::thumb($imagem, 61, 76);
            }
            $this->feature->create($input);

            return Redirect::route('painel.aplicacoes.index');
        }

        return Redirect::route('painel.aplicacoes.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $feature = $this->feature->findOrFail($id);

        return View::make('painel.features.show', compact('feature'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $feature = $this->feature->find($id);

        if (is_null($feature)) {
            return Redirect::route('painel.aplicacoes.index');
        }

        return View::make('painel.features.edit', compact('feature'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $input = array_except(Input::all(), array('_method', 'imagem'));
        $validation = Validator::make($input, Feature::$rules);

        if ($validation->passes()) {
            $feature = $this->feature->find($id);
            if (Input::hasFile('imagem')) {
                $imagem = Image::upload(Input::file('imagem'), 'features');
                $input['imagem']   = Image::thumb($imagem, 61, 76);
                @unlink(public_path(substr($feature->imagem, 1)));
                @unlink(public_path(substr(str_replace('/61x76_crop', '', $feature->imagem), 1)));
            }
            $feature->update($input);

            return Redirect::route('painel.aplicacoes.show', $id);
        }

        return Redirect::route('painel.aplicacoes.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $feature = $this->feature->find($id);

        @unlink(public_path(substr($feature->imagem, 1)));
        @unlink(public_path(substr(str_replace('/61x76_crop', '', $feature->imagem), 1)));

        $feature->delete();

        return Redirect::route('painel.aplicacoes.index');
    }

}
