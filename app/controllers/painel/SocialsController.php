<?php namespace App\Controllers\Painel;

use Auth, BaseController, Form, Input, Redirect, Sentry, View,
    Validator, Social, Image;

class SocialsController extends BaseController
{
    /**
     * Social Repository
     *
     * @var Social
     */
    protected $social;

    public function __construct(Social $social)
    {
        $this->social = $social;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $socials = $this->social->ordered()->get();

        return View::make('painel.socials.index', compact('socials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('painel.socials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        $input['user_id'] = Sentry::getUser()->id;

        $validation = Validator::make($input, Social::$rules);

        if ($validation->passes()) {
            if (Input::hasFile('imagem')) {
                $input['imagem'] = Image::upload(Input::file('imagem'), 'socials');
            }
            $this->social->create($input);

            return Redirect::route('painel.socials.index');
        }

        return Redirect::route('painel.socials.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $social = $this->social->findOrFail($id);

        return View::make('painel.socials.show', compact('social'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $social = $this->social->find($id);

        if (is_null($social)) {
            return Redirect::route('painel.socials.index');
        }

        return View::make('painel.socials.edit', compact('social'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $input = array_except(Input::all(), array('_method', 'imagem'));
        $validation = Validator::make($input, Social::$rules);

        if ($validation->passes()) {
            $social = $this->social->find($id);
            if (Input::hasFile('imagem')) {
                $input['imagem'] = Image::upload(Input::file('imagem'), 'socials');
                @unlink(public_path(substr($social->imagem, 1)));
            }

            $social->update($input);

            return Redirect::route('painel.socials.show', $id);
        }

        return Redirect::route('painel.socials.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $social = $this->social->find($id);
        @unlink(public_path(substr($social->imagem, 1)));

        $social->delete();

        return Redirect::route('painel.socials.index');
    }

}
