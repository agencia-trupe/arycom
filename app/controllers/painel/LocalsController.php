<?php namespace App\Controllers\Painel;

use Auth, BaseController, Form, Input, Redirect, Sentry, View,
    Validator, Local, Image;

class LocalsController extends BaseController
{
    /**
     * Local Repository
     *
     * @var Local
     */
    protected $local;

    public function __construct(Local $local)
    {
        $this->local = $local;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $locals = $this->local->ordered()->get();

        return View::make('painel.locais.index', compact('locals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('painel.locais.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        $validation = Validator::make($input, Local::$rules);

        if ($validation->passes()) {
            if (Input::hasFile('imagem')) {
                $input['imagem'] = Image::upload(Input::file('imagem'), 'locais');
            }
            $this->local->create($input);

            return Redirect::route('painel.locais.index');
        }

        return Redirect::route('painel.locais.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $local = $this->local->findOrFail($id);

        return View::make('painel.locais.show', compact('local'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $local = $this->local->find($id);

        if (is_null($local)) {
            return Redirect::route('painel.locais.index');
        }

        return View::make('painel.locais.edit', compact('local'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $input = array_except(Input::all(), array('_method', 'imagem'));
        $validation = Validator::make($input, Local::$rules);

        if ($validation->passes()) {
            $local = $this->local->find($id);

            if (Input::hasFile('imagem')) {
                $input['imagem'] = Image::upload(Input::file('imagem'), 'locais');
                @unlink(public_path(substr($local->imagem, 1)));
            }

            $local->update($input);

            return Redirect::route('painel.locais.show', $id);
        }

        return Redirect::route('painel.locais.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $local = $this->local->find($id);

        @unlink(public_path(substr($local->imagem, 1)));

        $local->delete();

        return Redirect::route('painel.locais.index');
    }

}
