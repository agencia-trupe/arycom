<?php namespace App\Controllers\Painel;

/**
 * AtuacoesController.php
 *
 * AtuacoesController
 *
 * Add Seo features into CodeIgniter projects.
 *
 * @package     Package
 * @category    Category
 * @framework   Framework
 * @author      Author
 * @link        http://trupe.net
 * @version     Version
 * @todo        Check image deletion when editing or deleting object
 */

use Auth, BaseController, Form, Input, Redirect, Sentry, View,
    Validator, Atuacao, Image, File, Parser, Templates, Str;

class AtuacoesController extends BaseController
{
    /**
     * Atuacao Repository
     *
     * @var Atuacao
     */
    protected $atuacao;

    public function __construct(Atuacao $atuacao)
    {
        $this->atuacao = $atuacao;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $atuacoes = $this->atuacao->orderBy('ordem', 'asc')->get();

        return View::make('painel.atuacoes.index', compact('atuacoes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('painel.atuacoes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        $input['user_id'] = Sentry::getUser()->id;
        $validation = Validator::make($input, Atuacao::$rules);

        if ($validation->passes()) { {
                $input['imagem'] = Image::upload(Input::file('imagem'), 'atuacoes');
            }
            $this->atuacao->create($input);

            return Redirect::route('painel.atuacoes.index');
        }

        return Redirect::route('painel.atuacoes.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $atuacao = $this->atuacao->findOrFail($id);

        return View::make('painel.atuacoes.show', compact('atuacao'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $atuacao = $this->atuacao->find($id);

        if (is_null($atuacao)) {
            return Redirect::route('painel.atuacoes.index');
        }

        return View::make('painel.atuacoes.edit', compact('atuacao'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $input = array_except(Input::all(), array('_method', 'imagem'));
        $validation = Validator::make($input, Atuacao::$rules);

        if ($validation->passes()) {
            $atuacao = $this->atuacao->find($id);
            if (Input::hasFile('imagem')) {
                $input['imagem'] = Image::upload(Input::file('imagem'), 'atuacoes');
                @unlink(public_path(substr($atuacao->imagem, 1)));
            }
            $atuacao->update($input);

            return Redirect::route('painel.atuacoes.show', $id);
        }

        return Redirect::route('painel.atuacoes.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $atuacao = $this->atuacao->find($id);

        @unlink(public_path(substr($colaborador->imagem, 1)));
        @unlink(public_path(substr(str_replace('/230x170_crop', '/77x50_crop', $colaborador->imagem), 1)));
        @unlink(public_path(substr(str_replace('/230x170_crop', '', $colaborador->imagem), 1)));

        $atuacao->delete();

        return Redirect::route('painel.atuacoes.index');
    }

}
