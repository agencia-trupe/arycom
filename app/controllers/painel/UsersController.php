<?php namespace App\Controllers\Painel;

use Auth, BaseController, Form, Input, Redirect, Sentry, View,
    Validator, User, Image, Lang;

class UsersController extends BaseController
{
    /**
     * Social Repository
     *
     * @var Social
     */
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $users = $this->user->all();

        return View::make('painel.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('painel.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();

        try {
            $user = Sentry::createUser($input);

            return Redirect::route('painel.users.index');
        } catch (\Cartalyst\Sentry\Users\LoginRequiredException $e) {
            return Redirect::route('painel.users.create')->withErrors(array('login' => Lang::get('validation.required', array('attribute' => 'login'))));
        } catch (\Cartalyst\Sentry\Users\PasswordRequiredException $e) {
            return Redirect::route('painel.users.create')->withErrors(array('login' => Lang::get('validation.required', array('attribute' => 'password'))));
        } catch (\Cartalyst\Sentry\Users\UserNotActivatedException $e) {
            return Redirect::route('painel.users.create')->withErrors(array('login' => Lang::get('messages.user-not-activated')));
        } catch (\Cartalyst\Sentry\Users\UserNotFoundException $e) {
            return Redirect::route('painel.users.create')->withErrors(array('login' => Lang::get('messages.error-not-found', array('attribute' => $username))));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $user = $this->user->findOrFail($id);

        return View::make('painel.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->user->find($id);

        if (is_null($user)) {
            return Redirect::route('painel.users.index');
        }

        return View::make('painel.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $input = array_except(Input::all(), '_method');
        $validation = Validator::make($input, Social::$rules);

        if ($validation->passes()) {
            $user = $this->user->find($id);
            if (Input::hasFile('imagem')) {
                $input['imagem'] = Image::upload(Input::file('imagem'), 'users');
                @unlink(public_path(substr($user->imagem, 1)));
            }

            $user->update($input);

            return Redirect::route('painel.users.show', $id);
        }

        return Redirect::route('painel.users.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = $this->user->find($id);
        @unlink(public_path(substr($user->imagem, 1)));

        $user->delete();

        return Redirect::route('painel.users.index');
    }

}
