<?php namespace App\Controllers\Painel;

/**
 * EspecificasController.php
 *
 * EspecíficasController
 *
 * Gerencia as áreas específicas de atuação.
 *
 * @package     Arycom
 * @category    Controllers
 * @framework   Laravel
 * @author      Nilton Freitas - Trupe Agência Criativa
 * @link        http://trupe.net
 * @version     0.1
 * @todo        -
 */

use Auth, BaseController, Form, Input, Redirect, Sentry, View,
    Validator, Especifica, Atuacao, Response;

class EspecificasController extends BaseController
{
    /**
     * Especifica Repository
     *
     * @var Especifica
     */
    protected $especifica;

    public function __construct(Especifica $especifica)
    {
        $this->especifica = $especifica;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $especificas = $this->especifica->orderBy('ordem', 'asc')->get();

        return View::make('painel.especificas.index', compact('especificas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $atuacoes = array('' => 'Selecione uma Área geral de atuação') + Atuacao::ordered()->get()->lists('titulo', 'id');

        return View::make('painel.especificas.create', compact('atuacoes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        $input['user_id'] = Sentry::getUser()->id;
        $validation = Validator::make($input, Especifica::$rules);

        if ($validation->passes()) {
            $this->especifica->create($input);

            return Redirect::route('painel.especificas.index');
        }

        return Redirect::route('painel.especificas.create')->withInput()
                                                           ->withErrors($validation)
                                                           ->with('message', 'There were validation errors.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $especifica = $this->especifica->findOrFail($id);

        return View::make('painel.especificas.show', compact('especifica'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $especifica = $this->especifica->find($id);
        $atuacoes = array('' => 'Selecione uma Área geral de atuação')
                    + Atuacao::ordered()->get()->lists('titulo', 'id');

        if (is_null($especifica)) {
            return Redirect::route('painel.especificas.index');
        }

        return View::make('painel.especificas.edit', compact('especifica', 'atuacoes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $input = array_except(Input::all(), '_method');
        $input['user_id'] = Sentry::getUser()->id;
        $validation = Validator::make($input, Especifica::$rules);

        if ($validation->passes()) {
            $especifica = $this->especifica->find($id);
            $especifica->update($input);

            return Redirect::route('painel.especificas.show', $id);
        }

        return Redirect::route('painel.especificas.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->especifica->find($id)->delete();

        return Redirect::route('painel.especificas.index');
    }

    public function getEspecificas($geralId)
    {
        $especificas = Especifica::where('atuacao_id', $geralId)->ordered()->get();

        return Response::json(
                array('error' => false,
                    'especificas' => $especificas->lists('titulo', 'id')),
                200);
    }

}
