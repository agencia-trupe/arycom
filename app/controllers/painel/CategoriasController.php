<?php namespace App\Controllers\Painel;

use Auth, BaseController, Form, Input, Redirect, Sentry, View,
    Validator, Categoria, Atuacao;

class CategoriasController extends BaseController
{
    /**
     * Categoria Repository
     *
     * @var Categoria
     */
    protected $categoria;

    public function __construct(Categoria $categoria)
    {
        $this->categoria = $categoria;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $categorias = $this->categoria->orderBy('ordem', 'asc')->get();

        return View::make('painel.categorias.index', compact('categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $atuacoes = Atuacao::ordered()->get();

        return View::make('painel.categorias.create', compact('atuacoes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        $input['user_id'] = Sentry::getUser()->id;
        $validation = Validator::make($input, Categoria::$rules);

        if ($validation->passes()) {
            $this->categoria->create($input);

            return Redirect::route('painel.categorias.index');
        }

        return Redirect::route('painel.categorias.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $categoria = $this->categoria->findOrFail($id);

        return View::make('painel.categorias.show', compact('categoria'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $categoria = $this->categoria->find($id);

        if (is_null($categoria)) {
            return Redirect::route('painel.categorias.index');
        }

        return View::make('painel.categorias.edit', compact('categoria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $input = array_except(Input::all(), '_method');
        $validation = Validator::make($input, Categoria::$rules);

        if ($validation->passes()) {
            $categoria = $this->categoria->find($id);
            $categoria->update($input);

            return Redirect::route('painel.categorias.show', $id);
        }

        return Redirect::route('painel.categorias.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->categoria->find($id)->delete();

        return Redirect::route('painel.categorias.index');
    }

}
