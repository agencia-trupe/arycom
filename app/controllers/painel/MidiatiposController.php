<?php namespace App\Controllers\Painel;

use Auth, BaseController, Form, Input, Redirect, Sentry, View,
    Validator, Midia, Midiatipo;

class MidiatiposController extends BaseController
{
    /**
     * Midiatipo Repository
     *
     * @var Midiatipo
     */
    protected $midiatipo;

    public function __construct(Midiatipo $midiatipo)
    {
        $this->midiatipo = $midiatipo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $midiatipos = $this->midiatipo->ordered()->paginate(10);

        return View::make('painel.midiatipos.index', compact('midiatipos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('painel.midiatipos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        $input['user_id'] = Sentry::getUser()->id;
        $validation = Validator::make($input, Midiatipo::$rules);

        if ($validation->passes()) {
            $this->midiatipo->create($input);

            return Redirect::route('painel.midiatipos.index');
        }

        return Redirect::route('painel.midiatipos.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $midiatipo = $this->midiatipo->findOrFail($id);

        return View::make('painel.midiatipos.show', compact('midiatipo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $midiatipo = $this->midiatipo->find($id);

        if (is_null($midiatipo)) {
            return Redirect::route('painel.midiatipos.index');
        }

        return View::make('painel.midiatipos.edit', compact('midiatipo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $input = array_except(Input::all(), '_method');
        $input['user_id'] = Sentry::getUser()->id;
        $validation = Validator::make($input, Midiatipo::$rules);

        if ($validation->passes()) {
            $midiatipo = $this->midiatipo->find($id);
            $midiatipo->update($input);

            return Redirect::route('painel.midiatipos.show', $id);
        }

        return Redirect::route('painel.midiatipos.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->midiatipo->find($id)->delete();

        return Redirect::route('painel.midiatipos.index');
    }

}
