<?php namespace App\Controllers\Painel;

use BaseController, Response, ArquivoEn, Input, Image;

class ArquivosEnController extends BaseController {

	/**
	 * Arquivo Repository
	 *
	 * @var Arquivo
	 */
	protected $arquivo;

	public function __construct(ArquivoEn $arquivo)
	{
		$this->arquivo = $arquivo;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		if (!Input::hasFile('path-en'))
		{
			$response = array('error' => 'Você precisa adicionar um arquivo');
		}
		elseif ($input['titulo'] == "")
		{
			$response = array('error' => 'Você precisa adicionar um título');
		}
		else
		{
			$response = 'teste';

			$arquivo = Input::file('path-en');
 			$arquivo->move(public_path().'/uploads/arquivos/', $arquivo->getClientOriginalName());
			$input['path'] = '/uploads/arquivos/'.$arquivo->getClientOriginalName();

			$arquivo = $this->arquivo->create($input);
			if( $arquivo )
				$response = array('error' => false, 'msg' => 'Arquivo adicionado com sucesso', 'arquivoId' => $arquivo->id);
			else
				$response = array('error' => 'Falha ao adicionar arquivo');
		}
		echo json_encode($response);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$arquivo = $this->arquivo->find($id);
		@unlink(public_path($arquivo->path));
		if ($arquivo->delete())
		{
			$response = array('error' => false, 'msg' => 'Arquivo apagado com sucesso');
		}
		else
		{
			$response = array('error' => true, 'msg' => 'Erro ao deletar arquivo');
		}
		echo json_encode($response);
	}

}
