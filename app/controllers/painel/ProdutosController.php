<?php namespace App\Controllers\Painel;

use Auth, BaseController, Form, Input, Redirect, Sentry, View,
    Validator, Produto, Categoria, Atuacao, Image, Feature;

class ProdutosController extends BaseController
{
    /**
     * Produto Repository
     *
     * @var Produto
     */
    protected $produto;

    public function __construct(Produto $produto)
    {
        $this->produto = $produto;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $filtro = Input::get('cat');

        if($filtro){
            $produtos = $this->produto->where('categoria_id', '=', $filtro)->orderBy('ordem', 'asc')->get();
            $filtroCat = $filtro;
        }else{
            $produtos = $this->produto->orderBy('ordem', 'asc')->paginate(20);
            $filtroCat = false;
        }
        
        $categorias = Categoria::orderBy('ordem', 'asc')->get();        

        return View::make('painel.produtos.index', compact('produtos', 'categorias', 'filtroCat'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categorias = array('' => 'Selecione uma Categoria')
                    + Categoria::orderBy('ordem', 'asc')->lists('titulo', 'id');
        $atuacoes 	= Atuacao::orderBy('ordem', 'asc')->lists('titulo', 'id');
        $aplicacoes = Feature::orderBy('ordem', 'asc')->lists('titulo', 'id');

        return View::make('painel.produtos.create', compact('categorias', 'atuacoes', 'aplicacoes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        $input['user_id'] = Sentry::getUser()->id;
        $validation = Validator::make($input, Produto::$rules);

        if ($validation->passes()) {
            if (Input::hasFile('imagem')) {
                $imagem = Image::upload(Input::file('imagem'), 'produtos');
                $input['imagem'] = Image::thumb($imagem, 290, 200);
            }
            if (Input::hasFile('arquivo')) {

            }

            //Cria um novo produto
            if((isset($input['atuacao']) && $input['atuacao']) && (isset($input['feature']) && $input['feature']))
                $produto = new Produto(array_except($input, array('atuacao', 'feature')));
            elseif(isset($input['atuacao']) && $input['atuacao'])
                $produto = new Produto(array_except($input, array('atuacao')));
            elseif(isset($input['feature']) && $input['feature'])
                $produto = new Produto(array_except($input, array('feature')));
            else
                $produto = new Produto();

            $produto->save();

            //Adiciona os modelos relacionados
            if(isset($input['atuacao']) && $input['atuacao'])
                $produto->atuacoes()->sync(array_values($input['atuacao']));

            if(isset($input['feature']) && $input['feature'])
                $produto->features()->sync(array_values($input['feature']));
            
            //Salva o novo produto
            $produto->save();

            return Redirect::route('painel.produtos.index');
        }

        return Redirect::route('painel.produtos.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $produto = $this->produto->findOrFail($id);

        return View::make('painel.produtos.show', compact('produto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $produto = $this->produto->find($id);
        $categorias = array('' => 'Selecione uma Categoria')
                    + Categoria::orderBy('ordem', 'asc')->lists('titulo', 'id');
        $atuacoes 	= Atuacao::lists('titulo', 'id');
        /*foreach ($produto->atuacoes as $atuacao) {
           foreach ($atuacao->especificas as $especifica) {
                $especificas[$especifica->id] = $especifica->titulo;
           }
        }*/

        //$especificas = array('' => 'Selecione') + $especificas;

        $aplicacoes = Feature::lists('titulo', 'id');

        if (is_null($produto)) {
            return Redirect::route('painel.produtos.index');
        }

        return View::make('painel.produtos.edit', compact('produto', 'categorias', 'atuacoes', 'aplicacoes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $input = array_except(Input::all(), array('_method', 'imagem'));
        $validation = Validator::make($input, Produto::$rules);

        if ($validation->passes()) {
            $produto = $this->produto->find($id);

            if (Input::hasFile('imagem')) {
                $imagem = Image::upload(Input::file('imagem'), 'produtos');
                $input['imagem']   = Image::resize($imagem, 290, 200);
                @unlink(public_path(substr($produto->imagem, 1)));
                @unlink(public_path(substr(str_replace('/290x200_crop', '', $produto->imagem), 1)));
            }
            
            if(isset($input['atuacao']) && $input['atuacao'])
                $produto->atuacoes()->sync(array_values($input['atuacao']));
            if(isset($input['feature']) && $input['feature'])
                $produto->features()->sync(array_values($input['feature']));
            
            if((isset($input['atuacao']) && $input['atuacao']) && (isset($input['feature']) && $input['feature']))
                $produto->update(array_except($input, array('atuacao', 'feature')));
            elseif(isset($input['atuacao']) && $input['atuacao'])
                $produto->update(array_except($input, array('atuacao')));
            elseif(isset($input['feature']) && $input['feature'])
                $produto->update(array_except($input, array('feature')));
            else
                $produtos->update();

            return Redirect::route('painel.produtos.show', $id);
        }

        return Redirect::route('painel.produtos.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->produto->find($id)->delete();

        return Redirect::route('painel.produtos.index');
    }
}
