<?php namespace App\Controllers\Painel;

use Auth, BaseController, Form, Input, Redirect, Sentry, View,
    Validator, Noticia, \Carbon\Carbon, Image, Str;

class NoticiasController extends BaseController
{
    /**
     * Noticia Repository
     *
     * @var Noticia
     */
    protected $noticia;

    public function __construct(Noticia $noticia)
    {
        $this->noticia = $noticia;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $noticias = $this->noticia->orderBy('published', 'desc')->get();

        return View::make('painel.noticias.index', compact('noticias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('painel.noticias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();

        $validation = Validator::make($input, Noticia::$rules);

        if ($validation->passes()) {
            if (Input::hasFile('imagem')) {
                $input['imagem'] = Image::upload(Input::file('imagem'), 'noticias');
            }

            $input['published'] = Carbon::createFromFormat('d/m/Y H:i',
                                $input['published_data'] . ' ' .
                                $input['published_time'], 'America/Sao_Paulo')
                                ->toDateTimeString();
            $input['user_id'] = Sentry::getUser()->id;

            $this->noticia->create(array_except($input, array('published_time', 'published_data')));

            return Redirect::route('painel.noticias.index');
        }

        return Redirect::route('painel.noticias.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $noticia = $this->noticia->findOrFail($id);

        return View::make('painel.noticias.show', compact('noticia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $noticia = $this->noticia->find($id);

        if (is_null($noticia)) {
            return Redirect::route('painel.noticias.index');
        }

        return View::make('painel.noticias.edit', compact('noticia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $input = array_except(Input::all(), '_method');

        //dd($input);

        $validation = Validator::make($input, Noticia::$rules);

        if ($validation->passes()) {
            $input['status'] = (isset($input['status'])) ?: '';
            $input['destaque'] = (isset($input['destaque'])) ?: '';

            $input['published'] = Carbon::createFromFormat('d/m/Y H:i',
                                $input['published_data'] . ' ' .
                                $input['published_time'], 'America/Sao_Paulo')
                                ->toDateTimeString();
            $input['user_id'] = Sentry::getUser()->id;
            $noticia = $this->noticia->find($id);

            if (Input::hasFile('imagem')) {
                $input['imagem'] = Image::upload(Input::file('imagem'), 'noticias');
                @unlink(public_path(substr($noticia->imagem, 1)));
            }

            $noticia->update(array_except($input, array('published_data', 'published_time')));

            return Redirect::route('painel.noticias.index');
        }

        return Redirect::route('painel.noticias.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->noticia->find($id)->delete();

        return Redirect::route('painel.noticias.index');
    }

}
