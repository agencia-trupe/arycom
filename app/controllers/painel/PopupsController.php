<?php namespace App\Controllers\Painel;

use Auth, BaseController, Form, Input, Redirect, Sentry, View,
    Validator, Popup, \Carbon\Carbon, Image, Str;

class PopupsController extends BaseController
{
    /**
     * Noticia Repository
     *
     * @var Noticia
     */
    protected $popup;

    public function __construct(Popup $popup)
    {
        $this->popup = $popup;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $popups = $this->popup->get();

        return View::make('painel.popup.index', compact('popups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $idioma = Input::get('idioma');

        return View::make('painel.popup.create', compact('idioma'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = array_except(Input::all(), 'file');

        if($input['tipo'] == 'imagem'){
            $input['video_embed'] = '';
            $input['imagem'] = $this->upload_foto();

            if($input['destino_link'] == '_blank')
                $input['link'] = $this->prep_url($input['link']);
        }else{
            $input['link'] = '';
            $input['destino_link'] = '';
        }

        $this->popup->create($input);

        return Redirect::route('painel.popup.index');
    }

    private function upload_foto()
    {
        return Image::upload(Input::file('file'), 'popups');
    }

    private function prep_url($str = '')
    {
        if ($str == 'http://' OR $str == '')
        {
            return '';
        }

        $url = parse_url($str);

        if ( ! $url OR ! isset($url['scheme']))
        {
            $str = 'http://'.$str;
        }

        return $str;
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $popup = $this->popup->find($id);

        return View::make('painel.popup.show', compact('popup'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $popup = $this->popup->find($id);

        $idioma = $popup->idioma;

        if (is_null($popup)) {
            return Redirect::route('painel.popup.index');
        }

        return View::make('painel.popup.edit', compact('popup', 'idioma'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $popup = $this->popup->find($id);

        $input = array_except(Input::all(), '_method');
        $input = array_except(Input::all(), 'file');

        //dd($input);

        try {

            if($input['tipo'] == 'imagem'){
                $input['video_embed'] = '';

                if(isset($_FILES['file']) && $_FILES['file']['error'] != 4){
                    $input['imagem'] = $this->upload_foto();
                }else{
                    unset($input['imagem']);
                }

                if($input['destino_link'] == '_blank')
                    $input['link'] = $this->prep_url($input['link']);
            }else{
                $input['link'] = '';
                $input['destino_link'] = '';
            }

            $popup->update($input);

        } catch (Exception $e) {

            return Redirect::route('painel.popup.edit', $id)
                 ->withInput()
                 ->with('message', 'There were validation errors.');

        }

        return Redirect::route('painel.popup.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->popup->find($id)->delete();

        return Redirect::route('painel.popup.index');
    }

}
