<?php namespace App\Controllers\Painel;

use Auth, BaseController, Form, Input, Redirect, Sentry, View,
    Validator, Pagina, Image, Image2, File, Parser, Templates, Str;

class PaginasController extends BaseController
{
    /**
     * Pagina Repository
     *
     * @var Pagina
     */
    protected $pagina;

    public function __construct(Pagina $pagina)
    {
        $this->pagina = $pagina;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $paginas = $this->pagina->all();

        return View::make('painel.paginas.index', compact('paginas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $templatesNames = Templates::getTemplates();

        return View::make('painel.paginas.create')->with('templates', $templatesNames);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        $input['user_id'] = Sentry::getUser()->id;
        $validation = Validator::make($input, Pagina::$rules);

        if ($validation->passes()) {
            if (Input::hasFile('imagem')) {
                $input['imagem'] = Image::upload(Input::file('imagem'), 'paginas');
            }
            $this->pagina->create($input);

            return Redirect::route('painel.paginas.index');
        }

        return Redirect::route('painel.paginas.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $pagina = $this->pagina->findOrFail($id);

        return View::make('painel.paginas.show', compact('pagina'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $pagina = $this->pagina->find($id);

        if (is_null($pagina)) {
            return Redirect::route('painel.paginas.index');
        }

        return View::make('painel.paginas.edit', compact('pagina'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $input = array_except(Input::all(), array('_method', 'imagem'));
        $input['user_id'] = Sentry::getUser()->id;

        $validation = Validator::make($input, Pagina::$rules);

        if ($validation->passes()) {
            $pagina = $this->pagina->find($id);

            if (Input::hasFile('imagem')) {

                $imagem = Input::file('imagem');
                $input['imagem'] = 'uploads/paginas/'.$imagem->getClientOriginalName();
                
                $move = $imagem->move('uploads/paginas', $imagem->getClientOriginalName());
                
                if($id == 8){
                    Image2::make($move)->resize(300, null, true, false)->save($move, 100);
                }

                @unlink(public_path(substr($pagina->imagem, 1)));
            }

            $pagina->update($input);

            return Redirect::route('painel.paginas.show', $id);
        }

        return Redirect::route('painel.paginas.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $pagina = $this->pagina->find($id);

        @unlink(public_path(substr($pagina->imagem, 1)));

        $pagina->delete();

        return Redirect::route('painel.paginas.index');
    }

}
