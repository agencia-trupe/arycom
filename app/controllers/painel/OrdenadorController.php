<?php namespace App\Controllers\Painel;

class OrdenadorController extends \BaseController
{
    public function ordenar()
    {
        $input = \Input::all();
        $model = $input['model'];
        $modelUcf = ucfirst($model);

        parse_str($input['itens'], $itens);

        $result = null;
        foreach ($itens[$model] as $key => $value) {
            $object = $modelUcf::find($value);
            $object->ordem = $key;
            $object->save();
        }

        return \Response::json(
                    array(
                        'error' 	=> 	false,
                        'message' 	=>	'Ítens ordenados com sucesso'
                    ), 200
                );

    }
}
