<?php namespace App\Controllers\Painel;

use Auth, BaseController, Form, Input, Redirect, Sentry, View,
    Validator, Midia, Midiatipo, Image, Video;

class MidiasController extends BaseController
{
    /**
     * Midia Repository
     *
     * @var Midia
     */
    protected $midia;

    public function __construct(Midia $midia)
    {
        $this->midia = $midia;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $tipo = Input::get('tipo');
        if($tipo)
            $midias = $this->midia->orderBy('id', 'DESC')->where('midiatipo_id', Input::get('tipo'))->paginate(10);
        else
            $midias = $this->midia->orderBy('id', 'DESC')->paginate(10);

        return View::make('painel.midias.index', compact('midias', 'tipo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $midiatipos = Midiatipo::ordered()->lists('titulo', 'id');

        return View::make('painel.midias.create', compact('midiatipos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        $input['user_id'] = Sentry::getUser()->id;
        $validation = Validator::make($input, Midia::$rules);

        if ($validation->passes()) {
            if (Input::hasFile('imagem')) {
                $input['path'] = Image::upload(Input::file('imagem'), 'midias');
            } else {
                $input['path'] = $input['video'];
                // Salvar a thumbnail do video
                //Video::saveLocalThumbnails($input['path'], public_path().'/uploads/videos/');
            }

            $this->midia->create(array_except($input, array('video', 'imagem')));

            return Redirect::route('painel.midias.index', array('tipo' => $input['midiatipo_id']));
        }

        return Redirect::route('painel.midias.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function show($id)
    {
        $midia = $this->midia->findOrFail($id);

        return View::make('painel.midias.show', compact('midia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $id
     * @return Response
     */
    public function edit($id)
    {
        $midiatipos = Midiatipo::ordered()->lists('titulo', 'id');
        $midia = $this->midia->find($id);

        if (is_null($midia)) {
            return Redirect::route('painel.midias.index');
        }

        return View::make('painel.midias.edit', compact('midia', 'midiatipos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function update($id)
    {
        $input = array_except(Input::all(), '_method');
        $input['user_id'] = Sentry::getUser()->id;
        $validation = Validator::make($input, Midia::$rules);

        if ($validation->passes()) {
            $midia = $this->midia->find($id);
            if (Input::hasFile('imagem')) {
                $input['path'] = Image::upload(Input::file('imagem'), 'midias');

            } elseif (isset($input['video'])) {
                $input['path'] = $input['video'];
            }
            $midia->update(array_except($input, array('imagem', 'video')));

            return Redirect::route('painel.midias.show', $id);
        }

        return Redirect::route('painel.midias.edit', $id)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $id
     * @return Response
     */
    public function destroy($id)
    {
        $this->midia->find($id)->delete();

        return Redirect::route('painel.midias.index');
    }

}
