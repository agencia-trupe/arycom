<?php namespace App\Controllers\Painel;

/**
 * ColaboradorsController.php
 *
 * ColaboradorsController
 *
 * Gerencia os colaboraodores.
 *
 * @package     Arycom
 * @category    Controllers
 * @framework   Laravel
 * @author      Nilton Freitas - Trupe Agência Criativa
 * @link        http://trupe.net
 * @version     0.1
 * @todo        -
 */

use Auth, BaseController, Form, Input, Redirect, Sentry, View,
    Validator, Colaborador, Image, File, Parser, Templates;

class ColaboradorsController extends BaseController
{
    /**
     * Colaborador Repository
     *
     * @var Colaborador
     */
    protected $_colaborador;

    public function __construct(Colaborador $colaborador)
    {
        $this->_colaborador = $colaborador;
    }

    /**
     * Display a listing of the resource.s
     *
     * @return Response
     */
    public function index()
    {
        $colaboradors = $this->_colaborador->ordered()->paginate(10);

        return View::make('painel.colaboradors.index', compact('colaboradors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return View::make('painel.colaboradors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $input = Input::all();
        $input['user_id'] = Sentry::getUser()->id;
        $validation = Validator::make($input, Colaborador::$rules);

        if ($validation->passes()) {
            if (Input::hasFile('imagem')) {
                $imagem = Image::upload(Input::file('imagem'), 'colaboradores');
                $input['imagem'] = Image::resize($imagem, 230, 170, TRUE);
            }
            $this->_colaborador->create($input);

            return Redirect::route('painel.colaboradores.index');
        }

        return Redirect::route('painel.colaboradores.create')
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int      $colaboradorId
     * @return Response
     */
    public function show($colaboradorId)
    {
        $colaborador = $this->_colaborador->findOrFail($colaboradorId);

        return View::make('painel.colaboradors.show', compact('colaborador'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int      $colaboradorId
     * @return Response
     */
    public function edit($colaboradorId)
    {
        $colaborador = $this->_colaborador->find($colaboradorId);

        if (is_null($colaborador)) {
            return Redirect::route('painel.colaboradores.index');
        }

        return View::make('painel.colaboradors.edit', compact('colaborador'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int      $colaboradorId
     * @return Response
     */
    public function update($colaboradorId)
    {
        $input = array_except(Input::all(), array('_method', 'imagem'));
        $input['user_id'] = Sentry::getUser()->id;
        $validation = Validator::make($input, Colaborador::$rules);

        if ($validation->passes()) {
            $colaborador = $this->_colaborador->find($colaboradorId);
            if (Input::hasFile('imagem')) {
                $imagem = Image::upload(Input::file('imagem'), 'paginas');
                $input['imagem']   = Image::resize($imagem, 230, 170, TRUE);
                @unlink(public_path(substr($colaborador->imagem, 1)));
                @unlink(public_path(substr(str_replace('/230x170_crop', '', $colaborador->imagem), 1)));
            }
            $colaborador->update($input);

            return Redirect::route('painel.colaboradores.show', $colaboradorId);
        }

        return Redirect::route('painel.colaboradores.edit', $colaboradorId)
            ->withInput()
            ->withErrors($validation)
            ->with('message', 'There were validation errors.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int      $colaboradorId
     * @return Response
     */
    public function destroy($colaboradorId)
    {
        $colaborador = $this->_colaborador->find($colaboradorId);

        @unlink(public_path(substr($colaborador->imagem, 1)));
        @unlink(public_path(substr(str_replace('/230x170_crop', '', $colaborador->imagem), 1)));

        $colaborador->delete();

        return Redirect::route('painel.colaboradores.index');
    }

}
