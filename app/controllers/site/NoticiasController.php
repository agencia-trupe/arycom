<?php namespace App\Controllers\Site;

/**
 * NoticiasController.php
 *
 * NoticiasController
 *
 * Exibe a lista de notícias e os posts individuais.
 *
 * @package     Arycom
 * @category    Controllers
 * @framework   Laravel
 * @author      Nilton Freitas - Trupe Agência Criativa
 * @link        http://trupe.net
 * @version     0.1
 * @todo        Integração da biblioteca de mídia ao cadastro e edição de
 *              notícias
 */

use BaseController, View, Noticia, Image;

class NoticiasController extends BaseController
{
    /**
     * Notícia Repository
     *
     * @var Noticia
     */
    protected $noticia;

    public function __construct(Noticia $noticia)
    {
        setlocale(LC_ALL, "pt_BR", "pt_BR.iso-8859-1", "pt_BR.utf-8", "portuguese");
        date_default_timezone_set('America/Sao_Paulo');
        $this->noticia = $noticia;
    }

    public function lista()
    {
        $destaque = $this->noticia->published()->filtroLinguagem()->where('destaque', TRUE)->first();
        if($destaque)
            $noticias = $this->noticia->published()->filtroLinguagem()->where('id', '!=', $destaque->id)->paginate(7);
        else
            $noticias = $this->noticia->published()->filtroLinguagem()->paginate(7);

        return View::make('site.noticias.index', compact('noticias', 'destaque'));
    }

    public function detalhe($noticiaSlug)
    {
        $noticia = $this->noticia->where('slug', $noticiaSlug)->first();

        return View::make('site.noticias.detalhe', compact('noticia'));
    }

}
