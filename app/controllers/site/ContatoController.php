<?php namespace App\Controllers\Site;
/**
 * ContatoController.php
 *
 * ContatoController
 *
 * Exibe o formulário e as informações de contato.
 *
 * @package     Arycom
 * @category    Controllers
 * @framework   Laravel
 * @author      Nilton Freitas - Trupe Agência Criativa
 * @link        http://trupe.net
 * @version     0.1
 * @todo        Integração da biblioteca de mídia ao cadastro e edição de
 *              notícias
 */

use BaseController, Local, View, Midia, Midiatipo, Image, Pagina, Input, Mail, App\Services\Validators\ContatoValidator, App\Services\Validators\AtivacaoValidator;

class ContatoController extends BaseController
{

    public function index()
    {
        $locais = Local::ordered()->get();

        return View::make('site.contato.index', compact('locais') );
    }

    public function sendMail()
    {
        $validator = new ContatoValidator;

        if ($validator->passes()) {
            $mail = Mail::send('site.contato.email-contato', Input::all(), function($message) {
                $message->to('arycom@arycom.com', 'Contato Arycom')
                        ->subject('Contato via site - ' . Input::get('nome'))
                        ->replyTo(Input::get('email'), Input::get('nome'));
            });

            echo json_encode(
                array('errors' => false,
                      'msg' => \Lang::get('site.contato.index.enviado')
                    ));
        } else {
            $errors = null;
            foreach ($validator->getErrors()->all() as $error) {
                $errors .= $error . "\n";
            }
            echo json_encode(
                array( 'errors' => $errors,
                       'msg' => 'Corrija os seguintes erros de validação:'));
        }
    }

    public function ativacao()
    {
        $locais = Local::all();

        return View::make('site.contato.ativar', compact('locais') );
    }

    public function enviarAtivacao()
    {
        $validator = new AtivacaoValidator;

        if ($validator->passes()) {
            $mail = Mail::send('site.contato.email-ativacao', Input::all(), function($message) {
                $message->to('activations@arycom.com', 'Ativações Arycom')
                        ->subject('Ativação de Produto - ' . Input::get('nome'))
                        ->replyTo(Input::get('email'), Input::get('nome'));
            });

            echo json_encode(
                array('errors' => false,
                      'msg' => 'Mensagem enviada com sucesso!'
                    ));
        } else {
            $errors = null;
            foreach ($validator->getErrors()->all() as $error) {
                $errors .= $error . "\n";
            }
            echo json_encode(
                array( 'errors' => $errors,
                       'msg' => 'Corrija os seguintes erros de validação:'));
        }
    }
}
