<?php namespace App\Controllers\Site;

/**
 * MidiasController.php
 *
 * MidiasController
 *
 * Exibe as imagens e os vídeos da biblioteca de mídia.
 *
 * @package     Arycom
 * @category    Controllers
 * @framework   Laravel
 * @author      Nilton Freitas - Trupe Agência Criativa
 * @link        http://trupe.net
 * @version     0.1
 * @todo        Integração da biblioteca de mídia ao cadastro e edição de
 *              notícias
 */

use BaseController, View, Midia, Midiatipo, Image, Pagina;

class MidiasController extends BaseController
{
    /**
     * Mídia Repository
     *
     * @var Midia
     */
    protected $midia;

    /**
     * MídiaTipo Repository
     *
     * @var MidiaTipo
     */
    protected $midiaTipo;

    public function __construct(Midia $midia, Midiatipo $midiaTipo)
    {
        $this->midia = $midia;
        $this->midiaTipo = $midiaTipo;
    }

    public function lista( $midiaTipo = NULL)
    {
        $audioVisual = Pagina::where('slug', 'audio-visual')->first();

        $midiaTipo = ($midiaTipo) ? $midiaTipo : 'fotos';
        $midiaTipos = $this->midiaTipo->ordered()->get();

        $tipo = $this->midiaTipo->where('slug', $midiaTipo)->first();
        $midias = $tipo->midias()->orderBy('id', 'DESC')->paginate(8);

        return View::make('site.midias.index', compact('audioVisual', 'tipo', 'midiaTipos', 'midias'));
    }

    public function video( $videoSlug )
    {
        $audioVisual = Pagina::where('slug', 'audio-visual')->first();
        $video = Midia::where( 'slug', $videoSlug )->first();

        return View::make('site.midias.video', compact('audioVisual', 'video'));
    }
}
