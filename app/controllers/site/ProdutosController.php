<?php namespace App\Controllers\Site;

/**
 * ProdutosController.php
 *
 * ProdutosController
 *
 * Add Seo features into CodeIgniter projects.
 *
 * @package     Arycom
 * @category    Controllers
 * @framework   Laravel
 * @author      Nilton de Freitas - Trupe Agência Criativa
 * @link        http://trupe.net
 * @version     0.1
 * @todo        -
 */

use BaseController, View, Atuacao, Categoria;

class ProdutosController extends BaseController
{
    public function test()
    {
        $atuacoes = Atuacao::all();
        $categorias = Categoria::all();

        return View::make('site.produtos.index', compact('atuacoes', 'categorias'));
    }
}
