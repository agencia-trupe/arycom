<?php namespace App\Controllers\Site;

/**
 * AtuacoesController.php
 *
 * AtuacoesController
 *
 * Exibe detalhes das areas de atuação.
 *
 * @package     Arycom
 * @category    Controllers
 * @framework   Laravel
 * @author      Nilton de Freitas - Trupe Agência Criativa
 * @link        http://trupe.net
 * @version     0.1
 * @todo        -
 */

use BaseController, View, Atuacao, Categoria, Produto;

class AtuacoesController extends BaseController
{
    public function lista()
    {
        $atuacoes = Atuacao::orderBy('ordem', 'asc')->get();
        $categorias = Categoria::orderBy('ordem', 'asc')->get();

        return View::make('site.produtos.index', compact('atuacoes', 'categorias'));
    }

    public function area($atuacaoSlug)
    {
        $atuacao = Atuacao::where('slug', $atuacaoSlug)->first();
        $atuacoes = Atuacao::orderBy('ordem', 'asc')->get();

        return View::make('site.produtos.area', compact('atuacao', 'atuacoes', 'categorias'));
    }

    public function produto($produtoSlug)
    {
        $produto = Produto::where('slug', $produtoSlug)->first();

        return View::make('site.produtos.produto', compact('produto'));
    }

}
