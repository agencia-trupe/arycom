<?php namespace App\Controllers\Site;

/**
 * PaginasController.php
 *
 * PaginasController
 *
 * Exibe as páginas cadastradas
 * Usado para exibir a página Empresa.
 *
 * @package     Arycom
 * @category    Controllers
 * @framework   Laravel
 * @author      Nilton Freitas - Trupe Agência Criativa
 * @link        http://trupe.net
 * @version     0.1
 * @todo        -
 */

use BaseController, Colaborador, View, Pagina;

class PaginasController extends BaseController
{
    public function empresa()
    {
        $pagina_slug = Pagina::where('slug', 'empresa')->first();
        $empresa = Pagina::where('slug', 'empresa')->take(1)->get();
        $empresa = $empresa[0];
        $colaboradores = Colaborador::ordered()->get();

        return View::make('site.empresa.index', compact('pagina_slug', 'colaboradores', 'empresa'));
    }

    public function parceiros()
    {
        $pagina_slug = Pagina::where('slug', 'parceiros')->first();
        $parceiros = Pagina::where('slug', 'parceiros')->first();

        return View::make('site.parceiros.index', compact('pagina_slug', 'parceiros'));
    }
}
