<?php

/**
 * @todo Constructor
 */

class Tubain
{

    public function getVideoFeed( $videoUrl )
    {
        $videoId = $this->getVideoId( $videoUrl );

        return file_get_contents("http://gdata.youtube.com/feeds/api/videos/"
                                    . $videoId . "?v=2&alt=jsonc");
    }

    public function getVideoId( $videoUrl )
    {
        if (strpos($videoUrl, 'http') !== 0) {
            throw new Exception("Invalid Video Url", 1);
        }

        parse_str( parse_url( $videoUrl, PHP_URL_QUERY ), $videoUrlVars );

        return (isset($videoUrlVars['v'])) ? $videoUrlVars['v'] : false;
    }

    public function getThumbnailUrl($videoUrl, $quality = NULL)
    {
        $videoId = $this->getVideoId($videoUrl);
        $jsonFeed = $this->getVideoFeed( $videoUrl );
        $decodedFeed = json_decode($jsonFeed);

        return $decodedFeed->data->thumbnail->sqDefault;
    }

    public function parseThumbnail( $videoUrl )
    {
        $thumbnailUrl = $this->getThumbnailUrl( $videoUrl );

        return '<img src="' . $thumbnailUrl . '" alt="" />';
    }

    public function getVideoThumbnails( $videoUrl )
    {
        $decodedFeed = json_decode($this->getVideoFeed( $videoUrl ));
        foreach ($decodedFeed->data->thumbnail as $key => $value) {
            $videoThumbnails[$key] = $value;
        }

        return $videoThumbnails;
    }

    public function saveLocalThumbnails($videoUrl, $localPath)
    {
        $videoThumbnails = $this->getVideoThumbnails( $videoUrl );
        $videoId = $this->getVideoId($videoUrl);
        $fileDir = $localPath . $videoId;

        if ( ! is_dir($fileDir) ) {
            // dir doesn't exist, make it
            $old = umask(0);
            mkdir($fileDir, 0777, TRUE);
            umask($old);
        }

        foreach ($videoThumbnails as $key => $value) {
            $filename = $fileDir . '/' . $key . '.jpg';

            if( ! file_put_contents( $filename, file_get_contents( $value ) ) )
                throw new Exception("Erro ao salvar thumbnail " . $value , 1);

            $result[] = $filename;
        }

        return $result;
    }

    public function buildEmbed( $videoId = null, $videoUrl = null, $width, $height)
    {
        if (! $videoId) {
            $videoId = $this->getVideoId( $videoUrl );
        }

        return  '<iframe width="' . $width . '" height="' . $height .
                '" src="//www.youtube.com/embed/' . $videoId .
                '" frameborder="0" allowfullscreen></iframe>';
    }

    function videoThumb($url){
        $notfound = "http://www.placehold.it/180x130/EFEFEF/AAAAAA&text=Thumb+Not+Found";

        if (strpos($url, 'youtube.com') !== FALSE) {
            $fonte = 'youtube';
        } elseif(strpos($url, 'youtu.be') !== FALSE) {
            $fonte = 'youtu.be';
        }elseif(strpos($url, 'vimeo.com') !== FALSE){
            $fonte = 'vimeo';
        }else{
            $fonte = 'id_video';
        }

        if($fonte == 'youtube'){
            parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
            $video_id = $my_array_of_vars['v'];
            if($video_id){
                $thumb = "http://i1.ytimg.com/vi/".$video_id."/hqdefault.jpg";
                return $thumb;
            }else{
                return $notfound;
            }
        }elseif($fonte == 'vimeo'){
            preg_match('@vimeo.com/([0-9]*)$@i', $url, $found);
            $video_id = $found[1];
            if($video_id){
                $xml = simplexml_load_file("http://vimeo.com/api/v2/video/".$video_id.".xml");
                foreach($xml->video as $node){
                    $thumb = $node->thumbnail_small;
                }
                return $thumb;
            }else{
                return $notfound;
            }
        }elseif($fonte == 'youtu.be'){
            preg_match('@youtu.be/(.*)$@i', $url, $found);
            $video_id = $found[1];
            if($video_id){
                $thumb = "http://i1.ytimg.com/vi/".$video_id."/hqdefault.jpg";
                return $thumb;
            }else{
                return $notfound;
            }
        }elseif($fonte == 'id_video'){
            if($url){
                $thumb = "http://i1.ytimg.com/vi/".$url."/hqdefault.jpg";
                return $thumb;
            }else{
                return $notfound;
            }
        }else{
            return $notfound;
        }
    }    
}
